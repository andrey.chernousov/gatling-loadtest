package gatlingtest_3

import Requests80._
import io.gatling.core.Predef._
import io.gatling.http.Predef._

import scala.annotation.tailrec
import scala.collection.breakOut
import scala.reflect.ClassTag
import io.gatling.commons.NotNothing
import io.gatling.commons.stats.{KO, OK, Status}
import io.gatling.commons.util.TypeCaster
import io.gatling.commons.util.TypeHelper._
import io.gatling.commons.validation._
import io.gatling.core.action.Action
import io.gatling.core.session.el.ElMessages
import io.gatling.core.stats.message.ResponseTimings
import io.gatling.core.session.Session._
import com.typesafe.scalalogging.LazyLogging
import scalaj.http._
import play.api.libs.json._

import scala.collection.mutable.ListBuffer
import scala.language.postfixOps
import scala.concurrent.duration._
//import scala.language.postfixOps

class LoadTest80 extends Simulation {
  before{
    println("**** LoadTest80 ****")
  }
  // TODO:  
//    val httpConf = http.baseUrl("http://192.168.101.91:80")
  val httpConf = http.baseUrl("http://192.168.101.80:80")

  val scenario_viewAndEdit = scenario("Initial create. Get and edit shemes")
    .exec(login)
    .exec(getListMnemos)
    .exec(ifCreateOneMnemos)

    .exec(getListMnemos)
    .exec(ifCreateOneMnemos)
    .exec(getListMnemos)
    .exec(ifCreateOneMnemos)
    .exec(getListMnemos)
    .exec(ifCreateOneMnemos)
    .exec(getListMnemos)
    .exec(ifCreateOneMnemos)
    .exec(getListMnemos)
    .exec(ifCreateOneMnemos)
    .exec(getListMnemos)
    .exec(openMainPortal)
    .exec(openEditorMnemosheme)
    //////    .repeat(5){
    //////      exec(getListMnemos)
    //////      .exec(getListMnemos)
    //////    }
    .repeat(2) {
    exec(login)
      .randomSwitch((50, {
        exec(view)
          .exec(openMainPortal)
          .exec(openEditorMnemosheme)
          .exec(getListMnemos)
          .doIf("${_notEmpty}") {
            exec(getMnemo)
          }
          .pause(0.2 second, 0.5 second)

      }),
        (50, {
          exec(edit)
            .exec(openMainPortal)
            .exec(openEditorMnemosheme)
            .exec(getListMnemos)
            .doIf("${_notEmpty}") {
              exec(saveMnemos)
            }
            .pause(0.2 second, 0.5 second)
        }))
  }


  //  .doIf("${_notEmpty}") {
  //    exec(deletingAllMnemos)
  //  }

  val scenario_viewAndCreate = scenario("Get and create sheme")
    .exec(login)
    .exec(getListMnemos)
    .repeat(2) {
      exec(login)
      .randomSwitch((99, {
        exec(view)
          .exec(openMainPortal)
          .exec(openEditorMnemosheme)
          .exec(getListMnemos)
          .doIf("${_notEmpty}") {
            exec(getMnemo)
          }
          .pause(0.2 second, 0.5 second)
      }),
        (1, {
          exec(create)
            .exec(openMainPortal)
            .exec(openEditorMnemosheme)
            .exec(createOneMnemos)
            .pause(0.2 second, 0.5 second)
        }))
    }


  //  val scenario_edit=scenario("Open mnemoshem")
  //    .randomSwitch((50, exec(login)),(50, exec(newnew)))

  setUp(scenario_viewAndEdit.inject(constantUsersPerSec(10) during (1 seconds))
    .protocols(httpConf), //,
    scenario_viewAndCreate.inject(nothingFor(5 second), constantUsersPerSec(10) during (1 seconds))
      .protocols(httpConf))

  after {
    println("***************************** executing cleanup....*****************************")
    val login: HttpResponse[String] = Http("http://192.168.101.80:8080/auth/realms/master/protocol/openid-connect/token")
      .postForm
      .param("client_id", "portal")
      .param("grant_type", "password")
      .param("username", "admin")
      .param("password", "admin")
      .header("Connection", "close")
      .asString
    val binResponse = Json.parse(login.body)
    //    println((binResponse \ "access_token").as[String])
    for (_delId <- TestVariables.mnemoCollectionId) {
      val res: HttpResponse[String] = Http("http://192.168.101.80:80/mnemoschemes/api/v1/mnemo/" + _delId)
        .method("DELETE")
        .header("Authorization", "Bearer " + (binResponse \ "access_token").as[String])
        .asString
      println(res.code + res.body)
    }
    println("***************************** finished executing cleanup....*********************")
  }
}

object TestVariables {
  var mnemoCollectionId: ListBuffer[String] = ListBuffer()

}