package gatlingtest_3

abstract class MnemoshemeProps {
  val id: String
  val name: String
  val createDate: Long
  val ts: Long
  val rights: List[String]
}

//[{"id":"8f6374b8-588d-46c1-883f-02a129b1b052","name":"1","createDate":1571660508050,"ts":1571660508050,"rights":["read","write"]},{"id":"4ad14396-0b9f-435c-bdae-9d4d0aea0483","name":"Sibur3-3","createDate":1574425995993,"ts":1574686098877,"rights":["read","write"]}
//,{"id":"ee8adca0-bacb-4df0-b454-4a0bec472bab","name":"Sibur3-2","createDate":1574421177459,"ts":1574756137274,"rights":["read","write"]},{"id":"7f1d1986-96a3-4cfe-a314-42bdfc6eb39f","name":"1gfd23fd","createDate":1571899227659,"ts":1571899227659,"rights":["read","w
//rite"]},{"id":"70a401d6-b18e-4235-9c0a-164727c47bd2","name":"Sibur3-1","createDate":1574420435752,"ts":1574753926135,"rights":["read","write"]},{"id":"4090510d-7cd8-4453-8871-42308cc54da6","name":"Reservoir Park. Capacities E7-E8","createDate":1572356826836,"ts":15
//72356826836,"rights":["read","write"]},{"id":"39959fdc-67da-407a-9b25-76b367c7c38e","name":"Elements","createDate":1574261873903,"ts":1574261873903,"rights":["read","write"]},{"id":"cb5b71ba-1da1-4324-98c4-b1929849f64c","name":"22","createDate":1571931165492,"ts":1
//572356439812,"rights":["read"]},{"id":"bd390313-63b8-44cb-bddd-a53472a27f74","name":"777","createDate":1573024769525,"ts":1573024769525,"rights":["read","write"]},{"id":"67dfdc7b-14df-4cc3-abfd-360a8ae5163e","name":"ForDebug(Ch TestCafe V)","description":"null","cr
//eateDate":1573112748445,"ts":1573112748445,"rights":["read","write"]},{"id":"a615a02a-3ecc-45bb-b26c-50cb7ee4478f","name":"1234","createDate":1571932021287,"ts":1571932021287,"rights":["read","write"]},{"id":"5a378d47-187a-4061-be43-e27807dffcbb","name":"2","create
//Date":1571665009600,"ts":1571665009600,"rights":["read","write"]},{"id":"77cf6e04-1c58-427d-b595-447771a03a9b","name":"44","createDate":1571931331339,"ts":1571931331339,"rights":["read","write"]},{"id":"dd32a19d-7fbf-4801-89eb-74087c8ce7c3","name":"large","descript
//ion":"string","createDate":123,"ts":123,"rights":["read","write"]},{"id":"5f40eb9d-798f-47a2-b34d-bbbb5d4d10a4","name":"TestResource","createDate":1572527489782,"ts":1572527489782,"rights":["read","write"]},{"id":"8a8a01f4-b082-40ed-9fae-4e4c03071ecd","name":"▌Єр ь
//эхьюёїхьр тшфэр?","createDate":1571922957751,"ts":1571922957751,"rights":["read","write"]},{"id":"d42f7161-b318-44a8-a625-771a38996c2b","name":"Sibur","createDate":1571898412355,"ts":1571898412355,"rights":["read","write"]},{"id":"44f15d34-875e-4adc-9063-410ab6640d
//1f","name":"Sibur V2-1","createDate":1572519240804,"ts":1574077436194,"rights":["read","write"]},{"id":"4b90003b-89e8-4c86-9409-ace0f4f0d60c","name":"ForDebug(Ch TestCafe V)","description":"null","createDate":1573112748445,"ts":1573112748445,"rights":["read","write
//"]},{"id":"4834b99f-21b2-42a1-ba27-2cac3e0eff1c","name":"1","createDate":1571895013794,"ts":1574747169882,"rights":["read","write"]},{"id":"52bdf108-876c-4acb-bf26-4d20df10b90b","name":"123","createDate":1571932211164,"ts":1571932211164,"rights":["read","write"]},{
//"id":"25579bae-b9ca-4776-be3a-efedde8c261c","name":"1221","createDate":1573033700001,"ts":1573033700001,"rights":["read","write"]},{"id":"3477005a-c261-4512-a416-0642afabdea2","name":"111","description":"111","createDate":1571899435809,"ts":1571899435809,"rights":[
//"read","write"]},{"id":"698d9f05-8d74-43a0-8c41-3e6a1513dbef","name":"TEst1111","createDate":1571926295432,"ts":1574258001060,"rights":["read","write"]},{"id":"1d85b3f2-21a4-4092-9590-f7cd74abd15d","name":"Sibur V2-2","createDate":1572519735790,"ts":1574922168349,"
//rights":["read","write"]},{"id":"c14ebe1d-17b1-4a78-ac60-9623e09ef281","name":"ForDebug(Ch TestCafe V)","description":"null","createDate":1573112748445,"ts":1573112748445,"rights":["read","write"]},{"id":"569fc812-7ab5-4638-b2a7-f3cc13c0b332","name":"Sibur","create
//Date":1571898629016,"ts":1572424276288,"rights":["read","write"]},{"id":"4ccc2cc0-b512-4f77-99a0-f74a9a1d7a99","name":"1234567890","createDate":1571987815718,"ts":1571987815718,"rights":["read","write"]},{"id":"4e90bc40-9c16-4aa5-be82-ef483d414e7e","name":"mnemoMon
//emo","createDate":1573020615374,"ts":1573020615374,"rights":["read","write"]},{"id":"a86299f7-bcf6-4ead-ba7b-fdfab20ba6c3","name":"qazwsx","createDate":1571989255586,"ts":1571991703505,"rights":["read","write"]},{"id":"337001fa-1042-11ea-9d2c-000c291ac301","name":"
//ForDebug(Ch TestCafe 0)","description":"null","createDate":1573112748445,"ts":1573112748445,"rights":["read","write"]},{"id":"2d29c6b0-2983-49fc-a04c-af0240787e31","name":"ForDebug(Ch TestCafe V)","description":"null","createDate":1573112748445,"ts":1573112748445,"
//rights":["read","write"]},{"id":"c78dce84-4deb-4a50-aae2-464cc18a5b1a","name":"ForDebug(Ch TestCafe V)","description":"null","createDate":1573112748445,"ts":1573112748445,"rights":["read","write"]}]

