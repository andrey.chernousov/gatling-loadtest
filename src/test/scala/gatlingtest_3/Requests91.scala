package gatlingtest_3

import java.util.UUID

import io.gatling.core.Predef._
import io.gatling.http.Predef._

import scala.util.Random
//import play.api.libs.json.{JsValue, Json}
//import org.json4s.jackson.Json
import play.api.libs.json._

import scala.collection.mutable.ListBuffer

//import scalaj.http.Http
//http://192.168.11.229:8086
object Requests91 {
  // TODO:  
    val login = http("Login").post("http://192.168.101.91:80/auth/realms/master/protocol/openid-connect/token")
//  val login = http("Login").post("http://192.168.101.80:8080/auth/realms/master/protocol/openid-connect/token")
    .formParam("client_id", "portal")
    .formParam("grant_type", "password")
    .formParam("username", "admin")
    .formParam("password", "admin")
    .header("Connection", "close")
    .check(jsonPath("$.access_token").saveAs("_access_token"))
    .check(status.is(200))

  //  val newnew=http("Firs req").post("http://192.168.100.56:8080/auth/realms/master/protocol/openid-connect/token")
  //    .formParam("client_id", "portal")
  //    .formParam("grant_type", "password")
  //    .formParam("username", "admin")
  //    .formParam("password", "admin")
  //    .header("Connection", "close")
  //    .check(status.is(200))

  val openMainPortal =
    exec(http("GET/").get("/"))
      .exec(http("GET/built/scripts/zone.min.js").get("/built/scripts/zone.min.js"))
      .exec(http("GET/built/scripts/import-map-overrides.js").get("/built/scripts/import-map-overrides.js"))
      .exec(http("GET/built/scripts/system.min.js").get("/built/scripts/system.min.js"))
      .exec(http("GET/built/scripts/amd.min.js").get("/built/scripts/amd.min.js"))
      .exec(http("GET/built/scripts/named-exports.js").get("/built/scripts/named-exports.js"))
      .exec(http("GET/built/scripts/named-register.min.js").get("/built/scripts/named-register.min.js"))
      .exec(http("GET/built/vendors~main.js").get("/built/vendors~main.js"))
      .exec(http("GET/built/main.js").get("/built/main.js"))
      .exec(http("GET/built/assets/settings.json").get("/built/assets/settings.json"))
      .exec(http("GET/auth/realms/master/account").get("/auth/realms/master/account"))
      .exec(http("GET/json/applicationUnits").get("/json/applicationUnits"))
      .exec(http("GET/json/applicationUnits").get("/json/applicationUnits"))
      .exec(http("GET/built/scripts/single-spa.min.js").get("/built/scripts/single-spa.min.js"))
      .exec(http("GET/json/UserSettings").get("/json/UserSettings"))
      .exec(http("GET/json/UserCommonApplications").get("/json/UserCommonApplications"))

  val openEditorMnemosheme =
    exec(http("GET/hostRootShell/assets/i18n/en_US.json").get("/hostRootShell/assets/i18n/en_US.json"))
      .exec(http("GET/hostMnemo/scripts.js").get("/hostMnemo/scripts.js"))
      .exec(http("GET/mnemo/css/common.css").get("/mnemo/css/common.css"))
      .exec(http("GET/mnemo/resources/graph.txt").get("/mnemo/resources/graph.txt"))
      .exec(http("GET/mnemo/resources/graph_ru.txt").get("/mnemo/resources/graph_ru.txt"))
      .exec(http("GET/mnemo/resources/editor.txt").get("/mnemo/resources/editor.txt"))
      .exec(http("GET/mnemo/resources/editor_ru.txt").get("/mnemo/resources/editor_ru.txt"))
      .exec(http("GET/hostMnemo/main.js").get("/hostMnemo/main.js"))
      .exec(http("GET/hostMnemo/assets/i18n/en_US.json").get("/hostMnemo/assets/i18n/en_US.json"))
      .exec(http("GET/hostMnemo/assets/i18n/zyfra-mnemo/zyfraMnemoModule.en_US.json").get("/hostMnemo/assets/i18n/zyfra-mnemo/zyfraMnemoModule.en_US.json"))
      .exec(http("GET/hostMnemo/assets/settings.json").get("/hostMnemo/assets/settings.json"))
      .exec(http("GET/mnemo/resources/grapheditor.txt").get("/mnemo/resources/grapheditor.txt"))
      .exec(http("GET/mnemo/styles/default.xml").get("/mnemo/styles/default.xml"))
      .exec(http("GET/mnemo/stencils/mnemo.library.xml").get("/mnemo/stencils/mnemo.library.xml"))

  val createOneMnemos = exec(_.set("_id", UUID.randomUUID().toString))
    //                        .exec(session =>{
    //                        println("${_access_token}")
    //                        session})
    //    .exec(session =>{
    //      println("************************************************")
    //      println(session("_id").as[String])
    //      println("************************************************")
    //
    //      val json_string = scala.io.Source.fromFile("mnemo_0.json").getLines.mkString
    //      val json_string2=ElFileBody("C:/BK/gatlingtest_3/src/test/scala/gatlingtest_3/mnemo_0.json").toString()
    //      println(System.getProperty("user.dir"))
    //      println(json_string)
    //      println("************************************************")
    //      session
    //    })
    // TODO:
    .exec(http("POST/mnemobase/api/v1/mnemo").post("/mnemobase/api/v1/mnemo")
//    .exec(http("POST/mnemoschemes/api/v1/mnemo").post("/mnemoschemes/api/v1/mnemo")
    .body(ElFileBody("mnemo_create.json")).asJson
    .header("Authorization", "Bearer " + "${_access_token}")
    .check(status.is(200))
  )

  val getListMnemos =
  // TODO:  
    exec(http("GET/mnemobase/api/v1/mnemo/info").get("/mnemobase/api/v1/mnemo/info")
//    exec(http("/mnemoschemes/api/v1/mnemo/info").get("/mnemoschemes/api/v1/mnemo/info")
      .header("Authorization", "Bearer " + "${_access_token}")
      .check(bodyString.saveAs("_collectionId"))
      .check(jsonPath("$[*]").findAll.saveAs("items")))
    .exec(session => {
        //        println("************************************************")
        //      println(session("_collectionId").as[String])
        //        println(session("items").as[String])
        val _sCollectionShemes = session("items").as[Vector[String]]
        //        println(ClassTag(_sCollectionShemes.getClass))
        var _aaaa: ListBuffer[String] = ListBuffer()
        TestVariabless.mnemoCollectionId = ListBuffer()
        for (i <- _sCollectionShemes) {
          var _itemSheme = Json.parse(i)
          if ((_itemSheme \ "name").as[String] == "ForDebug(Ch TestCafe V)") {
            //            println(i)
            TestVariabless.mnemoCollectionId = TestVariabless.mnemoCollectionId :+ (_itemSheme \ "id").as[String]
            //            println((_itemSheme \ "id").as[String])
          }
        }
        var a: Int = TestVariabless.mnemoCollectionId.size
        if (a == 0) {
          //          println("false")
          session.set("_notEmpty", false)
        }
        else {
          //          println("true")
          session.set("_notEmpty", true)
        }
        })
        .exec(ses => {
          var aa: Int = TestVariabless.mnemoCollectionId.size
        if (aa > 4) {
          ses.set("_more", false)
        }
        else {
          ses.set("_more", true)
        }
      })
//    .exec(sess => {
//      println("___________length " + TestVariabless.mnemoCollectionId.size+" _more="+sess("_more").as[String]+
//        " _notEmpty="+sess("_notEmpty").as[String])
//      sess
//    })

        //        if (TestVariabless.mnemoCollectionId.size > 4) session.set("_more", 4)
        //        println("************************************************")
        //        println("************************************************")
        //        session
  val getId = exec(session => {
    var rand = new Random(System.currentTimeMillis())
    var random_index = rand.nextInt(TestVariabless.mnemoCollectionId.length)
    var result = TestVariabless.mnemoCollectionId(random_index)
    println("******* " + result)


    session
  })
  val saveMnemos = exec(session => {
    var rand = new Random(System.currentTimeMillis())
    var random_index = rand.nextInt(TestVariabless.mnemoCollectionId.length)
    var result: String = TestVariabless.mnemoCollectionId(random_index)
//    println("*** _idEditedScheme " + result)
    session.set("_idEditedScheme", result)
    //      session
  })
    //    .exec(session=>{
    //      println("******* idEditedScheme "+session("_idEditedScheme").as[String])
    //      var _z:String=session("_idEditedScheme").as[String]
    //      println(_z)
    //      println(session("_collectionId").as[String])
    //      session
    //    })
    // TODO:
    .exec(http("PUT/mnemobase/api/v1/mnemo").put("/mnemobase/api/v1/mnemo")
//    .exec(http("PUT/mnemoschemes/api/v1/mnemo").put("/mnemoschemes/api/v1/mnemo")
    .body(ElFileBody("mnemo_edited.json")).asJson
    .header("Authorization", "Bearer " + "${_access_token}")
    .check(status.is(200))
  )

  val deletingOneMnemos = exec(session => {
//    println("_______________________")
    for (a <- TestVariabless.mnemoCollectionId) {
//      println("Delllllllllll " + a) // + session("_idDel").as[String])
      // TODO:
      exec(http("DELETE/mnemobase/api/v1/mnemo/")
        .delete("/mnemobase/api/v1/mnemo/" + a)
//      exec(http("DELETE/mnemoschemes/api/v1/mnemo/")
//        .delete("/mnemoschemes/api/v1/mnemo/" + a)
        .header("Authorization", "Bearer " + "${_access_token}")
        .check(status.is(200)
        ))
    }
    session
  })

  val deletingOnce = exec(http("DELETE/mnemobase/api/v1/mnemo/")
    // TODO:
    .delete("/mnemobase/api/v1/mnemo/" + "${oneId}")
//    .delete("/mnemoschemes/api/v1/mnemo/" + "${oneId}")
    .header("Authorization", "Bearer " + "${_access_token}")
    .check(status.is(200)))

  val deletingAllMnemos =
    exec(session => {
      var a: Seq[String] = TestVariabless.mnemoCollectionId.toSeq
      session.set("deletingCollection", a)
    })
      .foreach("${deletingCollection}", "oneId") {
        exec(deletingOnce)
      }

  val ifCreateOneMnemos =
    doIf("${_more}") {
      println("createOneMnemos")
      exec(createOneMnemos)
    }

  val view = exec(session => {
    println(" view")
    session
  })
  val edit = exec(session => {
    println("     edit")
    session
  })
  val create = exec(session => {
    println("          create")
    session
  })
  val getMnemo = exec(session => {
    var rand = new Random(System.currentTimeMillis())
    var random_index = rand.nextInt(TestVariabless.mnemoCollectionId.length)

    var result: String = TestVariabless.mnemoCollectionId(random_index)
//    println("******************** _idGetingcheme " + result)
    session.set("_idGetingcheme", result)
    //      session
  })
    // TODO:
      .exec(http("GET/mnemobase/api/v1/mnemo/").get("/mnemobase/api/v1/mnemo/" + "${_idGetingcheme}") //!!!
//    .exec(http("GET/mnemoschemes/api/v1/mnemo/").get("/mnemoschemes/api/v1/mnemo/" + "${_idGetingcheme}") //!!!
      .header("Authorization", "Bearer " + "${_access_token}")
//      .check(status.is(200))
    )
    .exec(http("GET/mnemo/resources/grapheditor_ru.txt").get("/mnemo/resources/grapheditor_ru.txt") //!!!!!!
      .header("Authorization", "Bearer " + "${_access_token}"))
//      .check(status.is(200)))
    .exec(http("GET/mnemo/styles/default.xml").get("/mnemo/styles/default.xml") //!!!
      .header("Authorization", "Bearer " + "${_access_token}"))
//      .check(status.is(200)))


  //  GET/mnemobase/api/v1/mnemo/
  //  GET/mnemo/resources/grapheditor_ru.txt
  //  GET/mnemo/styles/default.xml

}

//        println((jjjjv \ "id").as[String])

//io.netty.util.internal.OutOfDirectMemoryError: