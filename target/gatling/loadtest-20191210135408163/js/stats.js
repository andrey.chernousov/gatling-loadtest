var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "18296",
        "ok": "17101",
        "ko": "1195"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "23"
    },
    "maxResponseTime": {
        "total": "60002",
        "ok": "59950",
        "ko": "60002"
    },
    "meanResponseTime": {
        "total": "2126",
        "ok": "654",
        "ko": "23203"
    },
    "standardDeviation": {
        "total": "10180",
        "ok": "4270",
        "ko": "29162"
    },
    "percentiles1": {
        "total": "26",
        "ok": "26",
        "ko": "177"
    },
    "percentiles2": {
        "total": "143",
        "ok": "106",
        "ko": "60000"
    },
    "percentiles3": {
        "total": "2196",
        "ok": "1046",
        "ko": "60001"
    },
    "percentiles4": {
        "total": "60001",
        "ok": "25870",
        "ko": "60002"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 16077,
    "percentage": 88
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 378,
    "percentage": 2
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 646,
    "percentage": 4
},
    "group4": {
    "name": "failed",
    "count": 1195,
    "percentage": 7
},
    "meanNumberOfRequestsPerSecond": {
        "total": "74.073",
        "ok": "69.235",
        "ko": "4.838"
    }
},
contents: {
"req_login-99dea": {
        type: "REQUEST",
        name: "Login",
path: "Login",
pathFormatted: "req_login-99dea",
stats: {
    "name": "Login",
    "numberOfRequests": {
        "total": "600",
        "ok": "598",
        "ko": "2"
    },
    "minResponseTime": {
        "total": "140",
        "ok": "140",
        "ko": "60000"
    },
    "maxResponseTime": {
        "total": "60001",
        "ok": "4262",
        "ko": "60001"
    },
    "meanResponseTime": {
        "total": "651",
        "ok": "453",
        "ko": "60001"
    },
    "standardDeviation": {
        "total": "3481",
        "ok": "583",
        "ko": "1"
    },
    "percentiles1": {
        "total": "169",
        "ok": "169",
        "ko": "60001"
    },
    "percentiles2": {
        "total": "589",
        "ok": "589",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "1314",
        "ok": "1226",
        "ko": "60001"
    },
    "percentiles4": {
        "total": "3752",
        "ok": "2845",
        "ko": "60001"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 489,
    "percentage": 82
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 75,
    "percentage": 13
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 34,
    "percentage": 6
},
    "group4": {
    "name": "failed",
    "count": 2,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.429",
        "ok": "2.421",
        "ko": "0.008"
    }
}
    },"req_-mnemoschemes-a-0c177": {
        type: "REQUEST",
        name: "/mnemoschemes/api/v1/mnemo/info",
path: "/mnemoschemes/api/v1/mnemo/info",
pathFormatted: "req_-mnemoschemes-a-0c177",
stats: {
    "name": "/mnemoschemes/api/v1/mnemo/info",
    "numberOfRequests": {
        "total": "1191",
        "ok": "824",
        "ko": "367"
    },
    "minResponseTime": {
        "total": "23",
        "ok": "34",
        "ko": "23"
    },
    "maxResponseTime": {
        "total": "11608",
        "ok": "11608",
        "ko": "933"
    },
    "meanResponseTime": {
        "total": "131",
        "ok": "151",
        "ko": "87"
    },
    "standardDeviation": {
        "total": "463",
        "ok": "547",
        "ko": "142"
    },
    "percentiles1": {
        "total": "44",
        "ok": "52",
        "ko": "28"
    },
    "percentiles2": {
        "total": "71",
        "ok": "74",
        "ko": "31"
    },
    "percentiles3": {
        "total": "426",
        "ok": "431",
        "ko": "267"
    },
    "percentiles4": {
        "total": "1072",
        "ok": "1083",
        "ko": "812"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 794,
    "percentage": 67
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 23,
    "percentage": 2
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 7,
    "percentage": 1
},
    "group4": {
    "name": "failed",
    "count": 367,
    "percentage": 31
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.822",
        "ok": "3.336",
        "ko": "1.486"
    }
}
    },"req_post-mnemoschem-dba6c": {
        type: "REQUEST",
        name: "POST/mnemoschemes/api/v1/mnemo",
path: "POST/mnemoschemes/api/v1/mnemo",
pathFormatted: "req_post-mnemoschem-dba6c",
stats: {
    "name": "POST/mnemoschemes/api/v1/mnemo",
    "numberOfRequests": {
        "total": "13",
        "ok": "11",
        "ko": "2"
    },
    "minResponseTime": {
        "total": "28",
        "ok": "28",
        "ko": "28"
    },
    "maxResponseTime": {
        "total": "51",
        "ok": "36",
        "ko": "51"
    },
    "meanResponseTime": {
        "total": "32",
        "ok": "30",
        "ko": "40"
    },
    "standardDeviation": {
        "total": "6",
        "ok": "2",
        "ko": "12"
    },
    "percentiles1": {
        "total": "30",
        "ok": "30",
        "ko": "40"
    },
    "percentiles2": {
        "total": "32",
        "ok": "31",
        "ko": "45"
    },
    "percentiles3": {
        "total": "42",
        "ok": "34",
        "ko": "50"
    },
    "percentiles4": {
        "total": "49",
        "ok": "36",
        "ko": "51"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 11,
    "percentage": 85
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 2,
    "percentage": 15
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.053",
        "ok": "0.045",
        "ko": "0.008"
    }
}
    },"req_get--e0e39": {
        type: "REQUEST",
        name: "GET/",
path: "GET/",
pathFormatted: "req_get--e0e39",
stats: {
    "name": "GET/",
    "numberOfRequests": {
        "total": "500",
        "ok": "500",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1268",
        "ok": "1268",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "61",
        "ok": "61",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "123",
        "ok": "123",
        "ko": "-"
    },
    "percentiles1": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "percentiles2": {
        "total": "26",
        "ok": "26",
        "ko": "-"
    },
    "percentiles3": {
        "total": "259",
        "ok": "259",
        "ko": "-"
    },
    "percentiles4": {
        "total": "699",
        "ok": "699",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 499,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 1,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.024",
        "ok": "2.024",
        "ko": "-"
    }
}
    },"req_get-built-scrip-f7a2f": {
        type: "REQUEST",
        name: "GET/built/scripts/zone.min.js",
path: "GET/built/scripts/zone.min.js",
pathFormatted: "req_get-built-scrip-f7a2f",
stats: {
    "name": "GET/built/scripts/zone.min.js",
    "numberOfRequests": {
        "total": "500",
        "ok": "500",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "22",
        "ok": "22",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4296",
        "ok": "4296",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "238",
        "ok": "238",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "467",
        "ok": "467",
        "ko": "-"
    },
    "percentiles1": {
        "total": "68",
        "ok": "68",
        "ko": "-"
    },
    "percentiles2": {
        "total": "255",
        "ok": "255",
        "ko": "-"
    },
    "percentiles3": {
        "total": "977",
        "ok": "977",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2049",
        "ok": "2049",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 466,
    "percentage": 93
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 17,
    "percentage": 3
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 17,
    "percentage": 3
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.024",
        "ok": "2.024",
        "ko": "-"
    }
}
    },"req_get-built-scrip-6c713": {
        type: "REQUEST",
        name: "GET/built/scripts/import-map-overrides.js",
path: "GET/built/scripts/import-map-overrides.js",
pathFormatted: "req_get-built-scrip-6c713",
stats: {
    "name": "GET/built/scripts/import-map-overrides.js",
    "numberOfRequests": {
        "total": "500",
        "ok": "500",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3825",
        "ok": "3825",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "89",
        "ok": "89",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "244",
        "ok": "244",
        "ko": "-"
    },
    "percentiles1": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "percentiles2": {
        "total": "26",
        "ok": "26",
        "ko": "-"
    },
    "percentiles3": {
        "total": "290",
        "ok": "290",
        "ko": "-"
    },
    "percentiles4": {
        "total": "974",
        "ok": "974",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 493,
    "percentage": 99
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 4,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 3,
    "percentage": 1
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.024",
        "ok": "2.024",
        "ko": "-"
    }
}
    },"req_get-built-scrip-3e0b8": {
        type: "REQUEST",
        name: "GET/built/scripts/system.min.js",
path: "GET/built/scripts/system.min.js",
pathFormatted: "req_get-built-scrip-3e0b8",
stats: {
    "name": "GET/built/scripts/system.min.js",
    "numberOfRequests": {
        "total": "500",
        "ok": "500",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "22",
        "ok": "22",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2117",
        "ok": "2117",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "105",
        "ok": "105",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "206",
        "ok": "206",
        "ko": "-"
    },
    "percentiles1": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "percentiles2": {
        "total": "50",
        "ok": "50",
        "ko": "-"
    },
    "percentiles3": {
        "total": "486",
        "ok": "486",
        "ko": "-"
    },
    "percentiles4": {
        "total": "847",
        "ok": "847",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 493,
    "percentage": 99
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 5,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 2,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.024",
        "ok": "2.024",
        "ko": "-"
    }
}
    },"req_get-built-scrip-a6908": {
        type: "REQUEST",
        name: "GET/built/scripts/amd.min.js",
path: "GET/built/scripts/amd.min.js",
pathFormatted: "req_get-built-scrip-a6908",
stats: {
    "name": "GET/built/scripts/amd.min.js",
    "numberOfRequests": {
        "total": "500",
        "ok": "500",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5597",
        "ok": "5597",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "106",
        "ok": "106",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "361",
        "ok": "361",
        "ko": "-"
    },
    "percentiles1": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "percentiles2": {
        "total": "27",
        "ok": "27",
        "ko": "-"
    },
    "percentiles3": {
        "total": "482",
        "ok": "482",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1194",
        "ok": "1194",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 491,
    "percentage": 98
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 4,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 5,
    "percentage": 1
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.024",
        "ok": "2.024",
        "ko": "-"
    }
}
    },"req_get-built-scrip-a5f03": {
        type: "REQUEST",
        name: "GET/built/scripts/named-exports.js",
path: "GET/built/scripts/named-exports.js",
pathFormatted: "req_get-built-scrip-a5f03",
stats: {
    "name": "GET/built/scripts/named-exports.js",
    "numberOfRequests": {
        "total": "500",
        "ok": "500",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1399",
        "ok": "1399",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "81",
        "ok": "81",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "155",
        "ok": "155",
        "ko": "-"
    },
    "percentiles1": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "percentiles2": {
        "total": "27",
        "ok": "27",
        "ko": "-"
    },
    "percentiles3": {
        "total": "271",
        "ok": "271",
        "ko": "-"
    },
    "percentiles4": {
        "total": "720",
        "ok": "720",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 496,
    "percentage": 99
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 3,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 1,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.024",
        "ok": "2.024",
        "ko": "-"
    }
}
    },"req_get-built-scrip-96ae1": {
        type: "REQUEST",
        name: "GET/built/scripts/named-register.min.js",
path: "GET/built/scripts/named-register.min.js",
pathFormatted: "req_get-built-scrip-96ae1",
stats: {
    "name": "GET/built/scripts/named-register.min.js",
    "numberOfRequests": {
        "total": "500",
        "ok": "500",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1160",
        "ok": "1160",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "72",
        "ok": "72",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "154",
        "ok": "154",
        "ko": "-"
    },
    "percentiles1": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "percentiles2": {
        "total": "26",
        "ok": "26",
        "ko": "-"
    },
    "percentiles3": {
        "total": "331",
        "ok": "331",
        "ko": "-"
    },
    "percentiles4": {
        "total": "784",
        "ok": "784",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 495,
    "percentage": 99
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 5,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.024",
        "ok": "2.024",
        "ko": "-"
    }
}
    },"req_get-built-vendo-cdd2c": {
        type: "REQUEST",
        name: "GET/built/vendors~main.js",
path: "GET/built/vendors~main.js",
pathFormatted: "req_get-built-vendo-cdd2c",
stats: {
    "name": "GET/built/vendors~main.js",
    "numberOfRequests": {
        "total": "500",
        "ok": "500",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "22",
        "ok": "22",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "19716",
        "ok": "19716",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1142",
        "ok": "1142",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "2275",
        "ok": "2275",
        "ko": "-"
    },
    "percentiles1": {
        "total": "58",
        "ok": "51",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1240",
        "ok": "1240",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5442",
        "ok": "5442",
        "ko": "-"
    },
    "percentiles4": {
        "total": "10335",
        "ok": "10335",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 356,
    "percentage": 71
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 15,
    "percentage": 3
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 129,
    "percentage": 26
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.024",
        "ok": "2.024",
        "ko": "-"
    }
}
    },"req_get-built-main--37f57": {
        type: "REQUEST",
        name: "GET/built/main.js",
path: "GET/built/main.js",
pathFormatted: "req_get-built-main--37f57",
stats: {
    "name": "GET/built/main.js",
    "numberOfRequests": {
        "total": "500",
        "ok": "500",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "7644",
        "ok": "7644",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "171",
        "ok": "171",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "495",
        "ok": "495",
        "ko": "-"
    },
    "percentiles1": {
        "total": "26",
        "ok": "26",
        "ko": "-"
    },
    "percentiles2": {
        "total": "100",
        "ok": "100",
        "ko": "-"
    },
    "percentiles3": {
        "total": "710",
        "ok": "710",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2080",
        "ok": "2080",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 479,
    "percentage": 96
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 12,
    "percentage": 2
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 9,
    "percentage": 2
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.024",
        "ok": "2.024",
        "ko": "-"
    }
}
    },"req_get-built-asset-4566d": {
        type: "REQUEST",
        name: "GET/built/assets/settings.json",
path: "GET/built/assets/settings.json",
pathFormatted: "req_get-built-asset-4566d",
stats: {
    "name": "GET/built/assets/settings.json",
    "numberOfRequests": {
        "total": "500",
        "ok": "500",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3825",
        "ok": "3825",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "111",
        "ok": "111",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "298",
        "ok": "298",
        "ko": "-"
    },
    "percentiles1": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "percentiles2": {
        "total": "26",
        "ok": "26",
        "ko": "-"
    },
    "percentiles3": {
        "total": "610",
        "ok": "610",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1107",
        "ok": "1107",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 485,
    "percentage": 97
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 10,
    "percentage": 2
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 5,
    "percentage": 1
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.024",
        "ok": "2.024",
        "ko": "-"
    }
}
    },"req_get-auth-realms-24c57": {
        type: "REQUEST",
        name: "GET/auth/realms/master/account",
path: "GET/auth/realms/master/account",
pathFormatted: "req_get-auth-realms-24c57",
stats: {
    "name": "GET/auth/realms/master/account",
    "numberOfRequests": {
        "total": "500",
        "ok": "500",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "22",
        "ok": "22",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2966",
        "ok": "2966",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "102",
        "ok": "102",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "255",
        "ok": "255",
        "ko": "-"
    },
    "percentiles1": {
        "total": "26",
        "ok": "26",
        "ko": "-"
    },
    "percentiles2": {
        "total": "30",
        "ok": "30",
        "ko": "-"
    },
    "percentiles3": {
        "total": "272",
        "ok": "272",
        "ko": "-"
    },
    "percentiles4": {
        "total": "953",
        "ok": "953",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 492,
    "percentage": 98
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 5,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 3,
    "percentage": 1
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.024",
        "ok": "2.024",
        "ko": "-"
    }
}
    },"req_get-auth-realms-177d8": {
        type: "REQUEST",
        name: "GET/auth/realms/master/account Redirect 1",
path: "GET/auth/realms/master/account Redirect 1",
pathFormatted: "req_get-auth-realms-177d8",
stats: {
    "name": "GET/auth/realms/master/account Redirect 1",
    "numberOfRequests": {
        "total": "500",
        "ok": "500",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "28",
        "ok": "28",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "7506",
        "ok": "7506",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "169",
        "ok": "169",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "468",
        "ok": "468",
        "ko": "-"
    },
    "percentiles1": {
        "total": "57",
        "ok": "57",
        "ko": "-"
    },
    "percentiles2": {
        "total": "86",
        "ok": "86",
        "ko": "-"
    },
    "percentiles3": {
        "total": "617",
        "ok": "617",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1960",
        "ok": "1960",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 479,
    "percentage": 96
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 13,
    "percentage": 3
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 8,
    "percentage": 2
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.024",
        "ok": "2.024",
        "ko": "-"
    }
}
    },"req_get-json-applic-72f39": {
        type: "REQUEST",
        name: "GET/json/applicationUnits",
path: "GET/json/applicationUnits",
pathFormatted: "req_get-json-applic-72f39",
stats: {
    "name": "GET/json/applicationUnits",
    "numberOfRequests": {
        "total": "1000",
        "ok": "1000",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2372",
        "ok": "2372",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "77",
        "ok": "77",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "173",
        "ok": "173",
        "ko": "-"
    },
    "percentiles1": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "percentiles2": {
        "total": "26",
        "ok": "26",
        "ko": "-"
    },
    "percentiles3": {
        "total": "258",
        "ok": "258",
        "ko": "-"
    },
    "percentiles4": {
        "total": "712",
        "ok": "712",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 992,
    "percentage": 99
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 3,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 5,
    "percentage": 1
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.049",
        "ok": "4.049",
        "ko": "-"
    }
}
    },"req_get-built-scrip-2d4a0": {
        type: "REQUEST",
        name: "GET/built/scripts/single-spa.min.js",
path: "GET/built/scripts/single-spa.min.js",
pathFormatted: "req_get-built-scrip-2d4a0",
stats: {
    "name": "GET/built/scripts/single-spa.min.js",
    "numberOfRequests": {
        "total": "500",
        "ok": "500",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "7294",
        "ok": "7294",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "265",
        "ok": "265",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "568",
        "ok": "568",
        "ko": "-"
    },
    "percentiles1": {
        "total": "27",
        "ok": "27",
        "ko": "-"
    },
    "percentiles2": {
        "total": "251",
        "ok": "251",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1166",
        "ok": "1166",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2138",
        "ok": "2138",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 456,
    "percentage": 91
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 20,
    "percentage": 4
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 24,
    "percentage": 5
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.024",
        "ok": "2.024",
        "ko": "-"
    }
}
    },"req_get-json-userse-56296": {
        type: "REQUEST",
        name: "GET/json/UserSettings",
path: "GET/json/UserSettings",
pathFormatted: "req_get-json-userse-56296",
stats: {
    "name": "GET/json/UserSettings",
    "numberOfRequests": {
        "total": "500",
        "ok": "500",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3690",
        "ok": "3690",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "85",
        "ok": "85",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "228",
        "ok": "228",
        "ko": "-"
    },
    "percentiles1": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "percentiles2": {
        "total": "26",
        "ok": "26",
        "ko": "-"
    },
    "percentiles3": {
        "total": "267",
        "ok": "267",
        "ko": "-"
    },
    "percentiles4": {
        "total": "706",
        "ok": "706",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 497,
    "percentage": 99
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 2,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.024",
        "ok": "2.024",
        "ko": "-"
    }
}
    },"req_get-json-userco-f4ad0": {
        type: "REQUEST",
        name: "GET/json/UserCommonApplications",
path: "GET/json/UserCommonApplications",
pathFormatted: "req_get-json-userco-f4ad0",
stats: {
    "name": "GET/json/UserCommonApplications",
    "numberOfRequests": {
        "total": "500",
        "ok": "500",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3897",
        "ok": "3897",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "104",
        "ok": "104",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "267",
        "ok": "267",
        "ko": "-"
    },
    "percentiles1": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "percentiles2": {
        "total": "27",
        "ok": "27",
        "ko": "-"
    },
    "percentiles3": {
        "total": "701",
        "ok": "701",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1141",
        "ok": "1141",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 486,
    "percentage": 97
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 12,
    "percentage": 2
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 2,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.024",
        "ok": "2.024",
        "ko": "-"
    }
}
    },"req_get-hostrootshe-d0154": {
        type: "REQUEST",
        name: "GET/hostRootShell/assets/i18n/en_US.json",
path: "GET/hostRootShell/assets/i18n/en_US.json",
pathFormatted: "req_get-hostrootshe-d0154",
stats: {
    "name": "GET/hostRootShell/assets/i18n/en_US.json",
    "numberOfRequests": {
        "total": "500",
        "ok": "500",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "22",
        "ok": "22",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3894",
        "ok": "3894",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "88",
        "ok": "88",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "236",
        "ok": "236",
        "ko": "-"
    },
    "percentiles1": {
        "total": "26",
        "ok": "26",
        "ko": "-"
    },
    "percentiles2": {
        "total": "27",
        "ok": "27",
        "ko": "-"
    },
    "percentiles3": {
        "total": "253",
        "ok": "253",
        "ko": "-"
    },
    "percentiles4": {
        "total": "929",
        "ok": "929",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 492,
    "percentage": 98
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 6,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 2,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.024",
        "ok": "2.024",
        "ko": "-"
    }
}
    },"req_get-hostmnemo-s-7aeb7": {
        type: "REQUEST",
        name: "GET/hostMnemo/scripts.js",
path: "GET/hostMnemo/scripts.js",
pathFormatted: "req_get-hostmnemo-s-7aeb7",
stats: {
    "name": "GET/hostMnemo/scripts.js",
    "numberOfRequests": {
        "total": "500",
        "ok": "491",
        "ko": "9"
    },
    "minResponseTime": {
        "total": "22",
        "ok": "22",
        "ko": "60000"
    },
    "maxResponseTime": {
        "total": "60001",
        "ok": "54621",
        "ko": "60001"
    },
    "meanResponseTime": {
        "total": "12350",
        "ok": "11476",
        "ko": "60000"
    },
    "standardDeviation": {
        "total": "16285",
        "ok": "15089",
        "ko": "0"
    },
    "percentiles1": {
        "total": "251",
        "ok": "251",
        "ko": "60000"
    },
    "percentiles2": {
        "total": "26326",
        "ok": "25757",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "41237",
        "ok": "39922",
        "ko": "60001"
    },
    "percentiles4": {
        "total": "60000",
        "ok": "47416",
        "ko": "60001"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 287,
    "percentage": 57
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 3,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 201,
    "percentage": 40
},
    "group4": {
    "name": "failed",
    "count": 9,
    "percentage": 2
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.024",
        "ok": "1.988",
        "ko": "0.036"
    }
}
    },"req_get-mnemo-css-c-7a130": {
        type: "REQUEST",
        name: "GET/mnemo/css/common.css",
path: "GET/mnemo/css/common.css",
pathFormatted: "req_get-mnemo-css-c-7a130",
stats: {
    "name": "GET/mnemo/css/common.css",
    "numberOfRequests": {
        "total": "500",
        "ok": "500",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "23",
        "ok": "23",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "30832",
        "ok": "30832",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "181",
        "ok": "181",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "1423",
        "ok": "1423",
        "ko": "-"
    },
    "percentiles1": {
        "total": "26",
        "ok": "26",
        "ko": "-"
    },
    "percentiles2": {
        "total": "49",
        "ok": "49",
        "ko": "-"
    },
    "percentiles3": {
        "total": "560",
        "ok": "560",
        "ko": "-"
    },
    "percentiles4": {
        "total": "938",
        "ok": "938",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 490,
    "percentage": 98
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 6,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 4,
    "percentage": 1
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.024",
        "ok": "2.024",
        "ko": "-"
    }
}
    },"req_get-mnemo-resou-6daa7": {
        type: "REQUEST",
        name: "GET/mnemo/resources/graph.txt",
path: "GET/mnemo/resources/graph.txt",
pathFormatted: "req_get-mnemo-resou-6daa7",
stats: {
    "name": "GET/mnemo/resources/graph.txt",
    "numberOfRequests": {
        "total": "500",
        "ok": "500",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "22",
        "ok": "22",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2287",
        "ok": "2287",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "100",
        "ok": "100",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "213",
        "ok": "213",
        "ko": "-"
    },
    "percentiles1": {
        "total": "26",
        "ok": "26",
        "ko": "-"
    },
    "percentiles2": {
        "total": "27",
        "ok": "27",
        "ko": "-"
    },
    "percentiles3": {
        "total": "475",
        "ok": "475",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1156",
        "ok": "1156",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 491,
    "percentage": 98
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 5,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 4,
    "percentage": 1
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.024",
        "ok": "2.024",
        "ko": "-"
    }
}
    },"req_get-mnemo-resou-95e3c": {
        type: "REQUEST",
        name: "GET/mnemo/resources/graph_ru.txt",
path: "GET/mnemo/resources/graph_ru.txt",
pathFormatted: "req_get-mnemo-resou-95e3c",
stats: {
    "name": "GET/mnemo/resources/graph_ru.txt",
    "numberOfRequests": {
        "total": "500",
        "ok": "500",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "22",
        "ok": "22",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1256",
        "ok": "1256",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "101",
        "ok": "101",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "199",
        "ok": "199",
        "ko": "-"
    },
    "percentiles1": {
        "total": "26",
        "ok": "26",
        "ko": "-"
    },
    "percentiles2": {
        "total": "27",
        "ok": "27",
        "ko": "-"
    },
    "percentiles3": {
        "total": "499",
        "ok": "499",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1156",
        "ok": "1156",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 491,
    "percentage": 98
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 7,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 2,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.024",
        "ok": "2.024",
        "ko": "-"
    }
}
    },"req_get-mnemo-resou-d3607": {
        type: "REQUEST",
        name: "GET/mnemo/resources/editor.txt",
path: "GET/mnemo/resources/editor.txt",
pathFormatted: "req_get-mnemo-resou-d3607",
stats: {
    "name": "GET/mnemo/resources/editor.txt",
    "numberOfRequests": {
        "total": "500",
        "ok": "500",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "22",
        "ok": "22",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2052",
        "ok": "2052",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "95",
        "ok": "95",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "213",
        "ok": "213",
        "ko": "-"
    },
    "percentiles1": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "percentiles2": {
        "total": "26",
        "ok": "26",
        "ko": "-"
    },
    "percentiles3": {
        "total": "476",
        "ok": "476",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1156",
        "ok": "1156",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 489,
    "percentage": 98
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 8,
    "percentage": 2
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 3,
    "percentage": 1
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.024",
        "ok": "2.024",
        "ko": "-"
    }
}
    },"req_get-mnemo-resou-03f4c": {
        type: "REQUEST",
        name: "GET/mnemo/resources/editor_ru.txt",
path: "GET/mnemo/resources/editor_ru.txt",
pathFormatted: "req_get-mnemo-resou-03f4c",
stats: {
    "name": "GET/mnemo/resources/editor_ru.txt",
    "numberOfRequests": {
        "total": "500",
        "ok": "500",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "22",
        "ok": "22",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2062",
        "ok": "2062",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "88",
        "ok": "88",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "178",
        "ok": "178",
        "ko": "-"
    },
    "percentiles1": {
        "total": "26",
        "ok": "26",
        "ko": "-"
    },
    "percentiles2": {
        "total": "26",
        "ok": "26",
        "ko": "-"
    },
    "percentiles3": {
        "total": "281",
        "ok": "281",
        "ko": "-"
    },
    "percentiles4": {
        "total": "718",
        "ok": "718",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 497,
    "percentage": 99
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 2,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 1,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.024",
        "ok": "2.024",
        "ko": "-"
    }
}
    },"req_get-hostmnemo-m-22113": {
        type: "REQUEST",
        name: "GET/hostMnemo/main.js",
path: "GET/hostMnemo/main.js",
pathFormatted: "req_get-hostmnemo-m-22113",
stats: {
    "name": "GET/hostMnemo/main.js",
    "numberOfRequests": {
        "total": "500",
        "ok": "50",
        "ko": "450"
    },
    "minResponseTime": {
        "total": "24",
        "ok": "24",
        "ko": "60000"
    },
    "maxResponseTime": {
        "total": "60002",
        "ok": "59950",
        "ko": "60002"
    },
    "meanResponseTime": {
        "total": "59011",
        "ok": "50111",
        "ko": "60000"
    },
    "standardDeviation": {
        "total": "5065",
        "ok": "12981",
        "ko": "1"
    },
    "percentiles1": {
        "total": "60000",
        "ok": "55707",
        "ko": "60000"
    },
    "percentiles2": {
        "total": "60001",
        "ok": "57573",
        "ko": "60001"
    },
    "percentiles3": {
        "total": "60001",
        "ok": "59530",
        "ko": "60001"
    },
    "percentiles4": {
        "total": "60002",
        "ok": "59923",
        "ko": "60002"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 49,
    "percentage": 10
},
    "group4": {
    "name": "failed",
    "count": 450,
    "percentage": 90
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.024",
        "ok": "0.202",
        "ko": "1.822"
    }
}
    },"req_get-hostmnemo-a-913f4": {
        type: "REQUEST",
        name: "GET/hostMnemo/assets/i18n/en_US.json",
path: "GET/hostMnemo/assets/i18n/en_US.json",
pathFormatted: "req_get-hostmnemo-a-913f4",
stats: {
    "name": "GET/hostMnemo/assets/i18n/en_US.json",
    "numberOfRequests": {
        "total": "500",
        "ok": "500",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5594",
        "ok": "5594",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "339",
        "ok": "339",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "752",
        "ok": "752",
        "ko": "-"
    },
    "percentiles1": {
        "total": "51",
        "ok": "51",
        "ko": "-"
    },
    "percentiles2": {
        "total": "64",
        "ok": "64",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1279",
        "ok": "1279",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4055",
        "ok": "4055",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 417,
    "percentage": 83
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 54,
    "percentage": 11
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 29,
    "percentage": 6
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.024",
        "ok": "2.024",
        "ko": "-"
    }
}
    },"req_get-hostmnemo-a-19ffa": {
        type: "REQUEST",
        name: "GET/hostMnemo/assets/i18n/zyfra-mnemo/zyfraMnemoModule.en_US.json",
path: "GET/hostMnemo/assets/i18n/zyfra-mnemo/zyfraMnemoModule.en_US.json",
pathFormatted: "req_get-hostmnemo-a-19ffa",
stats: {
    "name": "GET/hostMnemo/assets/i18n/zyfra-mnemo/zyfraMnemoModule.en_US.json",
    "numberOfRequests": {
        "total": "500",
        "ok": "500",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "6297",
        "ok": "6297",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "124",
        "ok": "124",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "384",
        "ok": "384",
        "ko": "-"
    },
    "percentiles1": {
        "total": "26",
        "ok": "26",
        "ko": "-"
    },
    "percentiles2": {
        "total": "34",
        "ok": "34",
        "ko": "-"
    },
    "percentiles3": {
        "total": "473",
        "ok": "473",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1153",
        "ok": "1153",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 487,
    "percentage": 97
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 8,
    "percentage": 2
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 5,
    "percentage": 1
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.024",
        "ok": "2.024",
        "ko": "-"
    }
}
    },"req_get-hostmnemo-a-5dc3d": {
        type: "REQUEST",
        name: "GET/hostMnemo/assets/settings.json",
path: "GET/hostMnemo/assets/settings.json",
pathFormatted: "req_get-hostmnemo-a-5dc3d",
stats: {
    "name": "GET/hostMnemo/assets/settings.json",
    "numberOfRequests": {
        "total": "500",
        "ok": "500",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1156",
        "ok": "1156",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "65",
        "ok": "65",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "135",
        "ok": "135",
        "ko": "-"
    },
    "percentiles1": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "percentiles2": {
        "total": "26",
        "ok": "26",
        "ko": "-"
    },
    "percentiles3": {
        "total": "255",
        "ok": "255",
        "ko": "-"
    },
    "percentiles4": {
        "total": "708",
        "ok": "708",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 496,
    "percentage": 99
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 4,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.024",
        "ok": "2.024",
        "ko": "-"
    }
}
    },"req_get-mnemo-resou-b8f0f": {
        type: "REQUEST",
        name: "GET/mnemo/resources/grapheditor.txt",
path: "GET/mnemo/resources/grapheditor.txt",
pathFormatted: "req_get-mnemo-resou-b8f0f",
stats: {
    "name": "GET/mnemo/resources/grapheditor.txt",
    "numberOfRequests": {
        "total": "500",
        "ok": "500",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2040",
        "ok": "2040",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "113",
        "ok": "113",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "243",
        "ok": "243",
        "ko": "-"
    },
    "percentiles1": {
        "total": "26",
        "ok": "26",
        "ko": "-"
    },
    "percentiles2": {
        "total": "37",
        "ok": "37",
        "ko": "-"
    },
    "percentiles3": {
        "total": "474",
        "ok": "474",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1092",
        "ok": "1092",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 484,
    "percentage": 97
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 12,
    "percentage": 2
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 4,
    "percentage": 1
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.024",
        "ok": "2.024",
        "ko": "-"
    }
}
    },"req_get-mnemo-style-4f382": {
        type: "REQUEST",
        name: "GET/mnemo/styles/default.xml",
path: "GET/mnemo/styles/default.xml",
pathFormatted: "req_get-mnemo-style-4f382",
stats: {
    "name": "GET/mnemo/styles/default.xml",
    "numberOfRequests": {
        "total": "798",
        "ok": "798",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1606",
        "ok": "1606",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "68",
        "ok": "68",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "140",
        "ok": "140",
        "ko": "-"
    },
    "percentiles1": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "percentiles2": {
        "total": "26",
        "ok": "26",
        "ko": "-"
    },
    "percentiles3": {
        "total": "258",
        "ok": "258",
        "ko": "-"
    },
    "percentiles4": {
        "total": "701",
        "ok": "701",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 791,
    "percentage": 99
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 6,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 1,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.231",
        "ok": "3.231",
        "ko": "-"
    }
}
    },"req_get-mnemo-stenc-e38f6": {
        type: "REQUEST",
        name: "GET/mnemo/stencils/mnemo.library.xml",
path: "GET/mnemo/stencils/mnemo.library.xml",
pathFormatted: "req_get-mnemo-stenc-e38f6",
stats: {
    "name": "GET/mnemo/stencils/mnemo.library.xml",
    "numberOfRequests": {
        "total": "500",
        "ok": "500",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "31993",
        "ok": "31993",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "708",
        "ok": "708",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "1927",
        "ok": "1927",
        "ko": "-"
    },
    "percentiles1": {
        "total": "28",
        "ok": "28",
        "ko": "-"
    },
    "percentiles2": {
        "total": "591",
        "ok": "591",
        "ko": "-"
    },
    "percentiles3": {
        "total": "3334",
        "ok": "3334",
        "ko": "-"
    },
    "percentiles4": {
        "total": "8118",
        "ok": "8118",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 396,
    "percentage": 79
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 25,
    "percentage": 5
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 79,
    "percentage": 16
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.024",
        "ok": "2.024",
        "ko": "-"
    }
}
    },"req_get-mnemoscheme-b6811": {
        type: "REQUEST",
        name: "GET/mnemoschemes/api/v1/mnemo/",
path: "GET/mnemoschemes/api/v1/mnemo/",
pathFormatted: "req_get-mnemoscheme-b6811",
stats: {
    "name": "GET/mnemoschemes/api/v1/mnemo/",
    "numberOfRequests": {
        "total": "298",
        "ok": "14",
        "ko": "284"
    },
    "minResponseTime": {
        "total": "23",
        "ok": "35",
        "ko": "23"
    },
    "maxResponseTime": {
        "total": "1006",
        "ok": "1006",
        "ko": "929"
    },
    "meanResponseTime": {
        "total": "85",
        "ok": "183",
        "ko": "80"
    },
    "standardDeviation": {
        "total": "146",
        "ok": "251",
        "ko": "137"
    },
    "percentiles1": {
        "total": "28",
        "ok": "66",
        "ko": "28"
    },
    "percentiles2": {
        "total": "33",
        "ok": "210",
        "ko": "29"
    },
    "percentiles3": {
        "total": "474",
        "ok": "598",
        "ko": "445"
    },
    "percentiles4": {
        "total": "722",
        "ok": "924",
        "ko": "541"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 13,
    "percentage": 4
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 284,
    "percentage": 95
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.206",
        "ok": "0.057",
        "ko": "1.15"
    }
}
    },"req_get-mnemo-resou-96eb1": {
        type: "REQUEST",
        name: "GET/mnemo/resources/grapheditor_ru.txt",
path: "GET/mnemo/resources/grapheditor_ru.txt",
pathFormatted: "req_get-mnemo-resou-96eb1",
stats: {
    "name": "GET/mnemo/resources/grapheditor_ru.txt",
    "numberOfRequests": {
        "total": "298",
        "ok": "298",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "22",
        "ok": "22",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "7151",
        "ok": "7151",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "187",
        "ok": "187",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "511",
        "ok": "511",
        "ko": "-"
    },
    "percentiles1": {
        "total": "52",
        "ok": "52",
        "ko": "-"
    },
    "percentiles2": {
        "total": "171",
        "ok": "171",
        "ko": "-"
    },
    "percentiles3": {
        "total": "509",
        "ok": "509",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1809",
        "ok": "1809",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 289,
    "percentage": 97
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 4,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 5,
    "percentage": 2
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.206",
        "ok": "1.206",
        "ko": "-"
    }
}
    },"req_put-mnemoscheme-aabbe": {
        type: "REQUEST",
        name: "PUT/mnemoschemes/api/v1/mnemo",
path: "PUT/mnemoschemes/api/v1/mnemo",
pathFormatted: "req_put-mnemoscheme-aabbe",
stats: {
    "name": "PUT/mnemoschemes/api/v1/mnemo",
    "numberOfRequests": {
        "total": "98",
        "ok": "17",
        "ko": "81"
    },
    "minResponseTime": {
        "total": "38",
        "ok": "55",
        "ko": "38"
    },
    "maxResponseTime": {
        "total": "776",
        "ok": "103",
        "ko": "776"
    },
    "meanResponseTime": {
        "total": "138",
        "ok": "72",
        "ko": "152"
    },
    "standardDeviation": {
        "total": "134",
        "ok": "13",
        "ko": "143"
    },
    "percentiles1": {
        "total": "83",
        "ok": "69",
        "ko": "83"
    },
    "percentiles2": {
        "total": "106",
        "ok": "78",
        "ko": "138"
    },
    "percentiles3": {
        "total": "330",
        "ok": "97",
        "ko": "333"
    },
    "percentiles4": {
        "total": "760",
        "ok": "102",
        "ko": "763"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 17,
    "percentage": 17
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 81,
    "percentage": 83
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.397",
        "ok": "0.069",
        "ko": "0.328"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
