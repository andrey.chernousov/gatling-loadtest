var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "1829",
        "ok": "1779",
        "ko": "50"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "24"
    },
    "maxResponseTime": {
        "total": "13990",
        "ok": "13990",
        "ko": "409"
    },
    "meanResponseTime": {
        "total": "194",
        "ok": "198",
        "ko": "37"
    },
    "standardDeviation": {
        "total": "1058",
        "ok": "1073",
        "ko": "53"
    },
    "percentiles1": {
        "total": "25",
        "ok": "24",
        "ko": "28"
    },
    "percentiles2": {
        "total": "37",
        "ok": "38",
        "ko": "32"
    },
    "percentiles3": {
        "total": "413",
        "ok": "431",
        "ko": "42"
    },
    "percentiles4": {
        "total": "7639",
        "ok": "7976",
        "ko": "235"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1729,
    "percentage": 95
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 8,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 42,
    "percentage": 2
},
    "group4": {
    "name": "failed",
    "count": 50,
    "percentage": 3
},
    "meanNumberOfRequestsPerSecond": {
        "total": "67.741",
        "ok": "65.889",
        "ko": "1.852"
    }
},
contents: {
"req_login-99dea": {
        type: "REQUEST",
        name: "Login",
path: "Login",
pathFormatted: "req_login-99dea",
stats: {
    "name": "Login",
    "numberOfRequests": {
        "total": "60",
        "ok": "60",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "101",
        "ok": "101",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1156",
        "ok": "1156",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "198",
        "ok": "198",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "210",
        "ok": "210",
        "ko": "-"
    },
    "percentiles1": {
        "total": "128",
        "ok": "128",
        "ko": "-"
    },
    "percentiles2": {
        "total": "145",
        "ok": "145",
        "ko": "-"
    },
    "percentiles3": {
        "total": "515",
        "ok": "515",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1141",
        "ok": "1141",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 58,
    "percentage": 97
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 2,
    "percentage": 3
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.222",
        "ok": "2.222",
        "ko": "-"
    }
}
    },"req_get-mnemobase-a-1be13": {
        type: "REQUEST",
        name: "GET/mnemobase/api/v1/mnemo/info",
path: "GET/mnemobase/api/v1/mnemo/info",
pathFormatted: "req_get-mnemobase-a-1be13",
stats: {
    "name": "GET/mnemobase/api/v1/mnemo/info",
    "numberOfRequests": {
        "total": "119",
        "ok": "119",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "56",
        "ok": "56",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1142",
        "ok": "1142",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "190",
        "ok": "190",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "166",
        "ok": "166",
        "ko": "-"
    },
    "percentiles1": {
        "total": "132",
        "ok": "132",
        "ko": "-"
    },
    "percentiles2": {
        "total": "185",
        "ok": "185",
        "ko": "-"
    },
    "percentiles3": {
        "total": "549",
        "ok": "549",
        "ko": "-"
    },
    "percentiles4": {
        "total": "603",
        "ok": "603",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 118,
    "percentage": 99
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.407",
        "ok": "4.407",
        "ko": "-"
    }
}
    },"req_get--e0e39": {
        type: "REQUEST",
        name: "GET/",
path: "GET/",
pathFormatted: "req_get--e0e39",
stats: {
    "name": "GET/",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "22",
        "ok": "22",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3059",
        "ok": "3059",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "178",
        "ok": "178",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "591",
        "ok": "591",
        "ko": "-"
    },
    "percentiles1": {
        "total": "47",
        "ok": "47",
        "ko": "-"
    },
    "percentiles2": {
        "total": "51",
        "ok": "51",
        "ko": "-"
    },
    "percentiles3": {
        "total": "393",
        "ok": "393",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3054",
        "ok": "3054",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 48,
    "percentage": 96
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 2,
    "percentage": 4
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.852",
        "ok": "1.852",
        "ko": "-"
    }
}
    },"req_get-built-scrip-f7a2f": {
        type: "REQUEST",
        name: "GET/built/scripts/zone.min.js",
path: "GET/built/scripts/zone.min.js",
pathFormatted: "req_get-built-scrip-f7a2f",
stats: {
    "name": "GET/built/scripts/zone.min.js",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "342",
        "ok": "342",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "50",
        "ok": "50",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "60",
        "ok": "60",
        "ko": "-"
    },
    "percentiles1": {
        "total": "27",
        "ok": "27",
        "ko": "-"
    },
    "percentiles2": {
        "total": "48",
        "ok": "48",
        "ko": "-"
    },
    "percentiles3": {
        "total": "148",
        "ok": "148",
        "ko": "-"
    },
    "percentiles4": {
        "total": "306",
        "ok": "306",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.852",
        "ok": "1.852",
        "ko": "-"
    }
}
    },"req_get-built-scrip-6c713": {
        type: "REQUEST",
        name: "GET/built/scripts/import-map-overrides.js",
path: "GET/built/scripts/import-map-overrides.js",
pathFormatted: "req_get-built-scrip-6c713",
stats: {
    "name": "GET/built/scripts/import-map-overrides.js",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "393",
        "ok": "393",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "43",
        "ok": "43",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "77",
        "ok": "77",
        "ko": "-"
    },
    "percentiles1": {
        "total": "23",
        "ok": "23",
        "ko": "-"
    },
    "percentiles2": {
        "total": "24",
        "ok": "24",
        "ko": "-"
    },
    "percentiles3": {
        "total": "157",
        "ok": "157",
        "ko": "-"
    },
    "percentiles4": {
        "total": "387",
        "ok": "387",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.852",
        "ok": "1.852",
        "ko": "-"
    }
}
    },"req_get-built-scrip-3e0b8": {
        type: "REQUEST",
        name: "GET/built/scripts/system.min.js",
path: "GET/built/scripts/system.min.js",
pathFormatted: "req_get-built-scrip-3e0b8",
stats: {
    "name": "GET/built/scripts/system.min.js",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "454",
        "ok": "454",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "42",
        "ok": "42",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "81",
        "ok": "81",
        "ko": "-"
    },
    "percentiles1": {
        "total": "23",
        "ok": "23",
        "ko": "-"
    },
    "percentiles2": {
        "total": "26",
        "ok": "26",
        "ko": "-"
    },
    "percentiles3": {
        "total": "45",
        "ok": "45",
        "ko": "-"
    },
    "percentiles4": {
        "total": "438",
        "ok": "438",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.852",
        "ok": "1.852",
        "ko": "-"
    }
}
    },"req_get-built-scrip-a6908": {
        type: "REQUEST",
        name: "GET/built/scripts/amd.min.js",
path: "GET/built/scripts/amd.min.js",
pathFormatted: "req_get-built-scrip-a6908",
stats: {
    "name": "GET/built/scripts/amd.min.js",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "275",
        "ok": "275",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "30",
        "ok": "30",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "35",
        "ok": "35",
        "ko": "-"
    },
    "percentiles1": {
        "total": "24",
        "ok": "24",
        "ko": "-"
    },
    "percentiles2": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "percentiles3": {
        "total": "39",
        "ok": "39",
        "ko": "-"
    },
    "percentiles4": {
        "total": "160",
        "ok": "160",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.852",
        "ok": "1.852",
        "ko": "-"
    }
}
    },"req_get-built-scrip-a5f03": {
        type: "REQUEST",
        name: "GET/built/scripts/named-exports.js",
path: "GET/built/scripts/named-exports.js",
pathFormatted: "req_get-built-scrip-a5f03",
stats: {
    "name": "GET/built/scripts/named-exports.js",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "42",
        "ok": "42",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "24",
        "ok": "24",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "4",
        "ok": "4",
        "ko": "-"
    },
    "percentiles1": {
        "total": "23",
        "ok": "23",
        "ko": "-"
    },
    "percentiles2": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "percentiles3": {
        "total": "32",
        "ok": "32",
        "ko": "-"
    },
    "percentiles4": {
        "total": "39",
        "ok": "39",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.852",
        "ok": "1.852",
        "ko": "-"
    }
}
    },"req_get-built-scrip-96ae1": {
        type: "REQUEST",
        name: "GET/built/scripts/named-register.min.js",
path: "GET/built/scripts/named-register.min.js",
pathFormatted: "req_get-built-scrip-96ae1",
stats: {
    "name": "GET/built/scripts/named-register.min.js",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "38",
        "ok": "38",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "24",
        "ok": "24",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "4",
        "ok": "4",
        "ko": "-"
    },
    "percentiles1": {
        "total": "23",
        "ok": "23",
        "ko": "-"
    },
    "percentiles2": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "percentiles3": {
        "total": "35",
        "ok": "35",
        "ko": "-"
    },
    "percentiles4": {
        "total": "38",
        "ok": "38",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.852",
        "ok": "1.852",
        "ko": "-"
    }
}
    },"req_get-built-vendo-cdd2c": {
        type: "REQUEST",
        name: "GET/built/vendors~main.js",
path: "GET/built/vendors~main.js",
pathFormatted: "req_get-built-vendo-cdd2c",
stats: {
    "name": "GET/built/vendors~main.js",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2995",
        "ok": "2995",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "329",
        "ok": "329",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "576",
        "ok": "576",
        "ko": "-"
    },
    "percentiles1": {
        "total": "27",
        "ok": "27",
        "ko": "-"
    },
    "percentiles2": {
        "total": "477",
        "ok": "477",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1414",
        "ok": "1414",
        "ko": "-"
    },
    "percentiles4": {
        "total": "2500",
        "ok": "2500",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 43,
    "percentage": 86
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 3,
    "percentage": 6
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 4,
    "percentage": 8
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.852",
        "ok": "1.852",
        "ko": "-"
    }
}
    },"req_get-built-main--37f57": {
        type: "REQUEST",
        name: "GET/built/main.js",
path: "GET/built/main.js",
pathFormatted: "req_get-built-main--37f57",
stats: {
    "name": "GET/built/main.js",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "333",
        "ok": "333",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "37",
        "ok": "37",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "55",
        "ok": "55",
        "ko": "-"
    },
    "percentiles1": {
        "total": "24",
        "ok": "24",
        "ko": "-"
    },
    "percentiles2": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "percentiles3": {
        "total": "50",
        "ok": "50",
        "ko": "-"
    },
    "percentiles4": {
        "total": "301",
        "ok": "301",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.852",
        "ok": "1.852",
        "ko": "-"
    }
}
    },"req_get-built-asset-4566d": {
        type: "REQUEST",
        name: "GET/built/assets/settings.json",
path: "GET/built/assets/settings.json",
pathFormatted: "req_get-built-asset-4566d",
stats: {
    "name": "GET/built/assets/settings.json",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "39",
        "ok": "39",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "24",
        "ok": "24",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "3",
        "ok": "3",
        "ko": "-"
    },
    "percentiles1": {
        "total": "23",
        "ok": "23",
        "ko": "-"
    },
    "percentiles2": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "percentiles3": {
        "total": "29",
        "ok": "29",
        "ko": "-"
    },
    "percentiles4": {
        "total": "38",
        "ok": "38",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.852",
        "ok": "1.852",
        "ko": "-"
    }
}
    },"req_get-auth-realms-24c57": {
        type: "REQUEST",
        name: "GET/auth/realms/master/account",
path: "GET/auth/realms/master/account",
pathFormatted: "req_get-auth-realms-24c57",
stats: {
    "name": "GET/auth/realms/master/account",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "23",
        "ok": "23",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "265",
        "ok": "265",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "33",
        "ok": "33",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "34",
        "ok": "34",
        "ko": "-"
    },
    "percentiles1": {
        "total": "27",
        "ok": "27",
        "ko": "-"
    },
    "percentiles2": {
        "total": "30",
        "ok": "30",
        "ko": "-"
    },
    "percentiles3": {
        "total": "39",
        "ok": "39",
        "ko": "-"
    },
    "percentiles4": {
        "total": "160",
        "ok": "160",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.852",
        "ok": "1.852",
        "ko": "-"
    }
}
    },"req_get-auth-realms-177d8": {
        type: "REQUEST",
        name: "GET/auth/realms/master/account Redirect 1",
path: "GET/auth/realms/master/account Redirect 1",
pathFormatted: "req_get-auth-realms-177d8",
stats: {
    "name": "GET/auth/realms/master/account Redirect 1",
    "numberOfRequests": {
        "total": "50",
        "ok": "0",
        "ko": "50"
    },
    "minResponseTime": {
        "total": "24",
        "ok": "-",
        "ko": "24"
    },
    "maxResponseTime": {
        "total": "409",
        "ok": "-",
        "ko": "409"
    },
    "meanResponseTime": {
        "total": "37",
        "ok": "-",
        "ko": "37"
    },
    "standardDeviation": {
        "total": "53",
        "ok": "-",
        "ko": "53"
    },
    "percentiles1": {
        "total": "28",
        "ok": "-",
        "ko": "28"
    },
    "percentiles2": {
        "total": "32",
        "ok": "-",
        "ko": "32"
    },
    "percentiles3": {
        "total": "42",
        "ok": "-",
        "ko": "42"
    },
    "percentiles4": {
        "total": "235",
        "ok": "-",
        "ko": "235"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 50,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.852",
        "ok": "-",
        "ko": "1.852"
    }
}
    },"req_get-json-applic-72f39": {
        type: "REQUEST",
        name: "GET/json/applicationUnits",
path: "GET/json/applicationUnits",
pathFormatted: "req_get-json-applic-72f39",
stats: {
    "name": "GET/json/applicationUnits",
    "numberOfRequests": {
        "total": "100",
        "ok": "100",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "397",
        "ok": "397",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "42",
        "ok": "42",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "68",
        "ok": "68",
        "ko": "-"
    },
    "percentiles1": {
        "total": "23",
        "ok": "23",
        "ko": "-"
    },
    "percentiles2": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "percentiles3": {
        "total": "256",
        "ok": "256",
        "ko": "-"
    },
    "percentiles4": {
        "total": "280",
        "ok": "280",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 100,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.704",
        "ok": "3.704",
        "ko": "-"
    }
}
    },"req_get-built-scrip-2d4a0": {
        type: "REQUEST",
        name: "GET/built/scripts/single-spa.min.js",
path: "GET/built/scripts/single-spa.min.js",
pathFormatted: "req_get-built-scrip-2d4a0",
stats: {
    "name": "GET/built/scripts/single-spa.min.js",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "471",
        "ok": "471",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "41",
        "ok": "41",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "64",
        "ok": "64",
        "ko": "-"
    },
    "percentiles1": {
        "total": "24",
        "ok": "24",
        "ko": "-"
    },
    "percentiles2": {
        "total": "36",
        "ok": "36",
        "ko": "-"
    },
    "percentiles3": {
        "total": "72",
        "ok": "72",
        "ko": "-"
    },
    "percentiles4": {
        "total": "287",
        "ok": "287",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.852",
        "ok": "1.852",
        "ko": "-"
    }
}
    },"req_get-json-userse-56296": {
        type: "REQUEST",
        name: "GET/json/UserSettings",
path: "GET/json/UserSettings",
pathFormatted: "req_get-json-userse-56296",
stats: {
    "name": "GET/json/UserSettings",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "35",
        "ok": "35",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "24",
        "ok": "24",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "3",
        "ok": "3",
        "ko": "-"
    },
    "percentiles1": {
        "total": "23",
        "ok": "23",
        "ko": "-"
    },
    "percentiles2": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "percentiles3": {
        "total": "28",
        "ok": "28",
        "ko": "-"
    },
    "percentiles4": {
        "total": "34",
        "ok": "34",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.852",
        "ok": "1.852",
        "ko": "-"
    }
}
    },"req_get-json-userco-f4ad0": {
        type: "REQUEST",
        name: "GET/json/UserCommonApplications",
path: "GET/json/UserCommonApplications",
pathFormatted: "req_get-json-userco-f4ad0",
stats: {
    "name": "GET/json/UserCommonApplications",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "274",
        "ok": "274",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "29",
        "ok": "29",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "35",
        "ok": "35",
        "ko": "-"
    },
    "percentiles1": {
        "total": "23",
        "ok": "23",
        "ko": "-"
    },
    "percentiles2": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "percentiles3": {
        "total": "37",
        "ok": "37",
        "ko": "-"
    },
    "percentiles4": {
        "total": "159",
        "ok": "159",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.852",
        "ok": "1.852",
        "ko": "-"
    }
}
    },"req_get-hostrootshe-d0154": {
        type: "REQUEST",
        name: "GET/hostRootShell/assets/i18n/en_US.json",
path: "GET/hostRootShell/assets/i18n/en_US.json",
pathFormatted: "req_get-hostrootshe-d0154",
stats: {
    "name": "GET/hostRootShell/assets/i18n/en_US.json",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "395",
        "ok": "395",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "42",
        "ok": "42",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "70",
        "ok": "70",
        "ko": "-"
    },
    "percentiles1": {
        "total": "24",
        "ok": "24",
        "ko": "-"
    },
    "percentiles2": {
        "total": "26",
        "ok": "26",
        "ko": "-"
    },
    "percentiles3": {
        "total": "162",
        "ok": "162",
        "ko": "-"
    },
    "percentiles4": {
        "total": "338",
        "ok": "338",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.852",
        "ok": "1.852",
        "ko": "-"
    }
}
    },"req_get-hostmnemo-s-7aeb7": {
        type: "REQUEST",
        name: "GET/hostMnemo/scripts.js",
path: "GET/hostMnemo/scripts.js",
pathFormatted: "req_get-hostmnemo-s-7aeb7",
stats: {
    "name": "GET/hostMnemo/scripts.js",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3512",
        "ok": "3512",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "787",
        "ok": "787",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "1068",
        "ok": "1068",
        "ko": "-"
    },
    "percentiles1": {
        "total": "27",
        "ok": "27",
        "ko": "-"
    },
    "percentiles2": {
        "total": "1564",
        "ok": "1564",
        "ko": "-"
    },
    "percentiles3": {
        "total": "2886",
        "ok": "2886",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3469",
        "ok": "3469",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 32,
    "percentage": 64
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 2,
    "percentage": 4
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 16,
    "percentage": 32
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.852",
        "ok": "1.852",
        "ko": "-"
    }
}
    },"req_get-mnemo-css-c-7a130": {
        type: "REQUEST",
        name: "GET/mnemo/css/common.css",
path: "GET/mnemo/css/common.css",
pathFormatted: "req_get-mnemo-css-c-7a130",
stats: {
    "name": "GET/mnemo/css/common.css",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "37",
        "ok": "37",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "4",
        "ok": "4",
        "ko": "-"
    },
    "percentiles1": {
        "total": "24",
        "ok": "24",
        "ko": "-"
    },
    "percentiles2": {
        "total": "26",
        "ok": "26",
        "ko": "-"
    },
    "percentiles3": {
        "total": "34",
        "ok": "34",
        "ko": "-"
    },
    "percentiles4": {
        "total": "37",
        "ok": "37",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.852",
        "ok": "1.852",
        "ko": "-"
    }
}
    },"req_get-mnemo-resou-6daa7": {
        type: "REQUEST",
        name: "GET/mnemo/resources/graph.txt",
path: "GET/mnemo/resources/graph.txt",
pathFormatted: "req_get-mnemo-resou-6daa7",
stats: {
    "name": "GET/mnemo/resources/graph.txt",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "267",
        "ok": "267",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "40",
        "ok": "40",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "56",
        "ok": "56",
        "ko": "-"
    },
    "percentiles1": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "percentiles2": {
        "total": "27",
        "ok": "27",
        "ko": "-"
    },
    "percentiles3": {
        "total": "157",
        "ok": "157",
        "ko": "-"
    },
    "percentiles4": {
        "total": "262",
        "ok": "262",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.852",
        "ok": "1.852",
        "ko": "-"
    }
}
    },"req_get-mnemo-resou-95e3c": {
        type: "REQUEST",
        name: "GET/mnemo/resources/graph_ru.txt",
path: "GET/mnemo/resources/graph_ru.txt",
pathFormatted: "req_get-mnemo-resou-95e3c",
stats: {
    "name": "GET/mnemo/resources/graph_ru.txt",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "273",
        "ok": "273",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "34",
        "ok": "34",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "49",
        "ok": "49",
        "ko": "-"
    },
    "percentiles1": {
        "total": "24",
        "ok": "24",
        "ko": "-"
    },
    "percentiles2": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "percentiles3": {
        "total": "35",
        "ok": "35",
        "ko": "-"
    },
    "percentiles4": {
        "total": "272",
        "ok": "272",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.852",
        "ok": "1.852",
        "ko": "-"
    }
}
    },"req_get-mnemo-resou-d3607": {
        type: "REQUEST",
        name: "GET/mnemo/resources/editor.txt",
path: "GET/mnemo/resources/editor.txt",
pathFormatted: "req_get-mnemo-resou-d3607",
stats: {
    "name": "GET/mnemo/resources/editor.txt",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "264",
        "ok": "264",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "43",
        "ok": "43",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "65",
        "ok": "65",
        "ko": "-"
    },
    "percentiles1": {
        "total": "24",
        "ok": "24",
        "ko": "-"
    },
    "percentiles2": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "percentiles3": {
        "total": "261",
        "ok": "261",
        "ko": "-"
    },
    "percentiles4": {
        "total": "264",
        "ok": "264",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.852",
        "ok": "1.852",
        "ko": "-"
    }
}
    },"req_get-mnemo-resou-03f4c": {
        type: "REQUEST",
        name: "GET/mnemo/resources/editor_ru.txt",
path: "GET/mnemo/resources/editor_ru.txt",
pathFormatted: "req_get-mnemo-resou-03f4c",
stats: {
    "name": "GET/mnemo/resources/editor_ru.txt",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "272",
        "ok": "272",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "29",
        "ok": "29",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "35",
        "ok": "35",
        "ko": "-"
    },
    "percentiles1": {
        "total": "24",
        "ok": "24",
        "ko": "-"
    },
    "percentiles2": {
        "total": "26",
        "ok": "26",
        "ko": "-"
    },
    "percentiles3": {
        "total": "32",
        "ok": "32",
        "ko": "-"
    },
    "percentiles4": {
        "total": "160",
        "ok": "160",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.852",
        "ok": "1.852",
        "ko": "-"
    }
}
    },"req_get-hostmnemo-m-22113": {
        type: "REQUEST",
        name: "GET/hostMnemo/main.js",
path: "GET/hostMnemo/main.js",
pathFormatted: "req_get-hostmnemo-m-22113",
stats: {
    "name": "GET/hostMnemo/main.js",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "13990",
        "ok": "13990",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "3923",
        "ok": "3923",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "4892",
        "ok": "4892",
        "ko": "-"
    },
    "percentiles1": {
        "total": "26",
        "ok": "26",
        "ko": "-"
    },
    "percentiles2": {
        "total": "8824",
        "ok": "8824",
        "ko": "-"
    },
    "percentiles3": {
        "total": "11000",
        "ok": "11000",
        "ko": "-"
    },
    "percentiles4": {
        "total": "13643",
        "ok": "13643",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 30,
    "percentage": 60
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 20,
    "percentage": 40
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.852",
        "ok": "1.852",
        "ko": "-"
    }
}
    },"req_get-hostmnemo-a-913f4": {
        type: "REQUEST",
        name: "GET/hostMnemo/assets/i18n/en_US.json",
path: "GET/hostMnemo/assets/i18n/en_US.json",
pathFormatted: "req_get-hostmnemo-a-913f4",
stats: {
    "name": "GET/hostMnemo/assets/i18n/en_US.json",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "22",
        "ok": "22",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "255",
        "ok": "255",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "29",
        "ok": "29",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "32",
        "ok": "32",
        "ko": "-"
    },
    "percentiles1": {
        "total": "24",
        "ok": "24",
        "ko": "-"
    },
    "percentiles2": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "percentiles3": {
        "total": "34",
        "ok": "34",
        "ko": "-"
    },
    "percentiles4": {
        "total": "148",
        "ok": "148",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.852",
        "ok": "1.852",
        "ko": "-"
    }
}
    },"req_get-hostmnemo-a-19ffa": {
        type: "REQUEST",
        name: "GET/hostMnemo/assets/i18n/zyfra-mnemo/zyfraMnemoModule.en_US.json",
path: "GET/hostMnemo/assets/i18n/zyfra-mnemo/zyfraMnemoModule.en_US.json",
pathFormatted: "req_get-hostmnemo-a-19ffa",
stats: {
    "name": "GET/hostMnemo/assets/i18n/zyfra-mnemo/zyfraMnemoModule.en_US.json",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "272",
        "ok": "272",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "29",
        "ok": "29",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "35",
        "ok": "35",
        "ko": "-"
    },
    "percentiles1": {
        "total": "23",
        "ok": "23",
        "ko": "-"
    },
    "percentiles2": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "percentiles3": {
        "total": "39",
        "ok": "39",
        "ko": "-"
    },
    "percentiles4": {
        "total": "163",
        "ok": "163",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.852",
        "ok": "1.852",
        "ko": "-"
    }
}
    },"req_get-hostmnemo-a-5dc3d": {
        type: "REQUEST",
        name: "GET/hostMnemo/assets/settings.json",
path: "GET/hostMnemo/assets/settings.json",
pathFormatted: "req_get-hostmnemo-a-5dc3d",
stats: {
    "name": "GET/hostMnemo/assets/settings.json",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "267",
        "ok": "267",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "29",
        "ok": "29",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "34",
        "ok": "34",
        "ko": "-"
    },
    "percentiles1": {
        "total": "23",
        "ok": "23",
        "ko": "-"
    },
    "percentiles2": {
        "total": "24",
        "ok": "24",
        "ko": "-"
    },
    "percentiles3": {
        "total": "35",
        "ok": "35",
        "ko": "-"
    },
    "percentiles4": {
        "total": "155",
        "ok": "155",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.852",
        "ok": "1.852",
        "ko": "-"
    }
}
    },"req_get-mnemo-resou-b8f0f": {
        type: "REQUEST",
        name: "GET/mnemo/resources/grapheditor.txt",
path: "GET/mnemo/resources/grapheditor.txt",
pathFormatted: "req_get-mnemo-resou-b8f0f",
stats: {
    "name": "GET/mnemo/resources/grapheditor.txt",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "31",
        "ok": "31",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "24",
        "ok": "24",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "2",
        "ok": "2",
        "ko": "-"
    },
    "percentiles1": {
        "total": "23",
        "ok": "23",
        "ko": "-"
    },
    "percentiles2": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "percentiles3": {
        "total": "27",
        "ok": "27",
        "ko": "-"
    },
    "percentiles4": {
        "total": "31",
        "ok": "31",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.852",
        "ok": "1.852",
        "ko": "-"
    }
}
    },"req_get-mnemo-style-4f382": {
        type: "REQUEST",
        name: "GET/mnemo/styles/default.xml",
path: "GET/mnemo/styles/default.xml",
pathFormatted: "req_get-mnemo-style-4f382",
stats: {
    "name": "GET/mnemo/styles/default.xml",
    "numberOfRequests": {
        "total": "80",
        "ok": "80",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "38",
        "ok": "38",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "24",
        "ok": "24",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "4",
        "ok": "4",
        "ko": "-"
    },
    "percentiles1": {
        "total": "23",
        "ok": "23",
        "ko": "-"
    },
    "percentiles2": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "percentiles3": {
        "total": "32",
        "ok": "32",
        "ko": "-"
    },
    "percentiles4": {
        "total": "38",
        "ok": "38",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 80,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.963",
        "ok": "2.963",
        "ko": "-"
    }
}
    },"req_get-mnemo-stenc-e38f6": {
        type: "REQUEST",
        name: "GET/mnemo/stencils/mnemo.library.xml",
path: "GET/mnemo/stencils/mnemo.library.xml",
pathFormatted: "req_get-mnemo-stenc-e38f6",
stats: {
    "name": "GET/mnemo/stencils/mnemo.library.xml",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "678",
        "ok": "678",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "105",
        "ok": "105",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "142",
        "ok": "142",
        "ko": "-"
    },
    "percentiles1": {
        "total": "26",
        "ok": "26",
        "ko": "-"
    },
    "percentiles2": {
        "total": "142",
        "ok": "142",
        "ko": "-"
    },
    "percentiles3": {
        "total": "398",
        "ok": "398",
        "ko": "-"
    },
    "percentiles4": {
        "total": "645",
        "ok": "645",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.852",
        "ok": "1.852",
        "ko": "-"
    }
}
    },"req_get-mnemobase-a-ff9f5": {
        type: "REQUEST",
        name: "GET/mnemobase/api/v1/mnemo/",
path: "GET/mnemobase/api/v1/mnemo/",
pathFormatted: "req_get-mnemobase-a-ff9f5",
stats: {
    "name": "GET/mnemobase/api/v1/mnemo/",
    "numberOfRequests": {
        "total": "30",
        "ok": "30",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "58",
        "ok": "58",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "627",
        "ok": "627",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "223",
        "ok": "223",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "148",
        "ok": "148",
        "ko": "-"
    },
    "percentiles1": {
        "total": "175",
        "ok": "175",
        "ko": "-"
    },
    "percentiles2": {
        "total": "257",
        "ok": "257",
        "ko": "-"
    },
    "percentiles3": {
        "total": "540",
        "ok": "540",
        "ko": "-"
    },
    "percentiles4": {
        "total": "606",
        "ok": "606",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 30,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.111",
        "ok": "1.111",
        "ko": "-"
    }
}
    },"req_get-mnemo-resou-96eb1": {
        type: "REQUEST",
        name: "GET/mnemo/resources/grapheditor_ru.txt",
path: "GET/mnemo/resources/grapheditor_ru.txt",
pathFormatted: "req_get-mnemo-resou-96eb1",
stats: {
    "name": "GET/mnemo/resources/grapheditor_ru.txt",
    "numberOfRequests": {
        "total": "30",
        "ok": "30",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "464",
        "ok": "464",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "54",
        "ok": "54",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "106",
        "ok": "106",
        "ko": "-"
    },
    "percentiles1": {
        "total": "24",
        "ok": "24",
        "ko": "-"
    },
    "percentiles2": {
        "total": "26",
        "ok": "26",
        "ko": "-"
    },
    "percentiles3": {
        "total": "261",
        "ok": "261",
        "ko": "-"
    },
    "percentiles4": {
        "total": "456",
        "ok": "456",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 30,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.111",
        "ok": "1.111",
        "ko": "-"
    }
}
    },"req_put-mnemobase-a-181b7": {
        type: "REQUEST",
        name: "PUT/mnemobase/api/v1/mnemo",
path: "PUT/mnemobase/api/v1/mnemo",
pathFormatted: "req_put-mnemobase-a-181b7",
stats: {
    "name": "PUT/mnemobase/api/v1/mnemo",
    "numberOfRequests": {
        "total": "9",
        "ok": "9",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "135",
        "ok": "135",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "194",
        "ok": "194",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "163",
        "ok": "163",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "percentiles1": {
        "total": "169",
        "ok": "169",
        "ko": "-"
    },
    "percentiles2": {
        "total": "181",
        "ok": "181",
        "ko": "-"
    },
    "percentiles3": {
        "total": "192",
        "ok": "192",
        "ko": "-"
    },
    "percentiles4": {
        "total": "194",
        "ok": "194",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 9,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.333",
        "ok": "0.333",
        "ko": "-"
    }
}
    },"req_post-mnemobase--159fa": {
        type: "REQUEST",
        name: "POST/mnemobase/api/v1/mnemo",
path: "POST/mnemobase/api/v1/mnemo",
pathFormatted: "req_post-mnemobase--159fa",
stats: {
    "name": "POST/mnemobase/api/v1/mnemo",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "83",
        "ok": "83",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "83",
        "ok": "83",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "83",
        "ok": "83",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "83",
        "ok": "83",
        "ko": "-"
    },
    "percentiles2": {
        "total": "83",
        "ok": "83",
        "ko": "-"
    },
    "percentiles3": {
        "total": "83",
        "ok": "83",
        "ko": "-"
    },
    "percentiles4": {
        "total": "83",
        "ok": "83",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.037",
        "ok": "0.037",
        "ko": "-"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
