var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "10689",
        "ok": "10689",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "20",
        "ok": "20",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "6871",
        "ok": "6871",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "39",
        "ok": "39",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "173",
        "ok": "173",
        "ko": "-"
    },
    "percentiles1": {
        "total": "22",
        "ok": "22",
        "ko": "-"
    },
    "percentiles2": {
        "total": "23",
        "ok": "23",
        "ko": "-"
    },
    "percentiles3": {
        "total": "118",
        "ok": "118",
        "ko": "-"
    },
    "percentiles4": {
        "total": "246",
        "ok": "246",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 10663,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 13,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 13,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "120.101",
        "ok": "120.101",
        "ko": "-"
    }
},
contents: {
"req_login-99dea": {
        type: "REQUEST",
        name: "Login",
path: "Login",
pathFormatted: "req_login-99dea",
stats: {
    "name": "Login",
    "numberOfRequests": {
        "total": "260",
        "ok": "260",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "133",
        "ok": "133",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1194",
        "ok": "1194",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "182",
        "ok": "182",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "123",
        "ok": "123",
        "ko": "-"
    },
    "percentiles1": {
        "total": "154",
        "ok": "154",
        "ko": "-"
    },
    "percentiles2": {
        "total": "175",
        "ok": "175",
        "ko": "-"
    },
    "percentiles3": {
        "total": "217",
        "ok": "217",
        "ko": "-"
    },
    "percentiles4": {
        "total": "808",
        "ok": "808",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 257,
    "percentage": 99
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 3,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.921",
        "ok": "2.921",
        "ko": "-"
    }
}
    },"req_-mnemoschemes-a-0c177": {
        type: "REQUEST",
        name: "/mnemoschemes/api/v1/mnemo/info",
path: "/mnemoschemes/api/v1/mnemo/info",
pathFormatted: "req_-mnemoschemes-a-0c177",
stats: {
    "name": "/mnemoschemes/api/v1/mnemo/info",
    "numberOfRequests": {
        "total": "333",
        "ok": "333",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "34",
        "ok": "34",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "125",
        "ok": "125",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "45",
        "ok": "45",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "10",
        "ok": "10",
        "ko": "-"
    },
    "percentiles1": {
        "total": "42",
        "ok": "42",
        "ko": "-"
    },
    "percentiles2": {
        "total": "48",
        "ok": "48",
        "ko": "-"
    },
    "percentiles3": {
        "total": "63",
        "ok": "63",
        "ko": "-"
    },
    "percentiles4": {
        "total": "74",
        "ok": "74",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 333,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.742",
        "ok": "3.742",
        "ko": "-"
    }
}
    },"req_post-mnemoschem-dba6c": {
        type: "REQUEST",
        name: "POST/mnemoschemes/api/v1/mnemo",
path: "POST/mnemoschemes/api/v1/mnemo",
pathFormatted: "req_post-mnemoschem-dba6c",
stats: {
    "name": "POST/mnemoschemes/api/v1/mnemo",
    "numberOfRequests": {
        "total": "12",
        "ok": "12",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "28",
        "ok": "28",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "30",
        "ok": "30",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "29",
        "ok": "29",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "1",
        "ok": "1",
        "ko": "-"
    },
    "percentiles1": {
        "total": "29",
        "ok": "29",
        "ko": "-"
    },
    "percentiles2": {
        "total": "30",
        "ok": "30",
        "ko": "-"
    },
    "percentiles3": {
        "total": "30",
        "ok": "30",
        "ko": "-"
    },
    "percentiles4": {
        "total": "30",
        "ok": "30",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 12,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.135",
        "ok": "0.135",
        "ko": "-"
    }
}
    },"req_--6666c": {
        type: "REQUEST",
        name: "/",
path: "/",
pathFormatted: "req_--6666c",
stats: {
    "name": "/",
    "numberOfRequests": {
        "total": "305",
        "ok": "305",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "20",
        "ok": "20",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "252",
        "ok": "252",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "23",
        "ok": "23",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "19",
        "ok": "19",
        "ko": "-"
    },
    "percentiles1": {
        "total": "22",
        "ok": "22",
        "ko": "-"
    },
    "percentiles2": {
        "total": "22",
        "ok": "22",
        "ko": "-"
    },
    "percentiles3": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "percentiles4": {
        "total": "34",
        "ok": "34",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 305,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.427",
        "ok": "3.427",
        "ko": "-"
    }
}
    },"req_-built-scripts--beed0": {
        type: "REQUEST",
        name: "/built/scripts/zone.min.js",
path: "/built/scripts/zone.min.js",
pathFormatted: "req_-built-scripts--beed0",
stats: {
    "name": "/built/scripts/zone.min.js",
    "numberOfRequests": {
        "total": "305",
        "ok": "305",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "251",
        "ok": "251",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "24",
        "ok": "24",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "15",
        "ok": "15",
        "ko": "-"
    },
    "percentiles1": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "percentiles2": {
        "total": "22",
        "ok": "22",
        "ko": "-"
    },
    "percentiles3": {
        "total": "26",
        "ok": "26",
        "ko": "-"
    },
    "percentiles4": {
        "total": "69",
        "ok": "69",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 305,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.427",
        "ok": "3.427",
        "ko": "-"
    }
}
    },"req_-built-scripts--0a490": {
        type: "REQUEST",
        name: "/built/scripts/import-map-overrides.js",
path: "/built/scripts/import-map-overrides.js",
pathFormatted: "req_-built-scripts--0a490",
stats: {
    "name": "/built/scripts/import-map-overrides.js",
    "numberOfRequests": {
        "total": "305",
        "ok": "305",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "20",
        "ok": "20",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1044",
        "ok": "1044",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "27",
        "ok": "27",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "61",
        "ok": "61",
        "ko": "-"
    },
    "percentiles1": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "percentiles2": {
        "total": "22",
        "ok": "22",
        "ko": "-"
    },
    "percentiles3": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "percentiles4": {
        "total": "43",
        "ok": "43",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 304,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.427",
        "ok": "3.427",
        "ko": "-"
    }
}
    },"req_-built-scripts--da42e": {
        type: "REQUEST",
        name: "/built/scripts/system.min.js",
path: "/built/scripts/system.min.js",
pathFormatted: "req_-built-scripts--da42e",
stats: {
    "name": "/built/scripts/system.min.js",
    "numberOfRequests": {
        "total": "305",
        "ok": "305",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "20",
        "ok": "20",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "249",
        "ok": "249",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "26",
        "ok": "26",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "29",
        "ok": "29",
        "ko": "-"
    },
    "percentiles1": {
        "total": "22",
        "ok": "22",
        "ko": "-"
    },
    "percentiles2": {
        "total": "22",
        "ok": "22",
        "ko": "-"
    },
    "percentiles3": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "percentiles4": {
        "total": "246",
        "ok": "246",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 305,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.427",
        "ok": "3.427",
        "ko": "-"
    }
}
    },"req_-built-scripts--89aea": {
        type: "REQUEST",
        name: "/built/scripts/amd.min.js",
path: "/built/scripts/amd.min.js",
pathFormatted: "req_-built-scripts--89aea",
stats: {
    "name": "/built/scripts/amd.min.js",
    "numberOfRequests": {
        "total": "305",
        "ok": "305",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "20",
        "ok": "20",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1042",
        "ok": "1042",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "26",
        "ok": "26",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "59",
        "ok": "59",
        "ko": "-"
    },
    "percentiles1": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "percentiles2": {
        "total": "22",
        "ok": "22",
        "ko": "-"
    },
    "percentiles3": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "percentiles4": {
        "total": "43",
        "ok": "43",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 304,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.427",
        "ok": "3.427",
        "ko": "-"
    }
}
    },"req_-built-scripts--dbe46": {
        type: "REQUEST",
        name: "/built/scripts/named-exports.js",
path: "/built/scripts/named-exports.js",
pathFormatted: "req_-built-scripts--dbe46",
stats: {
    "name": "/built/scripts/named-exports.js",
    "numberOfRequests": {
        "total": "305",
        "ok": "305",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "20",
        "ok": "20",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "46",
        "ok": "46",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "22",
        "ok": "22",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "3",
        "ok": "3",
        "ko": "-"
    },
    "percentiles1": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "percentiles2": {
        "total": "22",
        "ok": "22",
        "ko": "-"
    },
    "percentiles3": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "percentiles4": {
        "total": "42",
        "ok": "42",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 305,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.427",
        "ok": "3.427",
        "ko": "-"
    }
}
    },"req_-built-scripts--e6e7f": {
        type: "REQUEST",
        name: "/built/scripts/named-register.min.js",
path: "/built/scripts/named-register.min.js",
pathFormatted: "req_-built-scripts--e6e7f",
stats: {
    "name": "/built/scripts/named-register.min.js",
    "numberOfRequests": {
        "total": "305",
        "ok": "305",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "20",
        "ok": "20",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "257",
        "ok": "257",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "27",
        "ok": "27",
        "ko": "-"
    },
    "percentiles1": {
        "total": "22",
        "ok": "22",
        "ko": "-"
    },
    "percentiles2": {
        "total": "22",
        "ok": "22",
        "ko": "-"
    },
    "percentiles3": {
        "total": "26",
        "ok": "26",
        "ko": "-"
    },
    "percentiles4": {
        "total": "241",
        "ok": "241",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 305,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.427",
        "ok": "3.427",
        "ko": "-"
    }
}
    },"req_-built-vendors--57cfa": {
        type: "REQUEST",
        name: "/built/vendors~main.js",
path: "/built/vendors~main.js",
pathFormatted: "req_-built-vendors--57cfa",
stats: {
    "name": "/built/vendors~main.js",
    "numberOfRequests": {
        "total": "305",
        "ok": "305",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "20",
        "ok": "20",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "419",
        "ok": "419",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "27",
        "ok": "27",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "31",
        "ok": "31",
        "ko": "-"
    },
    "percentiles1": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "percentiles2": {
        "total": "22",
        "ok": "22",
        "ko": "-"
    },
    "percentiles3": {
        "total": "28",
        "ok": "28",
        "ko": "-"
    },
    "percentiles4": {
        "total": "152",
        "ok": "152",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 305,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.427",
        "ok": "3.427",
        "ko": "-"
    }
}
    },"req_-built-main-js-746c8": {
        type: "REQUEST",
        name: "/built/main.js",
path: "/built/main.js",
pathFormatted: "req_-built-main-js-746c8",
stats: {
    "name": "/built/main.js",
    "numberOfRequests": {
        "total": "305",
        "ok": "305",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "20",
        "ok": "20",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "246",
        "ok": "246",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "24",
        "ok": "24",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "18",
        "ok": "18",
        "ko": "-"
    },
    "percentiles1": {
        "total": "22",
        "ok": "22",
        "ko": "-"
    },
    "percentiles2": {
        "total": "22",
        "ok": "22",
        "ko": "-"
    },
    "percentiles3": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "percentiles4": {
        "total": "43",
        "ok": "43",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 305,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.427",
        "ok": "3.427",
        "ko": "-"
    }
}
    },"req_-built-assets-s-b3836": {
        type: "REQUEST",
        name: "/built/assets/settings.json",
path: "/built/assets/settings.json",
pathFormatted: "req_-built-assets-s-b3836",
stats: {
    "name": "/built/assets/settings.json",
    "numberOfRequests": {
        "total": "305",
        "ok": "305",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "20",
        "ok": "20",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "249",
        "ok": "249",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "26",
        "ok": "26",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "29",
        "ok": "29",
        "ko": "-"
    },
    "percentiles1": {
        "total": "22",
        "ok": "22",
        "ko": "-"
    },
    "percentiles2": {
        "total": "22",
        "ok": "22",
        "ko": "-"
    },
    "percentiles3": {
        "total": "26",
        "ok": "26",
        "ko": "-"
    },
    "percentiles4": {
        "total": "245",
        "ok": "245",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 305,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.427",
        "ok": "3.427",
        "ko": "-"
    }
}
    },"req_-auth-realms-ma-5a21e": {
        type: "REQUEST",
        name: "/auth/realms/master/account",
path: "/auth/realms/master/account",
pathFormatted: "req_-auth-realms-ma-5a21e",
stats: {
    "name": "/auth/realms/master/account",
    "numberOfRequests": {
        "total": "305",
        "ok": "305",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "22",
        "ok": "22",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "254",
        "ok": "254",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "26",
        "ok": "26",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "23",
        "ok": "23",
        "ko": "-"
    },
    "percentiles1": {
        "total": "23",
        "ok": "23",
        "ko": "-"
    },
    "percentiles2": {
        "total": "24",
        "ok": "24",
        "ko": "-"
    },
    "percentiles3": {
        "total": "28",
        "ok": "28",
        "ko": "-"
    },
    "percentiles4": {
        "total": "54",
        "ok": "54",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 305,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.427",
        "ok": "3.427",
        "ko": "-"
    }
}
    },"req_-auth-realms-ma-0613f": {
        type: "REQUEST",
        name: "/auth/realms/master/account Redirect 1",
path: "/auth/realms/master/account Redirect 1",
pathFormatted: "req_-auth-realms-ma-0613f",
stats: {
    "name": "/auth/realms/master/account Redirect 1",
    "numberOfRequests": {
        "total": "305",
        "ok": "305",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "27",
        "ok": "27",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "350",
        "ok": "350",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "33",
        "ok": "33",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "percentiles1": {
        "total": "30",
        "ok": "30",
        "ko": "-"
    },
    "percentiles2": {
        "total": "32",
        "ok": "32",
        "ko": "-"
    },
    "percentiles3": {
        "total": "36",
        "ok": "36",
        "ko": "-"
    },
    "percentiles4": {
        "total": "120",
        "ok": "120",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 305,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.427",
        "ok": "3.427",
        "ko": "-"
    }
}
    },"req_-json-applicati-ca958": {
        type: "REQUEST",
        name: "/json/applicationUnits",
path: "/json/applicationUnits",
pathFormatted: "req_-json-applicati-ca958",
stats: {
    "name": "/json/applicationUnits",
    "numberOfRequests": {
        "total": "610",
        "ok": "610",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "918",
        "ok": "918",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "26",
        "ok": "26",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "42",
        "ok": "42",
        "ko": "-"
    },
    "percentiles1": {
        "total": "22",
        "ok": "22",
        "ko": "-"
    },
    "percentiles2": {
        "total": "22",
        "ok": "22",
        "ko": "-"
    },
    "percentiles3": {
        "total": "27",
        "ok": "27",
        "ko": "-"
    },
    "percentiles4": {
        "total": "94",
        "ok": "94",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 609,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "6.854",
        "ok": "6.854",
        "ko": "-"
    }
}
    },"req_-built-scripts--a12a0": {
        type: "REQUEST",
        name: "/built/scripts/single-spa.min.js",
path: "/built/scripts/single-spa.min.js",
pathFormatted: "req_-built-scripts--a12a0",
stats: {
    "name": "/built/scripts/single-spa.min.js",
    "numberOfRequests": {
        "total": "305",
        "ok": "305",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "20",
        "ok": "20",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "925",
        "ok": "925",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "26",
        "ok": "26",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "53",
        "ok": "53",
        "ko": "-"
    },
    "percentiles1": {
        "total": "22",
        "ok": "22",
        "ko": "-"
    },
    "percentiles2": {
        "total": "22",
        "ok": "22",
        "ko": "-"
    },
    "percentiles3": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "percentiles4": {
        "total": "45",
        "ok": "45",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 304,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.427",
        "ok": "3.427",
        "ko": "-"
    }
}
    },"req_-json-usersetti-341fa": {
        type: "REQUEST",
        name: "/json/UserSettings",
path: "/json/UserSettings",
pathFormatted: "req_-json-usersetti-341fa",
stats: {
    "name": "/json/UserSettings",
    "numberOfRequests": {
        "total": "305",
        "ok": "305",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1043",
        "ok": "1043",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "29",
        "ok": "29",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "65",
        "ok": "65",
        "ko": "-"
    },
    "percentiles1": {
        "total": "22",
        "ok": "22",
        "ko": "-"
    },
    "percentiles2": {
        "total": "22",
        "ok": "22",
        "ko": "-"
    },
    "percentiles3": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "percentiles4": {
        "total": "244",
        "ok": "244",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 304,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.427",
        "ok": "3.427",
        "ko": "-"
    }
}
    },"req_-json-usercommo-43a62": {
        type: "REQUEST",
        name: "/json/UserCommonApplications",
path: "/json/UserCommonApplications",
pathFormatted: "req_-json-usercommo-43a62",
stats: {
    "name": "/json/UserCommonApplications",
    "numberOfRequests": {
        "total": "305",
        "ok": "305",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "243",
        "ok": "243",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "23",
        "ok": "23",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "14",
        "ok": "14",
        "ko": "-"
    },
    "percentiles1": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "percentiles2": {
        "total": "22",
        "ok": "22",
        "ko": "-"
    },
    "percentiles3": {
        "total": "24",
        "ok": "24",
        "ko": "-"
    },
    "percentiles4": {
        "total": "43",
        "ok": "43",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 305,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.427",
        "ok": "3.427",
        "ko": "-"
    }
}
    },"req_-hostrootshell--a77cd": {
        type: "REQUEST",
        name: "/hostRootShell/assets/i18n/en_US.json",
path: "/hostRootShell/assets/i18n/en_US.json",
pathFormatted: "req_-hostrootshell--a77cd",
stats: {
    "name": "/hostRootShell/assets/i18n/en_US.json",
    "numberOfRequests": {
        "total": "305",
        "ok": "305",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "247",
        "ok": "247",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "22",
        "ok": "22",
        "ko": "-"
    },
    "percentiles1": {
        "total": "22",
        "ok": "22",
        "ko": "-"
    },
    "percentiles2": {
        "total": "22",
        "ok": "22",
        "ko": "-"
    },
    "percentiles3": {
        "total": "26",
        "ok": "26",
        "ko": "-"
    },
    "percentiles4": {
        "total": "45",
        "ok": "45",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 305,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.427",
        "ok": "3.427",
        "ko": "-"
    }
}
    },"req_-hostmnemo-scri-e7b48": {
        type: "REQUEST",
        name: "/hostMnemo/scripts.js",
path: "/hostMnemo/scripts.js",
pathFormatted: "req_-hostmnemo-scri-e7b48",
stats: {
    "name": "/hostMnemo/scripts.js",
    "numberOfRequests": {
        "total": "305",
        "ok": "305",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1641",
        "ok": "1641",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "58",
        "ok": "58",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "196",
        "ok": "196",
        "ko": "-"
    },
    "percentiles1": {
        "total": "22",
        "ok": "22",
        "ko": "-"
    },
    "percentiles2": {
        "total": "23",
        "ok": "23",
        "ko": "-"
    },
    "percentiles3": {
        "total": "27",
        "ok": "27",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1101",
        "ok": "1101",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 298,
    "percentage": 98
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 4,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 3,
    "percentage": 1
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.427",
        "ok": "3.427",
        "ko": "-"
    }
}
    },"req_-mnemo-css-comm-d07a9": {
        type: "REQUEST",
        name: "/mnemo/css/common.css",
path: "/mnemo/css/common.css",
pathFormatted: "req_-mnemo-css-comm-d07a9",
stats: {
    "name": "/mnemo/css/common.css",
    "numberOfRequests": {
        "total": "305",
        "ok": "305",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "697",
        "ok": "697",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "27",
        "ok": "27",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "51",
        "ok": "51",
        "ko": "-"
    },
    "percentiles1": {
        "total": "22",
        "ok": "22",
        "ko": "-"
    },
    "percentiles2": {
        "total": "23",
        "ok": "23",
        "ko": "-"
    },
    "percentiles3": {
        "total": "27",
        "ok": "27",
        "ko": "-"
    },
    "percentiles4": {
        "total": "46",
        "ok": "46",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 305,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.427",
        "ok": "3.427",
        "ko": "-"
    }
}
    },"req_-mnemo-resource-1c1a3": {
        type: "REQUEST",
        name: "/mnemo/resources/graph.txt",
path: "/mnemo/resources/graph.txt",
pathFormatted: "req_-mnemo-resource-1c1a3",
stats: {
    "name": "/mnemo/resources/graph.txt",
    "numberOfRequests": {
        "total": "305",
        "ok": "305",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "246",
        "ok": "246",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "22",
        "ok": "22",
        "ko": "-"
    },
    "percentiles1": {
        "total": "22",
        "ok": "22",
        "ko": "-"
    },
    "percentiles2": {
        "total": "22",
        "ok": "22",
        "ko": "-"
    },
    "percentiles3": {
        "total": "27",
        "ok": "27",
        "ko": "-"
    },
    "percentiles4": {
        "total": "53",
        "ok": "53",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 305,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.427",
        "ok": "3.427",
        "ko": "-"
    }
}
    },"req_-mnemo-resource-4d1f0": {
        type: "REQUEST",
        name: "/mnemo/resources/graph_ru.txt",
path: "/mnemo/resources/graph_ru.txt",
pathFormatted: "req_-mnemo-resource-4d1f0",
stats: {
    "name": "/mnemo/resources/graph_ru.txt",
    "numberOfRequests": {
        "total": "305",
        "ok": "305",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "363",
        "ok": "363",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "27",
        "ok": "27",
        "ko": "-"
    },
    "percentiles1": {
        "total": "22",
        "ok": "22",
        "ko": "-"
    },
    "percentiles2": {
        "total": "22",
        "ok": "22",
        "ko": "-"
    },
    "percentiles3": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "percentiles4": {
        "total": "44",
        "ok": "44",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 305,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.427",
        "ok": "3.427",
        "ko": "-"
    }
}
    },"req_-mnemo-resource-82cfb": {
        type: "REQUEST",
        name: "/mnemo/resources/editor.txt",
path: "/mnemo/resources/editor.txt",
pathFormatted: "req_-mnemo-resource-82cfb",
stats: {
    "name": "/mnemo/resources/editor.txt",
    "numberOfRequests": {
        "total": "305",
        "ok": "305",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "469",
        "ok": "469",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "31",
        "ok": "31",
        "ko": "-"
    },
    "percentiles1": {
        "total": "22",
        "ok": "22",
        "ko": "-"
    },
    "percentiles2": {
        "total": "22",
        "ok": "22",
        "ko": "-"
    },
    "percentiles3": {
        "total": "26",
        "ok": "26",
        "ko": "-"
    },
    "percentiles4": {
        "total": "46",
        "ok": "46",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 305,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.427",
        "ok": "3.427",
        "ko": "-"
    }
}
    },"req_-mnemo-resource-33452": {
        type: "REQUEST",
        name: "/mnemo/resources/editor_ru.txt",
path: "/mnemo/resources/editor_ru.txt",
pathFormatted: "req_-mnemo-resource-33452",
stats: {
    "name": "/mnemo/resources/editor_ru.txt",
    "numberOfRequests": {
        "total": "305",
        "ok": "305",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "248",
        "ok": "248",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "26",
        "ok": "26",
        "ko": "-"
    },
    "percentiles1": {
        "total": "22",
        "ok": "22",
        "ko": "-"
    },
    "percentiles2": {
        "total": "22",
        "ok": "22",
        "ko": "-"
    },
    "percentiles3": {
        "total": "27",
        "ok": "27",
        "ko": "-"
    },
    "percentiles4": {
        "total": "237",
        "ok": "237",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 305,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.427",
        "ok": "3.427",
        "ko": "-"
    }
}
    },"req_-hostmnemo-main-5bf99": {
        type: "REQUEST",
        name: "/hostMnemo/main.js",
path: "/hostMnemo/main.js",
pathFormatted: "req_-hostmnemo-main-5bf99",
stats: {
    "name": "/hostMnemo/main.js",
    "numberOfRequests": {
        "total": "305",
        "ok": "305",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "6871",
        "ok": "6871",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "194",
        "ok": "194",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "945",
        "ok": "945",
        "ko": "-"
    },
    "percentiles1": {
        "total": "22",
        "ok": "22",
        "ko": "-"
    },
    "percentiles2": {
        "total": "22",
        "ok": "22",
        "ko": "-"
    },
    "percentiles3": {
        "total": "66",
        "ok": "66",
        "ko": "-"
    },
    "percentiles4": {
        "total": "6159",
        "ok": "6159",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 295,
    "percentage": 97
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 10,
    "percentage": 3
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.427",
        "ok": "3.427",
        "ko": "-"
    }
}
    },"req_-hostmnemo-asse-3384a": {
        type: "REQUEST",
        name: "/hostMnemo/assets/i18n/en_US.json",
path: "/hostMnemo/assets/i18n/en_US.json",
pathFormatted: "req_-hostmnemo-asse-3384a",
stats: {
    "name": "/hostMnemo/assets/i18n/en_US.json",
    "numberOfRequests": {
        "total": "305",
        "ok": "305",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "699",
        "ok": "699",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "32",
        "ok": "32",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "65",
        "ok": "65",
        "ko": "-"
    },
    "percentiles1": {
        "total": "22",
        "ok": "22",
        "ko": "-"
    },
    "percentiles2": {
        "total": "22",
        "ok": "22",
        "ko": "-"
    },
    "percentiles3": {
        "total": "28",
        "ok": "28",
        "ko": "-"
    },
    "percentiles4": {
        "total": "250",
        "ok": "250",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 305,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.427",
        "ok": "3.427",
        "ko": "-"
    }
}
    },"req_-hostmnemo-asse-1c9a9": {
        type: "REQUEST",
        name: "/hostMnemo/assets/i18n/zyfra-mnemo/zyfraMnemoModule.en_US.json",
path: "/hostMnemo/assets/i18n/zyfra-mnemo/zyfraMnemoModule.en_US.json",
pathFormatted: "req_-hostmnemo-asse-1c9a9",
stats: {
    "name": "/hostMnemo/assets/i18n/zyfra-mnemo/zyfraMnemoModule.en_US.json",
    "numberOfRequests": {
        "total": "305",
        "ok": "305",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "248",
        "ok": "248",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "24",
        "ok": "24",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "18",
        "ok": "18",
        "ko": "-"
    },
    "percentiles1": {
        "total": "22",
        "ok": "22",
        "ko": "-"
    },
    "percentiles2": {
        "total": "23",
        "ok": "23",
        "ko": "-"
    },
    "percentiles3": {
        "total": "26",
        "ok": "26",
        "ko": "-"
    },
    "percentiles4": {
        "total": "44",
        "ok": "44",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 305,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.427",
        "ok": "3.427",
        "ko": "-"
    }
}
    },"req_-hostmnemo-asse-dc6b4": {
        type: "REQUEST",
        name: "/hostMnemo/assets/settings.json",
path: "/hostMnemo/assets/settings.json",
pathFormatted: "req_-hostmnemo-asse-dc6b4",
stats: {
    "name": "/hostMnemo/assets/settings.json",
    "numberOfRequests": {
        "total": "305",
        "ok": "305",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "688",
        "ok": "688",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "26",
        "ok": "26",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "40",
        "ok": "40",
        "ko": "-"
    },
    "percentiles1": {
        "total": "22",
        "ok": "22",
        "ko": "-"
    },
    "percentiles2": {
        "total": "22",
        "ok": "22",
        "ko": "-"
    },
    "percentiles3": {
        "total": "26",
        "ok": "26",
        "ko": "-"
    },
    "percentiles4": {
        "total": "45",
        "ok": "45",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 305,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.427",
        "ok": "3.427",
        "ko": "-"
    }
}
    },"req_-mnemo-resource-a83c8": {
        type: "REQUEST",
        name: "/mnemo/resources/grapheditor.txt",
path: "/mnemo/resources/grapheditor.txt",
pathFormatted: "req_-mnemo-resource-a83c8",
stats: {
    "name": "/mnemo/resources/grapheditor.txt",
    "numberOfRequests": {
        "total": "305",
        "ok": "305",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "810",
        "ok": "810",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "28",
        "ok": "28",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "50",
        "ok": "50",
        "ko": "-"
    },
    "percentiles1": {
        "total": "22",
        "ok": "22",
        "ko": "-"
    },
    "percentiles2": {
        "total": "23",
        "ok": "23",
        "ko": "-"
    },
    "percentiles3": {
        "total": "28",
        "ok": "28",
        "ko": "-"
    },
    "percentiles4": {
        "total": "238",
        "ok": "238",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 304,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.427",
        "ok": "3.427",
        "ko": "-"
    }
}
    },"req_-mnemo-styles-d-ee36a": {
        type: "REQUEST",
        name: "/mnemo/styles/default.xml",
path: "/mnemo/styles/default.xml",
pathFormatted: "req_-mnemo-styles-d-ee36a",
stats: {
    "name": "/mnemo/styles/default.xml",
    "numberOfRequests": {
        "total": "305",
        "ok": "305",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "278",
        "ok": "278",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "26",
        "ok": "26",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "27",
        "ok": "27",
        "ko": "-"
    },
    "percentiles1": {
        "total": "22",
        "ok": "22",
        "ko": "-"
    },
    "percentiles2": {
        "total": "22",
        "ok": "22",
        "ko": "-"
    },
    "percentiles3": {
        "total": "26",
        "ok": "26",
        "ko": "-"
    },
    "percentiles4": {
        "total": "237",
        "ok": "237",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 305,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.427",
        "ok": "3.427",
        "ko": "-"
    }
}
    },"req_-mnemo-stencils-43d94": {
        type: "REQUEST",
        name: "/mnemo/stencils/mnemo.library.xml",
path: "/mnemo/stencils/mnemo.library.xml",
pathFormatted: "req_-mnemo-stencils-43d94",
stats: {
    "name": "/mnemo/stencils/mnemo.library.xml",
    "numberOfRequests": {
        "total": "305",
        "ok": "305",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "703",
        "ok": "703",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "28",
        "ok": "28",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "43",
        "ok": "43",
        "ko": "-"
    },
    "percentiles1": {
        "total": "22",
        "ok": "22",
        "ko": "-"
    },
    "percentiles2": {
        "total": "23",
        "ok": "23",
        "ko": "-"
    },
    "percentiles3": {
        "total": "31",
        "ok": "31",
        "ko": "-"
    },
    "percentiles4": {
        "total": "115",
        "ok": "115",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 305,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.427",
        "ok": "3.427",
        "ko": "-"
    }
}
    },"req_get-mnemoscheme-b6811": {
        type: "REQUEST",
        name: "GET/mnemoschemes/api/v1/mnemo/",
path: "GET/mnemoschemes/api/v1/mnemo/",
pathFormatted: "req_get-mnemoscheme-b6811",
stats: {
    "name": "GET/mnemoschemes/api/v1/mnemo/",
    "numberOfRequests": {
        "total": "168",
        "ok": "168",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "37",
        "ok": "37",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "594",
        "ok": "594",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "183",
        "ok": "183",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "89",
        "ok": "89",
        "ko": "-"
    },
    "percentiles1": {
        "total": "186",
        "ok": "186",
        "ko": "-"
    },
    "percentiles2": {
        "total": "202",
        "ok": "202",
        "ko": "-"
    },
    "percentiles3": {
        "total": "327",
        "ok": "327",
        "ko": "-"
    },
    "percentiles4": {
        "total": "582",
        "ok": "582",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 168,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.888",
        "ok": "1.888",
        "ko": "-"
    }
}
    },"req_get-mnemo-resou-96eb1": {
        type: "REQUEST",
        name: "GET/mnemo/resources/grapheditor_ru.txt",
path: "GET/mnemo/resources/grapheditor_ru.txt",
pathFormatted: "req_get-mnemo-resou-96eb1",
stats: {
    "name": "GET/mnemo/resources/grapheditor_ru.txt",
    "numberOfRequests": {
        "total": "168",
        "ok": "168",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "369",
        "ok": "369",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "27",
        "ok": "27",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "32",
        "ok": "32",
        "ko": "-"
    },
    "percentiles1": {
        "total": "22",
        "ok": "22",
        "ko": "-"
    },
    "percentiles2": {
        "total": "23",
        "ok": "23",
        "ko": "-"
    },
    "percentiles3": {
        "total": "27",
        "ok": "27",
        "ko": "-"
    },
    "percentiles4": {
        "total": "116",
        "ok": "116",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 168,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.888",
        "ok": "1.888",
        "ko": "-"
    }
}
    },"req_get-mnemo-style-4f382": {
        type: "REQUEST",
        name: "GET/mnemo/styles/default.xml",
path: "GET/mnemo/styles/default.xml",
pathFormatted: "req_get-mnemo-style-4f382",
stats: {
    "name": "GET/mnemo/styles/default.xml",
    "numberOfRequests": {
        "total": "168",
        "ok": "168",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "245",
        "ok": "245",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "24",
        "ok": "24",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "17",
        "ok": "17",
        "ko": "-"
    },
    "percentiles1": {
        "total": "22",
        "ok": "22",
        "ko": "-"
    },
    "percentiles2": {
        "total": "23",
        "ok": "23",
        "ko": "-"
    },
    "percentiles3": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "percentiles4": {
        "total": "49",
        "ok": "49",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 168,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.888",
        "ok": "1.888",
        "ko": "-"
    }
}
    },"req_put-mnemoscheme-aabbe": {
        type: "REQUEST",
        name: "PUT/mnemoschemes/api/v1/mnemo",
path: "PUT/mnemoschemes/api/v1/mnemo",
pathFormatted: "req_put-mnemoscheme-aabbe",
stats: {
    "name": "PUT/mnemoschemes/api/v1/mnemo",
    "numberOfRequests": {
        "total": "125",
        "ok": "125",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "54",
        "ok": "54",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "558",
        "ok": "558",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "87",
        "ok": "87",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "54",
        "ok": "54",
        "ko": "-"
    },
    "percentiles1": {
        "total": "77",
        "ok": "77",
        "ko": "-"
    },
    "percentiles2": {
        "total": "95",
        "ok": "95",
        "ko": "-"
    },
    "percentiles3": {
        "total": "120",
        "ok": "120",
        "ko": "-"
    },
    "percentiles4": {
        "total": "305",
        "ok": "305",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 125,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.404",
        "ok": "1.404",
        "ko": "-"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
