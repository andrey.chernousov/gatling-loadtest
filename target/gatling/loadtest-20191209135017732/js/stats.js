var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "1831",
        "ok": "1831",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "20",
        "ok": "20",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "15049",
        "ok": "15049",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "229",
        "ok": "229",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "1360",
        "ok": "1360",
        "ko": "-"
    },
    "percentiles1": {
        "total": "23",
        "ok": "23",
        "ko": "-"
    },
    "percentiles2": {
        "total": "28",
        "ok": "28",
        "ko": "-"
    },
    "percentiles3": {
        "total": "269",
        "ok": "269",
        "ko": "-"
    },
    "percentiles4": {
        "total": "8404",
        "ok": "8404",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1782,
    "percentage": 97
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 9,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 40,
    "percentage": 2
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "59.065",
        "ok": "59.065",
        "ko": "-"
    }
},
contents: {
"req_login-99dea": {
        type: "REQUEST",
        name: "Login",
path: "Login",
pathFormatted: "req_login-99dea",
stats: {
    "name": "Login",
    "numberOfRequests": {
        "total": "60",
        "ok": "60",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "133",
        "ok": "133",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1162",
        "ok": "1162",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "237",
        "ok": "237",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "228",
        "ok": "228",
        "ko": "-"
    },
    "percentiles1": {
        "total": "170",
        "ok": "170",
        "ko": "-"
    },
    "percentiles2": {
        "total": "200",
        "ok": "200",
        "ko": "-"
    },
    "percentiles3": {
        "total": "639",
        "ok": "639",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1156",
        "ok": "1156",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 57,
    "percentage": 95
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 3,
    "percentage": 5
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.935",
        "ok": "1.935",
        "ko": "-"
    }
}
    },"req_-mnemoschemes-a-0c177": {
        type: "REQUEST",
        name: "/mnemoschemes/api/v1/mnemo/info",
path: "/mnemoschemes/api/v1/mnemo/info",
pathFormatted: "req_-mnemoschemes-a-0c177",
stats: {
    "name": "/mnemoschemes/api/v1/mnemo/info",
    "numberOfRequests": {
        "total": "119",
        "ok": "119",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "83",
        "ok": "83",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1259",
        "ok": "1259",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "184",
        "ok": "184",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "151",
        "ok": "151",
        "ko": "-"
    },
    "percentiles1": {
        "total": "141",
        "ok": "141",
        "ko": "-"
    },
    "percentiles2": {
        "total": "181",
        "ok": "181",
        "ko": "-"
    },
    "percentiles3": {
        "total": "486",
        "ok": "486",
        "ko": "-"
    },
    "percentiles4": {
        "total": "816",
        "ok": "816",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 117,
    "percentage": 98
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 1,
    "percentage": 1
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.839",
        "ok": "3.839",
        "ko": "-"
    }
}
    },"req_--6666c": {
        type: "REQUEST",
        name: "/",
path: "/",
pathFormatted: "req_--6666c",
stats: {
    "name": "/",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "245",
        "ok": "245",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "27",
        "ok": "27",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "31",
        "ok": "31",
        "ko": "-"
    },
    "percentiles1": {
        "total": "22",
        "ok": "22",
        "ko": "-"
    },
    "percentiles2": {
        "total": "24",
        "ok": "24",
        "ko": "-"
    },
    "percentiles3": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "percentiles4": {
        "total": "137",
        "ok": "137",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.613",
        "ok": "1.613",
        "ko": "-"
    }
}
    },"req_-built-scripts--beed0": {
        type: "REQUEST",
        name: "/built/scripts/zone.min.js",
path: "/built/scripts/zone.min.js",
pathFormatted: "req_-built-scripts--beed0",
stats: {
    "name": "/built/scripts/zone.min.js",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "20",
        "ok": "20",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "189",
        "ok": "189",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "43",
        "ok": "43",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "35",
        "ok": "35",
        "ko": "-"
    },
    "percentiles1": {
        "total": "24",
        "ok": "24",
        "ko": "-"
    },
    "percentiles2": {
        "total": "48",
        "ok": "48",
        "ko": "-"
    },
    "percentiles3": {
        "total": "91",
        "ok": "91",
        "ko": "-"
    },
    "percentiles4": {
        "total": "182",
        "ok": "182",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.613",
        "ok": "1.613",
        "ko": "-"
    }
}
    },"req_-built-scripts--0a490": {
        type: "REQUEST",
        name: "/built/scripts/import-map-overrides.js",
path: "/built/scripts/import-map-overrides.js",
pathFormatted: "req_-built-scripts--0a490",
stats: {
    "name": "/built/scripts/import-map-overrides.js",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "248",
        "ok": "248",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "32",
        "ok": "32",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "44",
        "ok": "44",
        "ko": "-"
    },
    "percentiles1": {
        "total": "22",
        "ok": "22",
        "ko": "-"
    },
    "percentiles2": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "percentiles3": {
        "total": "26",
        "ok": "26",
        "ko": "-"
    },
    "percentiles4": {
        "total": "246",
        "ok": "246",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.613",
        "ok": "1.613",
        "ko": "-"
    }
}
    },"req_-built-scripts--da42e": {
        type: "REQUEST",
        name: "/built/scripts/system.min.js",
path: "/built/scripts/system.min.js",
pathFormatted: "req_-built-scripts--da42e",
stats: {
    "name": "/built/scripts/system.min.js",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "247",
        "ok": "247",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "29",
        "ok": "29",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "34",
        "ok": "34",
        "ko": "-"
    },
    "percentiles1": {
        "total": "23",
        "ok": "23",
        "ko": "-"
    },
    "percentiles2": {
        "total": "24",
        "ok": "24",
        "ko": "-"
    },
    "percentiles3": {
        "total": "32",
        "ok": "32",
        "ko": "-"
    },
    "percentiles4": {
        "total": "183",
        "ok": "183",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.613",
        "ok": "1.613",
        "ko": "-"
    }
}
    },"req_-built-scripts--89aea": {
        type: "REQUEST",
        name: "/built/scripts/amd.min.js",
path: "/built/scripts/amd.min.js",
pathFormatted: "req_-built-scripts--89aea",
stats: {
    "name": "/built/scripts/amd.min.js",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "20",
        "ok": "20",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "34",
        "ok": "34",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "23",
        "ok": "23",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "2",
        "ok": "2",
        "ko": "-"
    },
    "percentiles1": {
        "total": "22",
        "ok": "22",
        "ko": "-"
    },
    "percentiles2": {
        "total": "23",
        "ok": "23",
        "ko": "-"
    },
    "percentiles3": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "percentiles4": {
        "total": "30",
        "ok": "30",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.613",
        "ok": "1.613",
        "ko": "-"
    }
}
    },"req_-built-scripts--dbe46": {
        type: "REQUEST",
        name: "/built/scripts/named-exports.js",
path: "/built/scripts/named-exports.js",
pathFormatted: "req_-built-scripts--dbe46",
stats: {
    "name": "/built/scripts/named-exports.js",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "584",
        "ok": "584",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "43",
        "ok": "43",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "90",
        "ok": "90",
        "ko": "-"
    },
    "percentiles1": {
        "total": "22",
        "ok": "22",
        "ko": "-"
    },
    "percentiles2": {
        "total": "24",
        "ok": "24",
        "ko": "-"
    },
    "percentiles3": {
        "total": "152",
        "ok": "152",
        "ko": "-"
    },
    "percentiles4": {
        "total": "427",
        "ok": "427",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.613",
        "ok": "1.613",
        "ko": "-"
    }
}
    },"req_-built-scripts--e6e7f": {
        type: "REQUEST",
        name: "/built/scripts/named-register.min.js",
path: "/built/scripts/named-register.min.js",
pathFormatted: "req_-built-scripts--e6e7f",
stats: {
    "name": "/built/scripts/named-register.min.js",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "254",
        "ok": "254",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "32",
        "ok": "32",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "45",
        "ok": "45",
        "ko": "-"
    },
    "percentiles1": {
        "total": "22",
        "ok": "22",
        "ko": "-"
    },
    "percentiles2": {
        "total": "23",
        "ok": "23",
        "ko": "-"
    },
    "percentiles3": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "percentiles4": {
        "total": "251",
        "ok": "251",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.613",
        "ok": "1.613",
        "ko": "-"
    }
}
    },"req_-built-vendors--57cfa": {
        type: "REQUEST",
        name: "/built/vendors~main.js",
path: "/built/vendors~main.js",
pathFormatted: "req_-built-vendors--57cfa",
stats: {
    "name": "/built/vendors~main.js",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1284",
        "ok": "1284",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "154",
        "ok": "154",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "236",
        "ok": "236",
        "ko": "-"
    },
    "percentiles1": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "percentiles2": {
        "total": "240",
        "ok": "240",
        "ko": "-"
    },
    "percentiles3": {
        "total": "495",
        "ok": "495",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1064",
        "ok": "1064",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 48,
    "percentage": 96
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 2
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 1,
    "percentage": 2
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.613",
        "ok": "1.613",
        "ko": "-"
    }
}
    },"req_-built-main-js-746c8": {
        type: "REQUEST",
        name: "/built/main.js",
path: "/built/main.js",
pathFormatted: "req_-built-main-js-746c8",
stats: {
    "name": "/built/main.js",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "374",
        "ok": "374",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "43",
        "ok": "43",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "71",
        "ok": "71",
        "ko": "-"
    },
    "percentiles1": {
        "total": "22",
        "ok": "22",
        "ko": "-"
    },
    "percentiles2": {
        "total": "24",
        "ok": "24",
        "ko": "-"
    },
    "percentiles3": {
        "total": "247",
        "ok": "247",
        "ko": "-"
    },
    "percentiles4": {
        "total": "312",
        "ok": "312",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.613",
        "ok": "1.613",
        "ko": "-"
    }
}
    },"req_-built-assets-s-b3836": {
        type: "REQUEST",
        name: "/built/assets/settings.json",
path: "/built/assets/settings.json",
pathFormatted: "req_-built-assets-s-b3836",
stats: {
    "name": "/built/assets/settings.json",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "587",
        "ok": "587",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "58",
        "ok": "58",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "119",
        "ok": "119",
        "ko": "-"
    },
    "percentiles1": {
        "total": "22",
        "ok": "22",
        "ko": "-"
    },
    "percentiles2": {
        "total": "24",
        "ok": "24",
        "ko": "-"
    },
    "percentiles3": {
        "total": "248",
        "ok": "248",
        "ko": "-"
    },
    "percentiles4": {
        "total": "579",
        "ok": "579",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.613",
        "ok": "1.613",
        "ko": "-"
    }
}
    },"req_-auth-realms-ma-5a21e": {
        type: "REQUEST",
        name: "/auth/realms/master/account",
path: "/auth/realms/master/account",
pathFormatted: "req_-auth-realms-ma-5a21e",
stats: {
    "name": "/auth/realms/master/account",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "22",
        "ok": "22",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "247",
        "ok": "247",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "28",
        "ok": "28",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "31",
        "ok": "31",
        "ko": "-"
    },
    "percentiles1": {
        "total": "23",
        "ok": "23",
        "ko": "-"
    },
    "percentiles2": {
        "total": "24",
        "ok": "24",
        "ko": "-"
    },
    "percentiles3": {
        "total": "26",
        "ok": "26",
        "ko": "-"
    },
    "percentiles4": {
        "total": "139",
        "ok": "139",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.613",
        "ok": "1.613",
        "ko": "-"
    }
}
    },"req_-auth-realms-ma-0613f": {
        type: "REQUEST",
        name: "/auth/realms/master/account Redirect 1",
path: "/auth/realms/master/account Redirect 1",
pathFormatted: "req_-auth-realms-ma-0613f",
stats: {
    "name": "/auth/realms/master/account Redirect 1",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "28",
        "ok": "28",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "364",
        "ok": "364",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "40",
        "ok": "40",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "47",
        "ok": "47",
        "ko": "-"
    },
    "percentiles1": {
        "total": "31",
        "ok": "31",
        "ko": "-"
    },
    "percentiles2": {
        "total": "33",
        "ok": "33",
        "ko": "-"
    },
    "percentiles3": {
        "total": "53",
        "ok": "53",
        "ko": "-"
    },
    "percentiles4": {
        "total": "214",
        "ok": "214",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.613",
        "ok": "1.613",
        "ko": "-"
    }
}
    },"req_-json-applicati-ca958": {
        type: "REQUEST",
        name: "/json/applicationUnits",
path: "/json/applicationUnits",
pathFormatted: "req_-json-applicati-ca958",
stats: {
    "name": "/json/applicationUnits",
    "numberOfRequests": {
        "total": "100",
        "ok": "100",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "36",
        "ok": "36",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "23",
        "ok": "23",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "2",
        "ok": "2",
        "ko": "-"
    },
    "percentiles1": {
        "total": "22",
        "ok": "22",
        "ko": "-"
    },
    "percentiles2": {
        "total": "23",
        "ok": "23",
        "ko": "-"
    },
    "percentiles3": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "percentiles4": {
        "total": "36",
        "ok": "36",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 100,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.226",
        "ok": "3.226",
        "ko": "-"
    }
}
    },"req_-built-scripts--a12a0": {
        type: "REQUEST",
        name: "/built/scripts/single-spa.min.js",
path: "/built/scripts/single-spa.min.js",
pathFormatted: "req_-built-scripts--a12a0",
stats: {
    "name": "/built/scripts/single-spa.min.js",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "454",
        "ok": "454",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "53",
        "ok": "53",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "84",
        "ok": "84",
        "ko": "-"
    },
    "percentiles1": {
        "total": "24",
        "ok": "24",
        "ko": "-"
    },
    "percentiles2": {
        "total": "44",
        "ok": "44",
        "ko": "-"
    },
    "percentiles3": {
        "total": "158",
        "ok": "158",
        "ko": "-"
    },
    "percentiles4": {
        "total": "433",
        "ok": "433",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.613",
        "ok": "1.613",
        "ko": "-"
    }
}
    },"req_-json-usersetti-341fa": {
        type: "REQUEST",
        name: "/json/UserSettings",
path: "/json/UserSettings",
pathFormatted: "req_-json-usersetti-341fa",
stats: {
    "name": "/json/UserSettings",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "26",
        "ok": "26",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "23",
        "ok": "23",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "1",
        "ok": "1",
        "ko": "-"
    },
    "percentiles1": {
        "total": "22",
        "ok": "22",
        "ko": "-"
    },
    "percentiles2": {
        "total": "24",
        "ok": "24",
        "ko": "-"
    },
    "percentiles3": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "percentiles4": {
        "total": "26",
        "ok": "26",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.613",
        "ok": "1.613",
        "ko": "-"
    }
}
    },"req_-json-usercommo-43a62": {
        type: "REQUEST",
        name: "/json/UserCommonApplications",
path: "/json/UserCommonApplications",
pathFormatted: "req_-json-usercommo-43a62",
stats: {
    "name": "/json/UserCommonApplications",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "247",
        "ok": "247",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "32",
        "ok": "32",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "44",
        "ok": "44",
        "ko": "-"
    },
    "percentiles1": {
        "total": "22",
        "ok": "22",
        "ko": "-"
    },
    "percentiles2": {
        "total": "24",
        "ok": "24",
        "ko": "-"
    },
    "percentiles3": {
        "total": "35",
        "ok": "35",
        "ko": "-"
    },
    "percentiles4": {
        "total": "247",
        "ok": "247",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.613",
        "ok": "1.613",
        "ko": "-"
    }
}
    },"req_-hostrootshell--a77cd": {
        type: "REQUEST",
        name: "/hostRootShell/assets/i18n/en_US.json",
path: "/hostRootShell/assets/i18n/en_US.json",
pathFormatted: "req_-hostrootshell--a77cd",
stats: {
    "name": "/hostRootShell/assets/i18n/en_US.json",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "103",
        "ok": "103",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "11",
        "ok": "11",
        "ko": "-"
    },
    "percentiles1": {
        "total": "23",
        "ok": "23",
        "ko": "-"
    },
    "percentiles2": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "percentiles3": {
        "total": "26",
        "ok": "26",
        "ko": "-"
    },
    "percentiles4": {
        "total": "66",
        "ok": "66",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.613",
        "ok": "1.613",
        "ko": "-"
    }
}
    },"req_-hostmnemo-scri-e7b48": {
        type: "REQUEST",
        name: "/hostMnemo/scripts.js",
path: "/hostMnemo/scripts.js",
pathFormatted: "req_-hostmnemo-scri-e7b48",
stats: {
    "name": "/hostMnemo/scripts.js",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5710",
        "ok": "5710",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1242",
        "ok": "1242",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "1761",
        "ok": "1761",
        "ko": "-"
    },
    "percentiles1": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2170",
        "ok": "2170",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5022",
        "ok": "5022",
        "ko": "-"
    },
    "percentiles4": {
        "total": "5637",
        "ok": "5637",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 31,
    "percentage": 62
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 2
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 18,
    "percentage": 36
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.613",
        "ok": "1.613",
        "ko": "-"
    }
}
    },"req_-mnemo-css-comm-d07a9": {
        type: "REQUEST",
        name: "/mnemo/css/common.css",
path: "/mnemo/css/common.css",
pathFormatted: "req_-mnemo-css-comm-d07a9",
stats: {
    "name": "/mnemo/css/common.css",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "247",
        "ok": "247",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "33",
        "ok": "33",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "44",
        "ok": "44",
        "ko": "-"
    },
    "percentiles1": {
        "total": "24",
        "ok": "24",
        "ko": "-"
    },
    "percentiles2": {
        "total": "26",
        "ok": "26",
        "ko": "-"
    },
    "percentiles3": {
        "total": "40",
        "ok": "40",
        "ko": "-"
    },
    "percentiles4": {
        "total": "247",
        "ok": "247",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.613",
        "ok": "1.613",
        "ko": "-"
    }
}
    },"req_-mnemo-resource-1c1a3": {
        type: "REQUEST",
        name: "/mnemo/resources/graph.txt",
path: "/mnemo/resources/graph.txt",
pathFormatted: "req_-mnemo-resource-1c1a3",
stats: {
    "name": "/mnemo/resources/graph.txt",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "246",
        "ok": "246",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "33",
        "ok": "33",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "43",
        "ok": "43",
        "ko": "-"
    },
    "percentiles1": {
        "total": "24",
        "ok": "24",
        "ko": "-"
    },
    "percentiles2": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "percentiles3": {
        "total": "33",
        "ok": "33",
        "ko": "-"
    },
    "percentiles4": {
        "total": "245",
        "ok": "245",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.613",
        "ok": "1.613",
        "ko": "-"
    }
}
    },"req_-mnemo-resource-4d1f0": {
        type: "REQUEST",
        name: "/mnemo/resources/graph_ru.txt",
path: "/mnemo/resources/graph_ru.txt",
pathFormatted: "req_-mnemo-resource-4d1f0",
stats: {
    "name": "/mnemo/resources/graph_ru.txt",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "249",
        "ok": "249",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "33",
        "ok": "33",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "44",
        "ok": "44",
        "ko": "-"
    },
    "percentiles1": {
        "total": "24",
        "ok": "24",
        "ko": "-"
    },
    "percentiles2": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "percentiles3": {
        "total": "32",
        "ok": "32",
        "ko": "-"
    },
    "percentiles4": {
        "total": "246",
        "ok": "246",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.613",
        "ok": "1.613",
        "ko": "-"
    }
}
    },"req_-mnemo-resource-82cfb": {
        type: "REQUEST",
        name: "/mnemo/resources/editor.txt",
path: "/mnemo/resources/editor.txt",
pathFormatted: "req_-mnemo-resource-82cfb",
stats: {
    "name": "/mnemo/resources/editor.txt",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "249",
        "ok": "249",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "32",
        "ok": "32",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "44",
        "ok": "44",
        "ko": "-"
    },
    "percentiles1": {
        "total": "23",
        "ok": "23",
        "ko": "-"
    },
    "percentiles2": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "percentiles3": {
        "total": "26",
        "ok": "26",
        "ko": "-"
    },
    "percentiles4": {
        "total": "248",
        "ok": "248",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.613",
        "ok": "1.613",
        "ko": "-"
    }
}
    },"req_-mnemo-resource-33452": {
        type: "REQUEST",
        name: "/mnemo/resources/editor_ru.txt",
path: "/mnemo/resources/editor_ru.txt",
pathFormatted: "req_-mnemo-resource-33452",
stats: {
    "name": "/mnemo/resources/editor_ru.txt",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "247",
        "ok": "247",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "33",
        "ok": "33",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "44",
        "ok": "44",
        "ko": "-"
    },
    "percentiles1": {
        "total": "23",
        "ok": "23",
        "ko": "-"
    },
    "percentiles2": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "percentiles3": {
        "total": "35",
        "ok": "35",
        "ko": "-"
    },
    "percentiles4": {
        "total": "246",
        "ok": "246",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.613",
        "ok": "1.613",
        "ko": "-"
    }
}
    },"req_-hostmnemo-main-5bf99": {
        type: "REQUEST",
        name: "/hostMnemo/main.js",
path: "/hostMnemo/main.js",
pathFormatted: "req_-hostmnemo-main-5bf99",
stats: {
    "name": "/hostMnemo/main.js",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "15049",
        "ok": "15049",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "5025",
        "ok": "5025",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "6262",
        "ok": "6262",
        "ko": "-"
    },
    "percentiles1": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "percentiles2": {
        "total": "12403",
        "ok": "12403",
        "ko": "-"
    },
    "percentiles3": {
        "total": "14838",
        "ok": "14838",
        "ko": "-"
    },
    "percentiles4": {
        "total": "15014",
        "ok": "15014",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 30,
    "percentage": 60
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 20,
    "percentage": 40
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.613",
        "ok": "1.613",
        "ko": "-"
    }
}
    },"req_-hostmnemo-asse-3384a": {
        type: "REQUEST",
        name: "/hostMnemo/assets/i18n/en_US.json",
path: "/hostMnemo/assets/i18n/en_US.json",
pathFormatted: "req_-hostmnemo-asse-3384a",
stats: {
    "name": "/hostMnemo/assets/i18n/en_US.json",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "27",
        "ok": "27",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "23",
        "ok": "23",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "1",
        "ok": "1",
        "ko": "-"
    },
    "percentiles1": {
        "total": "22",
        "ok": "22",
        "ko": "-"
    },
    "percentiles2": {
        "total": "24",
        "ok": "24",
        "ko": "-"
    },
    "percentiles3": {
        "total": "26",
        "ok": "26",
        "ko": "-"
    },
    "percentiles4": {
        "total": "27",
        "ok": "27",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.613",
        "ok": "1.613",
        "ko": "-"
    }
}
    },"req_-hostmnemo-asse-1c9a9": {
        type: "REQUEST",
        name: "/hostMnemo/assets/i18n/zyfra-mnemo/zyfraMnemoModule.en_US.json",
path: "/hostMnemo/assets/i18n/zyfra-mnemo/zyfraMnemoModule.en_US.json",
pathFormatted: "req_-hostmnemo-asse-1c9a9",
stats: {
    "name": "/hostMnemo/assets/i18n/zyfra-mnemo/zyfraMnemoModule.en_US.json",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1166",
        "ok": "1166",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "55",
        "ok": "55",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "165",
        "ok": "165",
        "ko": "-"
    },
    "percentiles1": {
        "total": "24",
        "ok": "24",
        "ko": "-"
    },
    "percentiles2": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "percentiles3": {
        "total": "147",
        "ok": "147",
        "ko": "-"
    },
    "percentiles4": {
        "total": "716",
        "ok": "716",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 49,
    "percentage": 98
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 2
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.613",
        "ok": "1.613",
        "ko": "-"
    }
}
    },"req_-hostmnemo-asse-dc6b4": {
        type: "REQUEST",
        name: "/hostMnemo/assets/settings.json",
path: "/hostMnemo/assets/settings.json",
pathFormatted: "req_-hostmnemo-asse-dc6b4",
stats: {
    "name": "/hostMnemo/assets/settings.json",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1046",
        "ok": "1046",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "50",
        "ok": "50",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "147",
        "ok": "147",
        "ko": "-"
    },
    "percentiles1": {
        "total": "23",
        "ok": "23",
        "ko": "-"
    },
    "percentiles2": {
        "total": "24",
        "ok": "24",
        "ko": "-"
    },
    "percentiles3": {
        "total": "51",
        "ok": "51",
        "ko": "-"
    },
    "percentiles4": {
        "total": "667",
        "ok": "667",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 49,
    "percentage": 98
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 2
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.613",
        "ok": "1.613",
        "ko": "-"
    }
}
    },"req_-mnemo-resource-a83c8": {
        type: "REQUEST",
        name: "/mnemo/resources/grapheditor.txt",
path: "/mnemo/resources/grapheditor.txt",
pathFormatted: "req_-mnemo-resource-a83c8",
stats: {
    "name": "/mnemo/resources/grapheditor.txt",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "249",
        "ok": "249",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "29",
        "ok": "29",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "32",
        "ok": "32",
        "ko": "-"
    },
    "percentiles1": {
        "total": "23",
        "ok": "23",
        "ko": "-"
    },
    "percentiles2": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "percentiles3": {
        "total": "27",
        "ok": "27",
        "ko": "-"
    },
    "percentiles4": {
        "total": "160",
        "ok": "160",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.613",
        "ok": "1.613",
        "ko": "-"
    }
}
    },"req_-mnemo-styles-d-ee36a": {
        type: "REQUEST",
        name: "/mnemo/styles/default.xml",
path: "/mnemo/styles/default.xml",
pathFormatted: "req_-mnemo-styles-d-ee36a",
stats: {
    "name": "/mnemo/styles/default.xml",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "275",
        "ok": "275",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "43",
        "ok": "43",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "63",
        "ok": "63",
        "ko": "-"
    },
    "percentiles1": {
        "total": "23",
        "ok": "23",
        "ko": "-"
    },
    "percentiles2": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "percentiles3": {
        "total": "247",
        "ok": "247",
        "ko": "-"
    },
    "percentiles4": {
        "total": "263",
        "ok": "263",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.613",
        "ok": "1.613",
        "ko": "-"
    }
}
    },"req_-mnemo-stencils-43d94": {
        type: "REQUEST",
        name: "/mnemo/stencils/mnemo.library.xml",
path: "/mnemo/stencils/mnemo.library.xml",
pathFormatted: "req_-mnemo-stencils-43d94",
stats: {
    "name": "/mnemo/stencils/mnemo.library.xml",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "824",
        "ok": "824",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "127",
        "ok": "127",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "192",
        "ok": "192",
        "ko": "-"
    },
    "percentiles1": {
        "total": "26",
        "ok": "26",
        "ko": "-"
    },
    "percentiles2": {
        "total": "136",
        "ok": "136",
        "ko": "-"
    },
    "percentiles3": {
        "total": "608",
        "ok": "608",
        "ko": "-"
    },
    "percentiles4": {
        "total": "759",
        "ok": "759",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 49,
    "percentage": 98
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 2
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.613",
        "ok": "1.613",
        "ko": "-"
    }
}
    },"req_get-mnemoscheme-b6811": {
        type: "REQUEST",
        name: "GET/mnemoschemes/api/v1/mnemo/",
path: "GET/mnemoschemes/api/v1/mnemo/",
pathFormatted: "req_get-mnemoscheme-b6811",
stats: {
    "name": "GET/mnemoschemes/api/v1/mnemo/",
    "numberOfRequests": {
        "total": "31",
        "ok": "31",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "55",
        "ok": "55",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "525",
        "ok": "525",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "206",
        "ok": "206",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "131",
        "ok": "131",
        "ko": "-"
    },
    "percentiles1": {
        "total": "205",
        "ok": "205",
        "ko": "-"
    },
    "percentiles2": {
        "total": "271",
        "ok": "271",
        "ko": "-"
    },
    "percentiles3": {
        "total": "443",
        "ok": "443",
        "ko": "-"
    },
    "percentiles4": {
        "total": "506",
        "ok": "506",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 31,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1",
        "ok": "1",
        "ko": "-"
    }
}
    },"req_get-mnemo-resou-96eb1": {
        type: "REQUEST",
        name: "GET/mnemo/resources/grapheditor_ru.txt",
path: "GET/mnemo/resources/grapheditor_ru.txt",
pathFormatted: "req_get-mnemo-resou-96eb1",
stats: {
    "name": "GET/mnemo/resources/grapheditor_ru.txt",
    "numberOfRequests": {
        "total": "31",
        "ok": "31",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "44",
        "ok": "44",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "24",
        "ok": "24",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "4",
        "ok": "4",
        "ko": "-"
    },
    "percentiles1": {
        "total": "23",
        "ok": "23",
        "ko": "-"
    },
    "percentiles2": {
        "total": "24",
        "ok": "24",
        "ko": "-"
    },
    "percentiles3": {
        "total": "27",
        "ok": "27",
        "ko": "-"
    },
    "percentiles4": {
        "total": "39",
        "ok": "39",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 31,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1",
        "ok": "1",
        "ko": "-"
    }
}
    },"req_get-mnemo-style-4f382": {
        type: "REQUEST",
        name: "GET/mnemo/styles/default.xml",
path: "GET/mnemo/styles/default.xml",
pathFormatted: "req_get-mnemo-style-4f382",
stats: {
    "name": "GET/mnemo/styles/default.xml",
    "numberOfRequests": {
        "total": "31",
        "ok": "31",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "248",
        "ok": "248",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "30",
        "ok": "30",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "40",
        "ok": "40",
        "ko": "-"
    },
    "percentiles1": {
        "total": "22",
        "ok": "22",
        "ko": "-"
    },
    "percentiles2": {
        "total": "24",
        "ok": "24",
        "ko": "-"
    },
    "percentiles3": {
        "total": "26",
        "ok": "26",
        "ko": "-"
    },
    "percentiles4": {
        "total": "181",
        "ok": "181",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 31,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1",
        "ok": "1",
        "ko": "-"
    }
}
    },"req_put-mnemoscheme-aabbe": {
        type: "REQUEST",
        name: "PUT/mnemoschemes/api/v1/mnemo",
path: "PUT/mnemoschemes/api/v1/mnemo",
pathFormatted: "req_put-mnemoscheme-aabbe",
stats: {
    "name": "PUT/mnemoschemes/api/v1/mnemo",
    "numberOfRequests": {
        "total": "8",
        "ok": "8",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "90",
        "ok": "90",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "197",
        "ok": "197",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "125",
        "ok": "125",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "35",
        "ok": "35",
        "ko": "-"
    },
    "percentiles1": {
        "total": "110",
        "ok": "110",
        "ko": "-"
    },
    "percentiles2": {
        "total": "137",
        "ok": "137",
        "ko": "-"
    },
    "percentiles3": {
        "total": "185",
        "ok": "185",
        "ko": "-"
    },
    "percentiles4": {
        "total": "195",
        "ok": "195",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 8,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.258",
        "ok": "0.258",
        "ko": "-"
    }
}
    },"req_post-mnemoschem-dba6c": {
        type: "REQUEST",
        name: "POST/mnemoschemes/api/v1/mnemo",
path: "POST/mnemoschemes/api/v1/mnemo",
pathFormatted: "req_post-mnemoschem-dba6c",
stats: {
    "name": "POST/mnemoschemes/api/v1/mnemo",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "30",
        "ok": "30",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "30",
        "ok": "30",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "30",
        "ok": "30",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "30",
        "ok": "30",
        "ko": "-"
    },
    "percentiles2": {
        "total": "30",
        "ok": "30",
        "ko": "-"
    },
    "percentiles3": {
        "total": "30",
        "ok": "30",
        "ko": "-"
    },
    "percentiles4": {
        "total": "30",
        "ok": "30",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.032",
        "ok": "0.032",
        "ko": "-"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
