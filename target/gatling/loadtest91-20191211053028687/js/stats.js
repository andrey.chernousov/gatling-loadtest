var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "1825",
        "ok": "1775",
        "ko": "50"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "23"
    },
    "maxResponseTime": {
        "total": "15025",
        "ok": "15025",
        "ko": "1057"
    },
    "meanResponseTime": {
        "total": "234",
        "ok": "238",
        "ko": "90"
    },
    "standardDeviation": {
        "total": "1320",
        "ok": "1338",
        "ko": "210"
    },
    "percentiles1": {
        "total": "27",
        "ok": "26",
        "ko": "34"
    },
    "percentiles2": {
        "total": "44",
        "ok": "44",
        "ko": "42"
    },
    "percentiles3": {
        "total": "405",
        "ok": "409",
        "ko": "401"
    },
    "percentiles4": {
        "total": "9779",
        "ok": "9799",
        "ko": "1056"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1723,
    "percentage": 94
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 8,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 44,
    "percentage": 2
},
    "group4": {
    "name": "failed",
    "count": 50,
    "percentage": 3
},
    "meanNumberOfRequestsPerSecond": {
        "total": "65.179",
        "ok": "63.393",
        "ko": "1.786"
    }
},
contents: {
"req_login-99dea": {
        type: "REQUEST",
        name: "Login",
path: "Login",
pathFormatted: "req_login-99dea",
stats: {
    "name": "Login",
    "numberOfRequests": {
        "total": "60",
        "ok": "60",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "103",
        "ok": "103",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "571",
        "ok": "571",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "155",
        "ok": "155",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "90",
        "ok": "90",
        "ko": "-"
    },
    "percentiles1": {
        "total": "132",
        "ok": "132",
        "ko": "-"
    },
    "percentiles2": {
        "total": "154",
        "ok": "154",
        "ko": "-"
    },
    "percentiles3": {
        "total": "214",
        "ok": "214",
        "ko": "-"
    },
    "percentiles4": {
        "total": "549",
        "ok": "549",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 60,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.143",
        "ok": "2.143",
        "ko": "-"
    }
}
    },"req_get-mnemobase-a-1be13": {
        type: "REQUEST",
        name: "GET/mnemobase/api/v1/mnemo/info",
path: "GET/mnemobase/api/v1/mnemo/info",
pathFormatted: "req_get-mnemobase-a-1be13",
stats: {
    "name": "GET/mnemobase/api/v1/mnemo/info",
    "numberOfRequests": {
        "total": "120",
        "ok": "120",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "52",
        "ok": "52",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "691",
        "ok": "691",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "146",
        "ok": "146",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "88",
        "ok": "88",
        "ko": "-"
    },
    "percentiles1": {
        "total": "117",
        "ok": "117",
        "ko": "-"
    },
    "percentiles2": {
        "total": "160",
        "ok": "160",
        "ko": "-"
    },
    "percentiles3": {
        "total": "267",
        "ok": "267",
        "ko": "-"
    },
    "percentiles4": {
        "total": "511",
        "ok": "511",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 120,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.286",
        "ok": "4.286",
        "ko": "-"
    }
}
    },"req_post-mnemobase--159fa": {
        type: "REQUEST",
        name: "POST/mnemobase/api/v1/mnemo",
path: "POST/mnemobase/api/v1/mnemo",
pathFormatted: "req_post-mnemobase--159fa",
stats: {
    "name": "POST/mnemobase/api/v1/mnemo",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "75",
        "ok": "75",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "75",
        "ok": "75",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "75",
        "ok": "75",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "75",
        "ok": "75",
        "ko": "-"
    },
    "percentiles2": {
        "total": "75",
        "ok": "75",
        "ko": "-"
    },
    "percentiles3": {
        "total": "75",
        "ok": "75",
        "ko": "-"
    },
    "percentiles4": {
        "total": "75",
        "ok": "75",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.036",
        "ok": "0.036",
        "ko": "-"
    }
}
    },"req_get--e0e39": {
        type: "REQUEST",
        name: "GET/",
path: "GET/",
pathFormatted: "req_get--e0e39",
stats: {
    "name": "GET/",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "26",
        "ok": "26",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "406",
        "ok": "406",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "65",
        "ok": "65",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "61",
        "ok": "61",
        "ko": "-"
    },
    "percentiles1": {
        "total": "50",
        "ok": "50",
        "ko": "-"
    },
    "percentiles2": {
        "total": "62",
        "ok": "62",
        "ko": "-"
    },
    "percentiles3": {
        "total": "85",
        "ok": "85",
        "ko": "-"
    },
    "percentiles4": {
        "total": "353",
        "ok": "353",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.786",
        "ok": "1.786",
        "ko": "-"
    }
}
    },"req_get-built-scrip-f7a2f": {
        type: "REQUEST",
        name: "GET/built/scripts/zone.min.js",
path: "GET/built/scripts/zone.min.js",
pathFormatted: "req_get-built-scrip-f7a2f",
stats: {
    "name": "GET/built/scripts/zone.min.js",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "642",
        "ok": "642",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "98",
        "ok": "98",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "163",
        "ok": "163",
        "ko": "-"
    },
    "percentiles1": {
        "total": "29",
        "ok": "29",
        "ko": "-"
    },
    "percentiles2": {
        "total": "69",
        "ok": "69",
        "ko": "-"
    },
    "percentiles3": {
        "total": "555",
        "ok": "555",
        "ko": "-"
    },
    "percentiles4": {
        "total": "615",
        "ok": "615",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.786",
        "ok": "1.786",
        "ko": "-"
    }
}
    },"req_get-built-scrip-6c713": {
        type: "REQUEST",
        name: "GET/built/scripts/import-map-overrides.js",
path: "GET/built/scripts/import-map-overrides.js",
pathFormatted: "req_get-built-scrip-6c713",
stats: {
    "name": "GET/built/scripts/import-map-overrides.js",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "265",
        "ok": "265",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "33",
        "ok": "33",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "34",
        "ok": "34",
        "ko": "-"
    },
    "percentiles1": {
        "total": "26",
        "ok": "26",
        "ko": "-"
    },
    "percentiles2": {
        "total": "34",
        "ok": "34",
        "ko": "-"
    },
    "percentiles3": {
        "total": "46",
        "ok": "46",
        "ko": "-"
    },
    "percentiles4": {
        "total": "159",
        "ok": "159",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.786",
        "ok": "1.786",
        "ko": "-"
    }
}
    },"req_get-built-scrip-3e0b8": {
        type: "REQUEST",
        name: "GET/built/scripts/system.min.js",
path: "GET/built/scripts/system.min.js",
pathFormatted: "req_get-built-scrip-3e0b8",
stats: {
    "name": "GET/built/scripts/system.min.js",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "799",
        "ok": "799",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "44",
        "ok": "44",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "108",
        "ok": "108",
        "ko": "-"
    },
    "percentiles1": {
        "total": "26",
        "ok": "26",
        "ko": "-"
    },
    "percentiles2": {
        "total": "32",
        "ok": "32",
        "ko": "-"
    },
    "percentiles3": {
        "total": "42",
        "ok": "42",
        "ko": "-"
    },
    "percentiles4": {
        "total": "429",
        "ok": "429",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.786",
        "ok": "1.786",
        "ko": "-"
    }
}
    },"req_get-built-scrip-a6908": {
        type: "REQUEST",
        name: "GET/built/scripts/amd.min.js",
path: "GET/built/scripts/amd.min.js",
pathFormatted: "req_get-built-scrip-a6908",
stats: {
    "name": "GET/built/scripts/amd.min.js",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "258",
        "ok": "258",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "34",
        "ok": "34",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "33",
        "ok": "33",
        "ko": "-"
    },
    "percentiles1": {
        "total": "26",
        "ok": "26",
        "ko": "-"
    },
    "percentiles2": {
        "total": "34",
        "ok": "34",
        "ko": "-"
    },
    "percentiles3": {
        "total": "43",
        "ok": "43",
        "ko": "-"
    },
    "percentiles4": {
        "total": "154",
        "ok": "154",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.786",
        "ok": "1.786",
        "ko": "-"
    }
}
    },"req_get-built-scrip-a5f03": {
        type: "REQUEST",
        name: "GET/built/scripts/named-exports.js",
path: "GET/built/scripts/named-exports.js",
pathFormatted: "req_get-built-scrip-a5f03",
stats: {
    "name": "GET/built/scripts/named-exports.js",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "417",
        "ok": "417",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "36",
        "ok": "36",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "55",
        "ok": "55",
        "ko": "-"
    },
    "percentiles1": {
        "total": "26",
        "ok": "26",
        "ko": "-"
    },
    "percentiles2": {
        "total": "35",
        "ok": "35",
        "ko": "-"
    },
    "percentiles3": {
        "total": "42",
        "ok": "42",
        "ko": "-"
    },
    "percentiles4": {
        "total": "234",
        "ok": "234",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.786",
        "ok": "1.786",
        "ko": "-"
    }
}
    },"req_get-built-scrip-96ae1": {
        type: "REQUEST",
        name: "GET/built/scripts/named-register.min.js",
path: "GET/built/scripts/named-register.min.js",
pathFormatted: "req_get-built-scrip-96ae1",
stats: {
    "name": "GET/built/scripts/named-register.min.js",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "301",
        "ok": "301",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "44",
        "ok": "44",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "60",
        "ok": "60",
        "ko": "-"
    },
    "percentiles1": {
        "total": "28",
        "ok": "28",
        "ko": "-"
    },
    "percentiles2": {
        "total": "36",
        "ok": "36",
        "ko": "-"
    },
    "percentiles3": {
        "total": "165",
        "ok": "165",
        "ko": "-"
    },
    "percentiles4": {
        "total": "286",
        "ok": "286",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.786",
        "ok": "1.786",
        "ko": "-"
    }
}
    },"req_get-built-vendo-cdd2c": {
        type: "REQUEST",
        name: "GET/built/vendors~main.js",
path: "GET/built/vendors~main.js",
pathFormatted: "req_get-built-vendo-cdd2c",
stats: {
    "name": "GET/built/vendors~main.js",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1582",
        "ok": "1582",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "317",
        "ok": "317",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "424",
        "ok": "424",
        "ko": "-"
    },
    "percentiles1": {
        "total": "30",
        "ok": "30",
        "ko": "-"
    },
    "percentiles2": {
        "total": "559",
        "ok": "559",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1208",
        "ok": "1208",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1476",
        "ok": "1476",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 41,
    "percentage": 82
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 6,
    "percentage": 12
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 3,
    "percentage": 6
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.786",
        "ok": "1.786",
        "ko": "-"
    }
}
    },"req_get-built-main--37f57": {
        type: "REQUEST",
        name: "GET/built/main.js",
path: "GET/built/main.js",
pathFormatted: "req_get-built-main--37f57",
stats: {
    "name": "GET/built/main.js",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "475",
        "ok": "475",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "57",
        "ok": "57",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "105",
        "ok": "105",
        "ko": "-"
    },
    "percentiles1": {
        "total": "26",
        "ok": "26",
        "ko": "-"
    },
    "percentiles2": {
        "total": "36",
        "ok": "36",
        "ko": "-"
    },
    "percentiles3": {
        "total": "302",
        "ok": "302",
        "ko": "-"
    },
    "percentiles4": {
        "total": "473",
        "ok": "473",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.786",
        "ok": "1.786",
        "ko": "-"
    }
}
    },"req_get-built-asset-4566d": {
        type: "REQUEST",
        name: "GET/built/assets/settings.json",
path: "GET/built/assets/settings.json",
pathFormatted: "req_get-built-asset-4566d",
stats: {
    "name": "GET/built/assets/settings.json",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "22",
        "ok": "22",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "278",
        "ok": "278",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "38",
        "ok": "38",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "49",
        "ok": "49",
        "ko": "-"
    },
    "percentiles1": {
        "total": "26",
        "ok": "26",
        "ko": "-"
    },
    "percentiles2": {
        "total": "33",
        "ok": "33",
        "ko": "-"
    },
    "percentiles3": {
        "total": "45",
        "ok": "45",
        "ko": "-"
    },
    "percentiles4": {
        "total": "278",
        "ok": "278",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.786",
        "ok": "1.786",
        "ko": "-"
    }
}
    },"req_get-auth-realms-24c57": {
        type: "REQUEST",
        name: "GET/auth/realms/master/account",
path: "GET/auth/realms/master/account",
pathFormatted: "req_get-auth-realms-24c57",
stats: {
    "name": "GET/auth/realms/master/account",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "23",
        "ok": "23",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "294",
        "ok": "294",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "53",
        "ok": "53",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "59",
        "ok": "59",
        "ko": "-"
    },
    "percentiles1": {
        "total": "40",
        "ok": "40",
        "ko": "-"
    },
    "percentiles2": {
        "total": "47",
        "ok": "47",
        "ko": "-"
    },
    "percentiles3": {
        "total": "183",
        "ok": "183",
        "ko": "-"
    },
    "percentiles4": {
        "total": "287",
        "ok": "287",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.786",
        "ok": "1.786",
        "ko": "-"
    }
}
    },"req_get-auth-realms-177d8": {
        type: "REQUEST",
        name: "GET/auth/realms/master/account Redirect 1",
path: "GET/auth/realms/master/account Redirect 1",
pathFormatted: "req_get-auth-realms-177d8",
stats: {
    "name": "GET/auth/realms/master/account Redirect 1",
    "numberOfRequests": {
        "total": "50",
        "ok": "0",
        "ko": "50"
    },
    "minResponseTime": {
        "total": "23",
        "ok": "-",
        "ko": "23"
    },
    "maxResponseTime": {
        "total": "1057",
        "ok": "-",
        "ko": "1057"
    },
    "meanResponseTime": {
        "total": "90",
        "ok": "-",
        "ko": "90"
    },
    "standardDeviation": {
        "total": "210",
        "ok": "-",
        "ko": "210"
    },
    "percentiles1": {
        "total": "34",
        "ok": "-",
        "ko": "34"
    },
    "percentiles2": {
        "total": "42",
        "ok": "-",
        "ko": "42"
    },
    "percentiles3": {
        "total": "401",
        "ok": "-",
        "ko": "401"
    },
    "percentiles4": {
        "total": "1056",
        "ok": "-",
        "ko": "1056"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 50,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.786",
        "ok": "-",
        "ko": "1.786"
    }
}
    },"req_get-json-applic-72f39": {
        type: "REQUEST",
        name: "GET/json/applicationUnits",
path: "GET/json/applicationUnits",
pathFormatted: "req_get-json-applic-72f39",
stats: {
    "name": "GET/json/applicationUnits",
    "numberOfRequests": {
        "total": "100",
        "ok": "100",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "257",
        "ok": "257",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "30",
        "ok": "30",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "24",
        "ok": "24",
        "ko": "-"
    },
    "percentiles1": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "percentiles2": {
        "total": "30",
        "ok": "30",
        "ko": "-"
    },
    "percentiles3": {
        "total": "42",
        "ok": "42",
        "ko": "-"
    },
    "percentiles4": {
        "total": "52",
        "ok": "52",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 100,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.571",
        "ok": "3.571",
        "ko": "-"
    }
}
    },"req_get-built-scrip-2d4a0": {
        type: "REQUEST",
        name: "GET/built/scripts/single-spa.min.js",
path: "GET/built/scripts/single-spa.min.js",
pathFormatted: "req_get-built-scrip-2d4a0",
stats: {
    "name": "GET/built/scripts/single-spa.min.js",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "564",
        "ok": "564",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "70",
        "ok": "70",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "122",
        "ok": "122",
        "ko": "-"
    },
    "percentiles1": {
        "total": "28",
        "ok": "28",
        "ko": "-"
    },
    "percentiles2": {
        "total": "42",
        "ok": "42",
        "ko": "-"
    },
    "percentiles3": {
        "total": "373",
        "ok": "373",
        "ko": "-"
    },
    "percentiles4": {
        "total": "554",
        "ok": "554",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.786",
        "ok": "1.786",
        "ko": "-"
    }
}
    },"req_get-json-userse-56296": {
        type: "REQUEST",
        name: "GET/json/UserSettings",
path: "GET/json/UserSettings",
pathFormatted: "req_get-json-userse-56296",
stats: {
    "name": "GET/json/UserSettings",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "275",
        "ok": "275",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "31",
        "ok": "31",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "35",
        "ok": "35",
        "ko": "-"
    },
    "percentiles1": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "percentiles2": {
        "total": "28",
        "ok": "28",
        "ko": "-"
    },
    "percentiles3": {
        "total": "41",
        "ok": "41",
        "ko": "-"
    },
    "percentiles4": {
        "total": "161",
        "ok": "161",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.786",
        "ok": "1.786",
        "ko": "-"
    }
}
    },"req_get-json-userco-f4ad0": {
        type: "REQUEST",
        name: "GET/json/UserCommonApplications",
path: "GET/json/UserCommonApplications",
pathFormatted: "req_get-json-userco-f4ad0",
stats: {
    "name": "GET/json/UserCommonApplications",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "297",
        "ok": "297",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "50",
        "ok": "50",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "68",
        "ok": "68",
        "ko": "-"
    },
    "percentiles1": {
        "total": "26",
        "ok": "26",
        "ko": "-"
    },
    "percentiles2": {
        "total": "35",
        "ok": "35",
        "ko": "-"
    },
    "percentiles3": {
        "total": "259",
        "ok": "259",
        "ko": "-"
    },
    "percentiles4": {
        "total": "290",
        "ok": "290",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.786",
        "ok": "1.786",
        "ko": "-"
    }
}
    },"req_get-hostrootshe-d0154": {
        type: "REQUEST",
        name: "GET/hostRootShell/assets/i18n/en_US.json",
path: "GET/hostRootShell/assets/i18n/en_US.json",
pathFormatted: "req_get-hostrootshe-d0154",
stats: {
    "name": "GET/hostRootShell/assets/i18n/en_US.json",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "417",
        "ok": "417",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "48",
        "ok": "48",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "80",
        "ok": "80",
        "ko": "-"
    },
    "percentiles1": {
        "total": "26",
        "ok": "26",
        "ko": "-"
    },
    "percentiles2": {
        "total": "35",
        "ok": "35",
        "ko": "-"
    },
    "percentiles3": {
        "total": "161",
        "ok": "161",
        "ko": "-"
    },
    "percentiles4": {
        "total": "408",
        "ok": "408",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.786",
        "ok": "1.786",
        "ko": "-"
    }
}
    },"req_get-hostmnemo-s-7aeb7": {
        type: "REQUEST",
        name: "GET/hostMnemo/scripts.js",
path: "GET/hostMnemo/scripts.js",
pathFormatted: "req_get-hostmnemo-s-7aeb7",
stats: {
    "name": "GET/hostMnemo/scripts.js",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "5322",
        "ok": "5322",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1246",
        "ok": "1246",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "1622",
        "ok": "1622",
        "ko": "-"
    },
    "percentiles1": {
        "total": "30",
        "ok": "30",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2685",
        "ok": "2685",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4167",
        "ok": "4167",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4881",
        "ok": "4881",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 30,
    "percentage": 60
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 20,
    "percentage": 40
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.786",
        "ok": "1.786",
        "ko": "-"
    }
}
    },"req_get-mnemo-css-c-7a130": {
        type: "REQUEST",
        name: "GET/mnemo/css/common.css",
path: "GET/mnemo/css/common.css",
pathFormatted: "req_get-mnemo-css-c-7a130",
stats: {
    "name": "GET/mnemo/css/common.css",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "650",
        "ok": "650",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "40",
        "ok": "40",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "87",
        "ok": "87",
        "ko": "-"
    },
    "percentiles1": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "percentiles2": {
        "total": "31",
        "ok": "31",
        "ko": "-"
    },
    "percentiles3": {
        "total": "42",
        "ok": "42",
        "ko": "-"
    },
    "percentiles4": {
        "total": "361",
        "ok": "361",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.786",
        "ok": "1.786",
        "ko": "-"
    }
}
    },"req_get-mnemo-resou-6daa7": {
        type: "REQUEST",
        name: "GET/mnemo/resources/graph.txt",
path: "GET/mnemo/resources/graph.txt",
pathFormatted: "req_get-mnemo-resou-6daa7",
stats: {
    "name": "GET/mnemo/resources/graph.txt",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "60",
        "ok": "60",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "28",
        "ok": "28",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "8",
        "ok": "8",
        "ko": "-"
    },
    "percentiles1": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "percentiles2": {
        "total": "28",
        "ok": "28",
        "ko": "-"
    },
    "percentiles3": {
        "total": "42",
        "ok": "42",
        "ko": "-"
    },
    "percentiles4": {
        "total": "53",
        "ok": "53",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.786",
        "ok": "1.786",
        "ko": "-"
    }
}
    },"req_get-mnemo-resou-95e3c": {
        type: "REQUEST",
        name: "GET/mnemo/resources/graph_ru.txt",
path: "GET/mnemo/resources/graph_ru.txt",
pathFormatted: "req_get-mnemo-resou-95e3c",
stats: {
    "name": "GET/mnemo/resources/graph_ru.txt",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "274",
        "ok": "274",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "36",
        "ok": "36",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "47",
        "ok": "47",
        "ko": "-"
    },
    "percentiles1": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "percentiles2": {
        "total": "27",
        "ok": "27",
        "ko": "-"
    },
    "percentiles3": {
        "total": "43",
        "ok": "43",
        "ko": "-"
    },
    "percentiles4": {
        "total": "267",
        "ok": "267",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.786",
        "ok": "1.786",
        "ko": "-"
    }
}
    },"req_get-mnemo-resou-d3607": {
        type: "REQUEST",
        name: "GET/mnemo/resources/editor.txt",
path: "GET/mnemo/resources/editor.txt",
pathFormatted: "req_get-mnemo-resou-d3607",
stats: {
    "name": "GET/mnemo/resources/editor.txt",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "259",
        "ok": "259",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "31",
        "ok": "31",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "33",
        "ok": "33",
        "ko": "-"
    },
    "percentiles1": {
        "total": "24",
        "ok": "24",
        "ko": "-"
    },
    "percentiles2": {
        "total": "28",
        "ok": "28",
        "ko": "-"
    },
    "percentiles3": {
        "total": "41",
        "ok": "41",
        "ko": "-"
    },
    "percentiles4": {
        "total": "153",
        "ok": "153",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.786",
        "ok": "1.786",
        "ko": "-"
    }
}
    },"req_get-mnemo-resou-03f4c": {
        type: "REQUEST",
        name: "GET/mnemo/resources/editor_ru.txt",
path: "GET/mnemo/resources/editor_ru.txt",
pathFormatted: "req_get-mnemo-resou-03f4c",
stats: {
    "name": "GET/mnemo/resources/editor_ru.txt",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "266",
        "ok": "266",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "41",
        "ok": "41",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "57",
        "ok": "57",
        "ko": "-"
    },
    "percentiles1": {
        "total": "24",
        "ok": "24",
        "ko": "-"
    },
    "percentiles2": {
        "total": "32",
        "ok": "32",
        "ko": "-"
    },
    "percentiles3": {
        "total": "164",
        "ok": "164",
        "ko": "-"
    },
    "percentiles4": {
        "total": "266",
        "ok": "266",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.786",
        "ok": "1.786",
        "ko": "-"
    }
}
    },"req_get-hostmnemo-m-22113": {
        type: "REQUEST",
        name: "GET/hostMnemo/main.js",
path: "GET/hostMnemo/main.js",
pathFormatted: "req_get-hostmnemo-m-22113",
stats: {
    "name": "GET/hostMnemo/main.js",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "15025",
        "ok": "15025",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4891",
        "ok": "4891",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "6063",
        "ok": "6063",
        "ko": "-"
    },
    "percentiles1": {
        "total": "37",
        "ok": "37",
        "ko": "-"
    },
    "percentiles2": {
        "total": "11296",
        "ok": "11296",
        "ko": "-"
    },
    "percentiles3": {
        "total": "14700",
        "ok": "14700",
        "ko": "-"
    },
    "percentiles4": {
        "total": "14986",
        "ok": "14986",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 30,
    "percentage": 60
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 20,
    "percentage": 40
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.786",
        "ok": "1.786",
        "ko": "-"
    }
}
    },"req_get-hostmnemo-a-913f4": {
        type: "REQUEST",
        name: "GET/hostMnemo/assets/i18n/en_US.json",
path: "GET/hostMnemo/assets/i18n/en_US.json",
pathFormatted: "req_get-hostmnemo-a-913f4",
stats: {
    "name": "GET/hostMnemo/assets/i18n/en_US.json",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "41",
        "ok": "41",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "5",
        "ok": "5",
        "ko": "-"
    },
    "percentiles1": {
        "total": "23",
        "ok": "23",
        "ko": "-"
    },
    "percentiles2": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "percentiles3": {
        "total": "39",
        "ok": "39",
        "ko": "-"
    },
    "percentiles4": {
        "total": "40",
        "ok": "40",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.786",
        "ok": "1.786",
        "ko": "-"
    }
}
    },"req_get-hostmnemo-a-19ffa": {
        type: "REQUEST",
        name: "GET/hostMnemo/assets/i18n/zyfra-mnemo/zyfraMnemoModule.en_US.json",
path: "GET/hostMnemo/assets/i18n/zyfra-mnemo/zyfraMnemoModule.en_US.json",
pathFormatted: "req_get-hostmnemo-a-19ffa",
stats: {
    "name": "GET/hostMnemo/assets/i18n/zyfra-mnemo/zyfraMnemoModule.en_US.json",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "806",
        "ok": "806",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "59",
        "ok": "59",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "132",
        "ok": "132",
        "ko": "-"
    },
    "percentiles1": {
        "total": "24",
        "ok": "24",
        "ko": "-"
    },
    "percentiles2": {
        "total": "34",
        "ok": "34",
        "ko": "-"
    },
    "percentiles3": {
        "total": "254",
        "ok": "254",
        "ko": "-"
    },
    "percentiles4": {
        "total": "620",
        "ok": "620",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 49,
    "percentage": 98
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 2
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.786",
        "ok": "1.786",
        "ko": "-"
    }
}
    },"req_get-hostmnemo-a-5dc3d": {
        type: "REQUEST",
        name: "GET/hostMnemo/assets/settings.json",
path: "GET/hostMnemo/assets/settings.json",
pathFormatted: "req_get-hostmnemo-a-5dc3d",
stats: {
    "name": "GET/hostMnemo/assets/settings.json",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "36",
        "ok": "36",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "4",
        "ok": "4",
        "ko": "-"
    },
    "percentiles1": {
        "total": "24",
        "ok": "24",
        "ko": "-"
    },
    "percentiles2": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "percentiles3": {
        "total": "33",
        "ok": "33",
        "ko": "-"
    },
    "percentiles4": {
        "total": "36",
        "ok": "36",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.786",
        "ok": "1.786",
        "ko": "-"
    }
}
    },"req_get-mnemo-resou-b8f0f": {
        type: "REQUEST",
        name: "GET/mnemo/resources/grapheditor.txt",
path: "GET/mnemo/resources/grapheditor.txt",
pathFormatted: "req_get-mnemo-resou-b8f0f",
stats: {
    "name": "GET/mnemo/resources/grapheditor.txt",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "438",
        "ok": "438",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "34",
        "ok": "34",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "58",
        "ok": "58",
        "ko": "-"
    },
    "percentiles1": {
        "total": "24",
        "ok": "24",
        "ko": "-"
    },
    "percentiles2": {
        "total": "27",
        "ok": "27",
        "ko": "-"
    },
    "percentiles3": {
        "total": "40",
        "ok": "40",
        "ko": "-"
    },
    "percentiles4": {
        "total": "247",
        "ok": "247",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.786",
        "ok": "1.786",
        "ko": "-"
    }
}
    },"req_get-mnemo-style-4f382": {
        type: "REQUEST",
        name: "GET/mnemo/styles/default.xml",
path: "GET/mnemo/styles/default.xml",
pathFormatted: "req_get-mnemo-style-4f382",
stats: {
    "name": "GET/mnemo/styles/default.xml",
    "numberOfRequests": {
        "total": "77",
        "ok": "77",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "274",
        "ok": "274",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "29",
        "ok": "29",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "29",
        "ok": "29",
        "ko": "-"
    },
    "percentiles1": {
        "total": "24",
        "ok": "24",
        "ko": "-"
    },
    "percentiles2": {
        "total": "26",
        "ok": "26",
        "ko": "-"
    },
    "percentiles3": {
        "total": "42",
        "ok": "42",
        "ko": "-"
    },
    "percentiles4": {
        "total": "117",
        "ok": "117",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 77,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.75",
        "ok": "2.75",
        "ko": "-"
    }
}
    },"req_get-mnemo-stenc-e38f6": {
        type: "REQUEST",
        name: "GET/mnemo/stencils/mnemo.library.xml",
path: "GET/mnemo/stencils/mnemo.library.xml",
pathFormatted: "req_get-mnemo-stenc-e38f6",
stats: {
    "name": "GET/mnemo/stencils/mnemo.library.xml",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1320",
        "ok": "1320",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "128",
        "ok": "128",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "222",
        "ok": "222",
        "ko": "-"
    },
    "percentiles1": {
        "total": "30",
        "ok": "30",
        "ko": "-"
    },
    "percentiles2": {
        "total": "129",
        "ok": "129",
        "ko": "-"
    },
    "percentiles3": {
        "total": "528",
        "ok": "528",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1008",
        "ok": "1008",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 49,
    "percentage": 98
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 1,
    "percentage": 2
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.786",
        "ok": "1.786",
        "ko": "-"
    }
}
    },"req_put-mnemobase-a-181b7": {
        type: "REQUEST",
        name: "PUT/mnemobase/api/v1/mnemo",
path: "PUT/mnemobase/api/v1/mnemo",
pathFormatted: "req_put-mnemobase-a-181b7",
stats: {
    "name": "PUT/mnemobase/api/v1/mnemo",
    "numberOfRequests": {
        "total": "13",
        "ok": "13",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "127",
        "ok": "127",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1179",
        "ok": "1179",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "239",
        "ok": "239",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "273",
        "ok": "273",
        "ko": "-"
    },
    "percentiles1": {
        "total": "148",
        "ok": "148",
        "ko": "-"
    },
    "percentiles2": {
        "total": "195",
        "ok": "195",
        "ko": "-"
    },
    "percentiles3": {
        "total": "602",
        "ok": "602",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1064",
        "ok": "1064",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 12,
    "percentage": 92
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 8
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.464",
        "ok": "0.464",
        "ko": "-"
    }
}
    },"req_get-mnemobase-a-ff9f5": {
        type: "REQUEST",
        name: "GET/mnemobase/api/v1/mnemo/",
path: "GET/mnemobase/api/v1/mnemo/",
pathFormatted: "req_get-mnemobase-a-ff9f5",
stats: {
    "name": "GET/mnemobase/api/v1/mnemo/",
    "numberOfRequests": {
        "total": "27",
        "ok": "27",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "112",
        "ok": "112",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "712",
        "ok": "712",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "221",
        "ok": "221",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "146",
        "ok": "146",
        "ko": "-"
    },
    "percentiles1": {
        "total": "162",
        "ok": "162",
        "ko": "-"
    },
    "percentiles2": {
        "total": "225",
        "ok": "225",
        "ko": "-"
    },
    "percentiles3": {
        "total": "583",
        "ok": "583",
        "ko": "-"
    },
    "percentiles4": {
        "total": "694",
        "ok": "694",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 27,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.964",
        "ok": "0.964",
        "ko": "-"
    }
}
    },"req_get-mnemo-resou-96eb1": {
        type: "REQUEST",
        name: "GET/mnemo/resources/grapheditor_ru.txt",
path: "GET/mnemo/resources/grapheditor_ru.txt",
pathFormatted: "req_get-mnemo-resou-96eb1",
stats: {
    "name": "GET/mnemo/resources/grapheditor_ru.txt",
    "numberOfRequests": {
        "total": "27",
        "ok": "27",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "48",
        "ok": "48",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "29",
        "ok": "29",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "7",
        "ok": "7",
        "ko": "-"
    },
    "percentiles1": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "percentiles2": {
        "total": "35",
        "ok": "35",
        "ko": "-"
    },
    "percentiles3": {
        "total": "40",
        "ok": "40",
        "ko": "-"
    },
    "percentiles4": {
        "total": "46",
        "ok": "46",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 27,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.964",
        "ok": "0.964",
        "ko": "-"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
