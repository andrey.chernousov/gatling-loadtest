var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "1831",
        "ok": "1781",
        "ko": "50"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "24"
    },
    "maxResponseTime": {
        "total": "14672",
        "ok": "14672",
        "ko": "141"
    },
    "meanResponseTime": {
        "total": "240",
        "ok": "246",
        "ko": "39"
    },
    "standardDeviation": {
        "total": "1286",
        "ok": "1304",
        "ko": "19"
    },
    "percentiles1": {
        "total": "25",
        "ok": "25",
        "ko": "35"
    },
    "percentiles2": {
        "total": "43",
        "ok": "43",
        "ko": "41"
    },
    "percentiles3": {
        "total": "563",
        "ok": "568",
        "ko": "68"
    },
    "percentiles4": {
        "total": "9688",
        "ok": "9724",
        "ko": "108"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1720,
    "percentage": 94
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 16,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 45,
    "percentage": 2
},
    "group4": {
    "name": "failed",
    "count": 50,
    "percentage": 3
},
    "meanNumberOfRequestsPerSecond": {
        "total": "65.393",
        "ok": "63.607",
        "ko": "1.786"
    }
},
contents: {
"req_login-99dea": {
        type: "REQUEST",
        name: "Login",
path: "Login",
pathFormatted: "req_login-99dea",
stats: {
    "name": "Login",
    "numberOfRequests": {
        "total": "60",
        "ok": "60",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "102",
        "ok": "102",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1328",
        "ok": "1328",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "227",
        "ok": "227",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "260",
        "ok": "260",
        "ko": "-"
    },
    "percentiles1": {
        "total": "134",
        "ok": "134",
        "ko": "-"
    },
    "percentiles2": {
        "total": "174",
        "ok": "174",
        "ko": "-"
    },
    "percentiles3": {
        "total": "849",
        "ok": "849",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1229",
        "ok": "1229",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 56,
    "percentage": 93
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 3,
    "percentage": 5
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 1,
    "percentage": 2
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.143",
        "ok": "2.143",
        "ko": "-"
    }
}
    },"req_get-mnemobase-a-1be13": {
        type: "REQUEST",
        name: "GET/mnemobase/api/v1/mnemo/info",
path: "GET/mnemobase/api/v1/mnemo/info",
pathFormatted: "req_get-mnemobase-a-1be13",
stats: {
    "name": "GET/mnemobase/api/v1/mnemo/info",
    "numberOfRequests": {
        "total": "120",
        "ok": "120",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "59",
        "ok": "59",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1519",
        "ok": "1519",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "363",
        "ok": "363",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "273",
        "ok": "273",
        "ko": "-"
    },
    "percentiles1": {
        "total": "298",
        "ok": "298",
        "ko": "-"
    },
    "percentiles2": {
        "total": "493",
        "ok": "493",
        "ko": "-"
    },
    "percentiles3": {
        "total": "863",
        "ok": "863",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1232",
        "ok": "1232",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 111,
    "percentage": 93
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 7,
    "percentage": 6
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 2,
    "percentage": 2
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "4.286",
        "ok": "4.286",
        "ko": "-"
    }
}
    },"req_post-mnemobase--159fa": {
        type: "REQUEST",
        name: "POST/mnemobase/api/v1/mnemo",
path: "POST/mnemobase/api/v1/mnemo",
pathFormatted: "req_post-mnemobase--159fa",
stats: {
    "name": "POST/mnemobase/api/v1/mnemo",
    "numberOfRequests": {
        "total": "1",
        "ok": "1",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "371",
        "ok": "371",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "371",
        "ok": "371",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "371",
        "ok": "371",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "0",
        "ok": "0",
        "ko": "-"
    },
    "percentiles1": {
        "total": "371",
        "ok": "371",
        "ko": "-"
    },
    "percentiles2": {
        "total": "371",
        "ok": "371",
        "ko": "-"
    },
    "percentiles3": {
        "total": "371",
        "ok": "371",
        "ko": "-"
    },
    "percentiles4": {
        "total": "371",
        "ok": "371",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.036",
        "ok": "0.036",
        "ko": "-"
    }
}
    },"req_get--e0e39": {
        type: "REQUEST",
        name: "GET/",
path: "GET/",
pathFormatted: "req_get--e0e39",
stats: {
    "name": "GET/",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "22",
        "ok": "22",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1090",
        "ok": "1090",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "68",
        "ok": "68",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "147",
        "ok": "147",
        "ko": "-"
    },
    "percentiles1": {
        "total": "47",
        "ok": "47",
        "ko": "-"
    },
    "percentiles2": {
        "total": "55",
        "ok": "55",
        "ko": "-"
    },
    "percentiles3": {
        "total": "72",
        "ok": "72",
        "ko": "-"
    },
    "percentiles4": {
        "total": "594",
        "ok": "594",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 49,
    "percentage": 98
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 2
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.786",
        "ok": "1.786",
        "ko": "-"
    }
}
    },"req_get-built-scrip-f7a2f": {
        type: "REQUEST",
        name: "GET/built/scripts/zone.min.js",
path: "GET/built/scripts/zone.min.js",
pathFormatted: "req_get-built-scrip-f7a2f",
stats: {
    "name": "GET/built/scripts/zone.min.js",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "776",
        "ok": "776",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "51",
        "ok": "51",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "105",
        "ok": "105",
        "ko": "-"
    },
    "percentiles1": {
        "total": "28",
        "ok": "28",
        "ko": "-"
    },
    "percentiles2": {
        "total": "46",
        "ok": "46",
        "ko": "-"
    },
    "percentiles3": {
        "total": "85",
        "ok": "85",
        "ko": "-"
    },
    "percentiles4": {
        "total": "452",
        "ok": "452",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.786",
        "ok": "1.786",
        "ko": "-"
    }
}
    },"req_get-built-scrip-6c713": {
        type: "REQUEST",
        name: "GET/built/scripts/import-map-overrides.js",
path: "GET/built/scripts/import-map-overrides.js",
pathFormatted: "req_get-built-scrip-6c713",
stats: {
    "name": "GET/built/scripts/import-map-overrides.js",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1281",
        "ok": "1281",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "70",
        "ok": "70",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "189",
        "ok": "189",
        "ko": "-"
    },
    "percentiles1": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "percentiles2": {
        "total": "28",
        "ok": "28",
        "ko": "-"
    },
    "percentiles3": {
        "total": "327",
        "ok": "327",
        "ko": "-"
    },
    "percentiles4": {
        "total": "845",
        "ok": "845",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 49,
    "percentage": 98
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 1,
    "percentage": 2
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.786",
        "ok": "1.786",
        "ko": "-"
    }
}
    },"req_get-built-scrip-3e0b8": {
        type: "REQUEST",
        name: "GET/built/scripts/system.min.js",
path: "GET/built/scripts/system.min.js",
pathFormatted: "req_get-built-scrip-3e0b8",
stats: {
    "name": "GET/built/scripts/system.min.js",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1116",
        "ok": "1116",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "65",
        "ok": "65",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "186",
        "ok": "186",
        "ko": "-"
    },
    "percentiles1": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "percentiles2": {
        "total": "31",
        "ok": "31",
        "ko": "-"
    },
    "percentiles3": {
        "total": "49",
        "ok": "49",
        "ko": "-"
    },
    "percentiles4": {
        "total": "967",
        "ok": "967",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 48,
    "percentage": 96
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 2,
    "percentage": 4
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.786",
        "ok": "1.786",
        "ko": "-"
    }
}
    },"req_get-built-scrip-a6908": {
        type: "REQUEST",
        name: "GET/built/scripts/amd.min.js",
path: "GET/built/scripts/amd.min.js",
pathFormatted: "req_get-built-scrip-a6908",
stats: {
    "name": "GET/built/scripts/amd.min.js",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "730",
        "ok": "730",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "56",
        "ok": "56",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "112",
        "ok": "112",
        "ko": "-"
    },
    "percentiles1": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "percentiles2": {
        "total": "31",
        "ok": "31",
        "ko": "-"
    },
    "percentiles3": {
        "total": "258",
        "ok": "258",
        "ko": "-"
    },
    "percentiles4": {
        "total": "515",
        "ok": "515",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.786",
        "ok": "1.786",
        "ko": "-"
    }
}
    },"req_get-built-scrip-a5f03": {
        type: "REQUEST",
        name: "GET/built/scripts/named-exports.js",
path: "GET/built/scripts/named-exports.js",
pathFormatted: "req_get-built-scrip-a5f03",
stats: {
    "name": "GET/built/scripts/named-exports.js",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "606",
        "ok": "606",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "50",
        "ok": "50",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "99",
        "ok": "99",
        "ko": "-"
    },
    "percentiles1": {
        "total": "24",
        "ok": "24",
        "ko": "-"
    },
    "percentiles2": {
        "total": "30",
        "ok": "30",
        "ko": "-"
    },
    "percentiles3": {
        "total": "179",
        "ok": "179",
        "ko": "-"
    },
    "percentiles4": {
        "total": "488",
        "ok": "488",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.786",
        "ok": "1.786",
        "ko": "-"
    }
}
    },"req_get-built-scrip-96ae1": {
        type: "REQUEST",
        name: "GET/built/scripts/named-register.min.js",
path: "GET/built/scripts/named-register.min.js",
pathFormatted: "req_get-built-scrip-96ae1",
stats: {
    "name": "GET/built/scripts/named-register.min.js",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "438",
        "ok": "438",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "42",
        "ok": "42",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "80",
        "ok": "80",
        "ko": "-"
    },
    "percentiles1": {
        "total": "24",
        "ok": "24",
        "ko": "-"
    },
    "percentiles2": {
        "total": "28",
        "ok": "28",
        "ko": "-"
    },
    "percentiles3": {
        "total": "42",
        "ok": "42",
        "ko": "-"
    },
    "percentiles4": {
        "total": "434",
        "ok": "434",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.786",
        "ok": "1.786",
        "ko": "-"
    }
}
    },"req_get-built-vendo-cdd2c": {
        type: "REQUEST",
        name: "GET/built/vendors~main.js",
path: "GET/built/vendors~main.js",
pathFormatted: "req_get-built-vendo-cdd2c",
stats: {
    "name": "GET/built/vendors~main.js",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "22",
        "ok": "22",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1874",
        "ok": "1874",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "288",
        "ok": "288",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "439",
        "ok": "439",
        "ko": "-"
    },
    "percentiles1": {
        "total": "40",
        "ok": "40",
        "ko": "-"
    },
    "percentiles2": {
        "total": "523",
        "ok": "523",
        "ko": "-"
    },
    "percentiles3": {
        "total": "1124",
        "ok": "1124",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1831",
        "ok": "1831",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 46,
    "percentage": 92
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 2
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 3,
    "percentage": 6
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.786",
        "ok": "1.786",
        "ko": "-"
    }
}
    },"req_get-built-main--37f57": {
        type: "REQUEST",
        name: "GET/built/main.js",
path: "GET/built/main.js",
pathFormatted: "req_get-built-main--37f57",
stats: {
    "name": "GET/built/main.js",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "512",
        "ok": "512",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "36",
        "ok": "36",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "68",
        "ok": "68",
        "ko": "-"
    },
    "percentiles1": {
        "total": "24",
        "ok": "24",
        "ko": "-"
    },
    "percentiles2": {
        "total": "26",
        "ok": "26",
        "ko": "-"
    },
    "percentiles3": {
        "total": "36",
        "ok": "36",
        "ko": "-"
    },
    "percentiles4": {
        "total": "297",
        "ok": "297",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.786",
        "ok": "1.786",
        "ko": "-"
    }
}
    },"req_get-built-asset-4566d": {
        type: "REQUEST",
        name: "GET/built/assets/settings.json",
path: "GET/built/assets/settings.json",
pathFormatted: "req_get-built-asset-4566d",
stats: {
    "name": "GET/built/assets/settings.json",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "308",
        "ok": "308",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "36",
        "ok": "36",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "50",
        "ok": "50",
        "ko": "-"
    },
    "percentiles1": {
        "total": "24",
        "ok": "24",
        "ko": "-"
    },
    "percentiles2": {
        "total": "28",
        "ok": "28",
        "ko": "-"
    },
    "percentiles3": {
        "total": "48",
        "ok": "48",
        "ko": "-"
    },
    "percentiles4": {
        "total": "281",
        "ok": "281",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.786",
        "ok": "1.786",
        "ko": "-"
    }
}
    },"req_get-auth-realms-24c57": {
        type: "REQUEST",
        name: "GET/auth/realms/master/account",
path: "GET/auth/realms/master/account",
pathFormatted: "req_get-auth-realms-24c57",
stats: {
    "name": "GET/auth/realms/master/account",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "22",
        "ok": "22",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "587",
        "ok": "587",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "72",
        "ok": "72",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "104",
        "ok": "104",
        "ko": "-"
    },
    "percentiles1": {
        "total": "36",
        "ok": "36",
        "ko": "-"
    },
    "percentiles2": {
        "total": "48",
        "ok": "48",
        "ko": "-"
    },
    "percentiles3": {
        "total": "283",
        "ok": "283",
        "ko": "-"
    },
    "percentiles4": {
        "total": "476",
        "ok": "476",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.786",
        "ok": "1.786",
        "ko": "-"
    }
}
    },"req_get-auth-realms-177d8": {
        type: "REQUEST",
        name: "GET/auth/realms/master/account Redirect 1",
path: "GET/auth/realms/master/account Redirect 1",
pathFormatted: "req_get-auth-realms-177d8",
stats: {
    "name": "GET/auth/realms/master/account Redirect 1",
    "numberOfRequests": {
        "total": "50",
        "ok": "0",
        "ko": "50"
    },
    "minResponseTime": {
        "total": "24",
        "ok": "-",
        "ko": "24"
    },
    "maxResponseTime": {
        "total": "141",
        "ok": "-",
        "ko": "141"
    },
    "meanResponseTime": {
        "total": "39",
        "ok": "-",
        "ko": "39"
    },
    "standardDeviation": {
        "total": "19",
        "ok": "-",
        "ko": "19"
    },
    "percentiles1": {
        "total": "35",
        "ok": "-",
        "ko": "35"
    },
    "percentiles2": {
        "total": "41",
        "ok": "-",
        "ko": "41"
    },
    "percentiles3": {
        "total": "68",
        "ok": "-",
        "ko": "68"
    },
    "percentiles4": {
        "total": "108",
        "ok": "-",
        "ko": "108"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 50,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.786",
        "ok": "-",
        "ko": "1.786"
    }
}
    },"req_get-json-applic-72f39": {
        type: "REQUEST",
        name: "GET/json/applicationUnits",
path: "GET/json/applicationUnits",
pathFormatted: "req_get-json-applic-72f39",
stats: {
    "name": "GET/json/applicationUnits",
    "numberOfRequests": {
        "total": "100",
        "ok": "100",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "315",
        "ok": "315",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "30",
        "ok": "30",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "37",
        "ok": "37",
        "ko": "-"
    },
    "percentiles1": {
        "total": "23",
        "ok": "23",
        "ko": "-"
    },
    "percentiles2": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "percentiles3": {
        "total": "37",
        "ok": "37",
        "ko": "-"
    },
    "percentiles4": {
        "total": "261",
        "ok": "261",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 100,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.571",
        "ok": "3.571",
        "ko": "-"
    }
}
    },"req_get-built-scrip-2d4a0": {
        type: "REQUEST",
        name: "GET/built/scripts/single-spa.min.js",
path: "GET/built/scripts/single-spa.min.js",
pathFormatted: "req_get-built-scrip-2d4a0",
stats: {
    "name": "GET/built/scripts/single-spa.min.js",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "264",
        "ok": "264",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "34",
        "ok": "34",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "34",
        "ok": "34",
        "ko": "-"
    },
    "percentiles1": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "percentiles2": {
        "total": "34",
        "ok": "34",
        "ko": "-"
    },
    "percentiles3": {
        "total": "48",
        "ok": "48",
        "ko": "-"
    },
    "percentiles4": {
        "total": "159",
        "ok": "159",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.786",
        "ok": "1.786",
        "ko": "-"
    }
}
    },"req_get-json-userse-56296": {
        type: "REQUEST",
        name: "GET/json/UserSettings",
path: "GET/json/UserSettings",
pathFormatted: "req_get-json-userse-56296",
stats: {
    "name": "GET/json/UserSettings",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "564",
        "ok": "564",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "41",
        "ok": "41",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "82",
        "ok": "82",
        "ko": "-"
    },
    "percentiles1": {
        "total": "23",
        "ok": "23",
        "ko": "-"
    },
    "percentiles2": {
        "total": "27",
        "ok": "27",
        "ko": "-"
    },
    "percentiles3": {
        "total": "43",
        "ok": "43",
        "ko": "-"
    },
    "percentiles4": {
        "total": "418",
        "ok": "418",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.786",
        "ok": "1.786",
        "ko": "-"
    }
}
    },"req_get-json-userco-f4ad0": {
        type: "REQUEST",
        name: "GET/json/UserCommonApplications",
path: "GET/json/UserCommonApplications",
pathFormatted: "req_get-json-userco-f4ad0",
stats: {
    "name": "GET/json/UserCommonApplications",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "46",
        "ok": "46",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "5",
        "ok": "5",
        "ko": "-"
    },
    "percentiles1": {
        "total": "23",
        "ok": "23",
        "ko": "-"
    },
    "percentiles2": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "percentiles3": {
        "total": "36",
        "ok": "36",
        "ko": "-"
    },
    "percentiles4": {
        "total": "44",
        "ok": "44",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.786",
        "ok": "1.786",
        "ko": "-"
    }
}
    },"req_get-hostrootshe-d0154": {
        type: "REQUEST",
        name: "GET/hostRootShell/assets/i18n/en_US.json",
path: "GET/hostRootShell/assets/i18n/en_US.json",
pathFormatted: "req_get-hostrootshe-d0154",
stats: {
    "name": "GET/hostRootShell/assets/i18n/en_US.json",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "22",
        "ok": "22",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "616",
        "ok": "616",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "45",
        "ok": "45",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "96",
        "ok": "96",
        "ko": "-"
    },
    "percentiles1": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "percentiles2": {
        "total": "26",
        "ok": "26",
        "ko": "-"
    },
    "percentiles3": {
        "total": "40",
        "ok": "40",
        "ko": "-"
    },
    "percentiles4": {
        "total": "507",
        "ok": "507",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.786",
        "ok": "1.786",
        "ko": "-"
    }
}
    },"req_get-hostmnemo-s-7aeb7": {
        type: "REQUEST",
        name: "GET/hostMnemo/scripts.js",
path: "GET/hostMnemo/scripts.js",
pathFormatted: "req_get-hostmnemo-s-7aeb7",
stats: {
    "name": "GET/hostMnemo/scripts.js",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4694",
        "ok": "4694",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1092",
        "ok": "1092",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "1451",
        "ok": "1451",
        "ko": "-"
    },
    "percentiles1": {
        "total": "31",
        "ok": "31",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2360",
        "ok": "2360",
        "ko": "-"
    },
    "percentiles3": {
        "total": "4156",
        "ok": "4156",
        "ko": "-"
    },
    "percentiles4": {
        "total": "4509",
        "ok": "4509",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 30,
    "percentage": 60
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 2,
    "percentage": 4
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 18,
    "percentage": 36
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.786",
        "ok": "1.786",
        "ko": "-"
    }
}
    },"req_get-mnemo-css-c-7a130": {
        type: "REQUEST",
        name: "GET/mnemo/css/common.css",
path: "GET/mnemo/css/common.css",
pathFormatted: "req_get-mnemo-css-c-7a130",
stats: {
    "name": "GET/mnemo/css/common.css",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "256",
        "ok": "256",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "35",
        "ok": "35",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "45",
        "ok": "45",
        "ko": "-"
    },
    "percentiles1": {
        "total": "24",
        "ok": "24",
        "ko": "-"
    },
    "percentiles2": {
        "total": "27",
        "ok": "27",
        "ko": "-"
    },
    "percentiles3": {
        "total": "45",
        "ok": "45",
        "ko": "-"
    },
    "percentiles4": {
        "total": "255",
        "ok": "255",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.786",
        "ok": "1.786",
        "ko": "-"
    }
}
    },"req_get-mnemo-resou-6daa7": {
        type: "REQUEST",
        name: "GET/mnemo/resources/graph.txt",
path: "GET/mnemo/resources/graph.txt",
pathFormatted: "req_get-mnemo-resou-6daa7",
stats: {
    "name": "GET/mnemo/resources/graph.txt",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "271",
        "ok": "271",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "36",
        "ok": "36",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "47",
        "ok": "47",
        "ko": "-"
    },
    "percentiles1": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "percentiles2": {
        "total": "26",
        "ok": "26",
        "ko": "-"
    },
    "percentiles3": {
        "total": "47",
        "ok": "47",
        "ko": "-"
    },
    "percentiles4": {
        "total": "264",
        "ok": "264",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.786",
        "ok": "1.786",
        "ko": "-"
    }
}
    },"req_get-mnemo-resou-95e3c": {
        type: "REQUEST",
        name: "GET/mnemo/resources/graph_ru.txt",
path: "GET/mnemo/resources/graph_ru.txt",
pathFormatted: "req_get-mnemo-resou-95e3c",
stats: {
    "name": "GET/mnemo/resources/graph_ru.txt",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "255",
        "ok": "255",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "31",
        "ok": "31",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "33",
        "ok": "33",
        "ko": "-"
    },
    "percentiles1": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "percentiles2": {
        "total": "26",
        "ok": "26",
        "ko": "-"
    },
    "percentiles3": {
        "total": "41",
        "ok": "41",
        "ko": "-"
    },
    "percentiles4": {
        "total": "163",
        "ok": "163",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.786",
        "ok": "1.786",
        "ko": "-"
    }
}
    },"req_get-mnemo-resou-d3607": {
        type: "REQUEST",
        name: "GET/mnemo/resources/editor.txt",
path: "GET/mnemo/resources/editor.txt",
pathFormatted: "req_get-mnemo-resou-d3607",
stats: {
    "name": "GET/mnemo/resources/editor.txt",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "42",
        "ok": "42",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "4",
        "ok": "4",
        "ko": "-"
    },
    "percentiles1": {
        "total": "24",
        "ok": "24",
        "ko": "-"
    },
    "percentiles2": {
        "total": "26",
        "ok": "26",
        "ko": "-"
    },
    "percentiles3": {
        "total": "33",
        "ok": "33",
        "ko": "-"
    },
    "percentiles4": {
        "total": "39",
        "ok": "39",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.786",
        "ok": "1.786",
        "ko": "-"
    }
}
    },"req_get-mnemo-resou-03f4c": {
        type: "REQUEST",
        name: "GET/mnemo/resources/editor_ru.txt",
path: "GET/mnemo/resources/editor_ru.txt",
pathFormatted: "req_get-mnemo-resou-03f4c",
stats: {
    "name": "GET/mnemo/resources/editor_ru.txt",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "270",
        "ok": "270",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "35",
        "ok": "35",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "47",
        "ok": "47",
        "ko": "-"
    },
    "percentiles1": {
        "total": "24",
        "ok": "24",
        "ko": "-"
    },
    "percentiles2": {
        "total": "26",
        "ok": "26",
        "ko": "-"
    },
    "percentiles3": {
        "total": "38",
        "ok": "38",
        "ko": "-"
    },
    "percentiles4": {
        "total": "266",
        "ok": "266",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.786",
        "ok": "1.786",
        "ko": "-"
    }
}
    },"req_get-hostmnemo-m-22113": {
        type: "REQUEST",
        name: "GET/hostMnemo/main.js",
path: "GET/hostMnemo/main.js",
pathFormatted: "req_get-hostmnemo-m-22113",
stats: {
    "name": "GET/hostMnemo/main.js",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "14672",
        "ok": "14672",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "4799",
        "ok": "4799",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "5926",
        "ok": "5926",
        "ko": "-"
    },
    "percentiles1": {
        "total": "27",
        "ok": "27",
        "ko": "-"
    },
    "percentiles2": {
        "total": "11300",
        "ok": "11300",
        "ko": "-"
    },
    "percentiles3": {
        "total": "13926",
        "ok": "13926",
        "ko": "-"
    },
    "percentiles4": {
        "total": "14510",
        "ok": "14510",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 30,
    "percentage": 60
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 20,
    "percentage": 40
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.786",
        "ok": "1.786",
        "ko": "-"
    }
}
    },"req_get-hostmnemo-a-913f4": {
        type: "REQUEST",
        name: "GET/hostMnemo/assets/i18n/en_US.json",
path: "GET/hostMnemo/assets/i18n/en_US.json",
pathFormatted: "req_get-hostmnemo-a-913f4",
stats: {
    "name": "GET/hostMnemo/assets/i18n/en_US.json",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "45",
        "ok": "45",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "5",
        "ok": "5",
        "ko": "-"
    },
    "percentiles1": {
        "total": "23",
        "ok": "23",
        "ko": "-"
    },
    "percentiles2": {
        "total": "26",
        "ok": "26",
        "ko": "-"
    },
    "percentiles3": {
        "total": "37",
        "ok": "37",
        "ko": "-"
    },
    "percentiles4": {
        "total": "42",
        "ok": "42",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.786",
        "ok": "1.786",
        "ko": "-"
    }
}
    },"req_get-hostmnemo-a-19ffa": {
        type: "REQUEST",
        name: "GET/hostMnemo/assets/i18n/zyfra-mnemo/zyfraMnemoModule.en_US.json",
path: "GET/hostMnemo/assets/i18n/zyfra-mnemo/zyfraMnemoModule.en_US.json",
pathFormatted: "req_get-hostmnemo-a-19ffa",
stats: {
    "name": "GET/hostMnemo/assets/i18n/zyfra-mnemo/zyfraMnemoModule.en_US.json",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "45",
        "ok": "45",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "27",
        "ok": "27",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "6",
        "ok": "6",
        "ko": "-"
    },
    "percentiles1": {
        "total": "24",
        "ok": "24",
        "ko": "-"
    },
    "percentiles2": {
        "total": "29",
        "ok": "29",
        "ko": "-"
    },
    "percentiles3": {
        "total": "38",
        "ok": "38",
        "ko": "-"
    },
    "percentiles4": {
        "total": "44",
        "ok": "44",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.786",
        "ok": "1.786",
        "ko": "-"
    }
}
    },"req_get-hostmnemo-a-5dc3d": {
        type: "REQUEST",
        name: "GET/hostMnemo/assets/settings.json",
path: "GET/hostMnemo/assets/settings.json",
pathFormatted: "req_get-hostmnemo-a-5dc3d",
stats: {
    "name": "GET/hostMnemo/assets/settings.json",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "570",
        "ok": "570",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "36",
        "ok": "36",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "76",
        "ok": "76",
        "ko": "-"
    },
    "percentiles1": {
        "total": "23",
        "ok": "23",
        "ko": "-"
    },
    "percentiles2": {
        "total": "27",
        "ok": "27",
        "ko": "-"
    },
    "percentiles3": {
        "total": "35",
        "ok": "35",
        "ko": "-"
    },
    "percentiles4": {
        "total": "309",
        "ok": "309",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.786",
        "ok": "1.786",
        "ko": "-"
    }
}
    },"req_get-mnemo-resou-b8f0f": {
        type: "REQUEST",
        name: "GET/mnemo/resources/grapheditor.txt",
path: "GET/mnemo/resources/grapheditor.txt",
pathFormatted: "req_get-mnemo-resou-b8f0f",
stats: {
    "name": "GET/mnemo/resources/grapheditor.txt",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "463",
        "ok": "463",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "64",
        "ok": "64",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "118",
        "ok": "118",
        "ko": "-"
    },
    "percentiles1": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "percentiles2": {
        "total": "29",
        "ok": "29",
        "ko": "-"
    },
    "percentiles3": {
        "total": "432",
        "ok": "432",
        "ko": "-"
    },
    "percentiles4": {
        "total": "461",
        "ok": "461",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.786",
        "ok": "1.786",
        "ko": "-"
    }
}
    },"req_get-mnemo-style-4f382": {
        type: "REQUEST",
        name: "GET/mnemo/styles/default.xml",
path: "GET/mnemo/styles/default.xml",
pathFormatted: "req_get-mnemo-style-4f382",
stats: {
    "name": "GET/mnemo/styles/default.xml",
    "numberOfRequests": {
        "total": "80",
        "ok": "80",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "59",
        "ok": "59",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "27",
        "ok": "27",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "7",
        "ok": "7",
        "ko": "-"
    },
    "percentiles1": {
        "total": "24",
        "ok": "24",
        "ko": "-"
    },
    "percentiles2": {
        "total": "28",
        "ok": "28",
        "ko": "-"
    },
    "percentiles3": {
        "total": "41",
        "ok": "41",
        "ko": "-"
    },
    "percentiles4": {
        "total": "49",
        "ok": "49",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 80,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.857",
        "ok": "2.857",
        "ko": "-"
    }
}
    },"req_get-mnemo-stenc-e38f6": {
        type: "REQUEST",
        name: "GET/mnemo/stencils/mnemo.library.xml",
path: "GET/mnemo/stencils/mnemo.library.xml",
pathFormatted: "req_get-mnemo-stenc-e38f6",
stats: {
    "name": "GET/mnemo/stencils/mnemo.library.xml",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "685",
        "ok": "685",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "91",
        "ok": "91",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "120",
        "ok": "120",
        "ko": "-"
    },
    "percentiles1": {
        "total": "33",
        "ok": "33",
        "ko": "-"
    },
    "percentiles2": {
        "total": "135",
        "ok": "135",
        "ko": "-"
    },
    "percentiles3": {
        "total": "209",
        "ok": "209",
        "ko": "-"
    },
    "percentiles4": {
        "total": "599",
        "ok": "599",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.786",
        "ok": "1.786",
        "ko": "-"
    }
}
    },"req_get-mnemobase-a-ff9f5": {
        type: "REQUEST",
        name: "GET/mnemobase/api/v1/mnemo/",
path: "GET/mnemobase/api/v1/mnemo/",
pathFormatted: "req_get-mnemobase-a-ff9f5",
stats: {
    "name": "GET/mnemobase/api/v1/mnemo/",
    "numberOfRequests": {
        "total": "30",
        "ok": "30",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "83",
        "ok": "83",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "631",
        "ok": "631",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "270",
        "ok": "270",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "165",
        "ok": "165",
        "ko": "-"
    },
    "percentiles1": {
        "total": "216",
        "ok": "216",
        "ko": "-"
    },
    "percentiles2": {
        "total": "422",
        "ok": "422",
        "ko": "-"
    },
    "percentiles3": {
        "total": "587",
        "ok": "587",
        "ko": "-"
    },
    "percentiles4": {
        "total": "625",
        "ok": "625",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 30,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.071",
        "ok": "1.071",
        "ko": "-"
    }
}
    },"req_put-mnemobase-a-181b7": {
        type: "REQUEST",
        name: "PUT/mnemobase/api/v1/mnemo",
path: "PUT/mnemobase/api/v1/mnemo",
pathFormatted: "req_put-mnemobase-a-181b7",
stats: {
    "name": "PUT/mnemobase/api/v1/mnemo",
    "numberOfRequests": {
        "total": "10",
        "ok": "10",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "112",
        "ok": "112",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "459",
        "ok": "459",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "279",
        "ok": "279",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "135",
        "ok": "135",
        "ko": "-"
    },
    "percentiles1": {
        "total": "268",
        "ok": "268",
        "ko": "-"
    },
    "percentiles2": {
        "total": "422",
        "ok": "422",
        "ko": "-"
    },
    "percentiles3": {
        "total": "455",
        "ok": "455",
        "ko": "-"
    },
    "percentiles4": {
        "total": "458",
        "ok": "458",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 10,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.357",
        "ok": "0.357",
        "ko": "-"
    }
}
    },"req_get-mnemo-resou-96eb1": {
        type: "REQUEST",
        name: "GET/mnemo/resources/grapheditor_ru.txt",
path: "GET/mnemo/resources/grapheditor_ru.txt",
pathFormatted: "req_get-mnemo-resou-96eb1",
stats: {
    "name": "GET/mnemo/resources/grapheditor_ru.txt",
    "numberOfRequests": {
        "total": "30",
        "ok": "30",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "450",
        "ok": "450",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "42",
        "ok": "42",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "76",
        "ok": "76",
        "ko": "-"
    },
    "percentiles1": {
        "total": "24",
        "ok": "24",
        "ko": "-"
    },
    "percentiles2": {
        "total": "30",
        "ok": "30",
        "ko": "-"
    },
    "percentiles3": {
        "total": "50",
        "ok": "50",
        "ko": "-"
    },
    "percentiles4": {
        "total": "335",
        "ok": "335",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 30,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.071",
        "ok": "1.071",
        "ko": "-"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
