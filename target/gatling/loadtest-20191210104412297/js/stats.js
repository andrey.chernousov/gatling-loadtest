var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "35866",
        "ok": "34856",
        "ko": "1010"
    },
    "minResponseTime": {
        "total": "20",
        "ok": "20",
        "ko": "22"
    },
    "maxResponseTime": {
        "total": "17336",
        "ok": "17336",
        "ko": "1061"
    },
    "meanResponseTime": {
        "total": "56",
        "ok": "56",
        "ko": "43"
    },
    "standardDeviation": {
        "total": "352",
        "ok": "357",
        "ko": "72"
    },
    "percentiles1": {
        "total": "24",
        "ok": "23",
        "ko": "29"
    },
    "percentiles2": {
        "total": "29",
        "ok": "29",
        "ko": "36"
    },
    "percentiles3": {
        "total": "140",
        "ok": "141",
        "ko": "69"
    },
    "percentiles4": {
        "total": "443",
        "ok": "448",
        "ko": "394"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 34689,
    "percentage": 97
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 68,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 99,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1010,
    "percentage": 3
},
    "meanNumberOfRequestsPerSecond": {
        "total": "286.928",
        "ok": "278.848",
        "ko": "8.08"
    }
},
contents: {
"req_login-99dea": {
        type: "REQUEST",
        name: "Login",
path: "Login",
pathFormatted: "req_login-99dea",
stats: {
    "name": "Login",
    "numberOfRequests": {
        "total": "1020",
        "ok": "1020",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "98",
        "ok": "98",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1454",
        "ok": "1454",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "147",
        "ok": "147",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "126",
        "ok": "126",
        "ko": "-"
    },
    "percentiles1": {
        "total": "121",
        "ok": "121",
        "ko": "-"
    },
    "percentiles2": {
        "total": "138",
        "ok": "138",
        "ko": "-"
    },
    "percentiles3": {
        "total": "208",
        "ok": "208",
        "ko": "-"
    },
    "percentiles4": {
        "total": "871",
        "ok": "871",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1008,
    "percentage": 99
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 8,
    "percentage": 1
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 4,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "8.16",
        "ok": "8.16",
        "ko": "-"
    }
}
    },"req_-mnemobase-api--65e0e": {
        type: "REQUEST",
        name: "/mnemobase/api/v1/mnemo/info",
path: "/mnemobase/api/v1/mnemo/info",
pathFormatted: "req_-mnemobase-api--65e0e",
stats: {
    "name": "/mnemobase/api/v1/mnemo/info",
    "numberOfRequests": {
        "total": "1072",
        "ok": "1072",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "50",
        "ok": "50",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2608",
        "ok": "2608",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "199",
        "ok": "199",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "290",
        "ok": "290",
        "ko": "-"
    },
    "percentiles1": {
        "total": "103",
        "ok": "103",
        "ko": "-"
    },
    "percentiles2": {
        "total": "167",
        "ok": "167",
        "ko": "-"
    },
    "percentiles3": {
        "total": "714",
        "ok": "714",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1774",
        "ok": "1774",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1025,
    "percentage": 96
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 27,
    "percentage": 3
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 20,
    "percentage": 2
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "8.576",
        "ok": "8.576",
        "ko": "-"
    }
}
    },"req_--6666c": {
        type: "REQUEST",
        name: "/",
path: "/",
pathFormatted: "req_--6666c",
stats: {
    "name": "/",
    "numberOfRequests": {
        "total": "1010",
        "ok": "1010",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "22",
        "ok": "22",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "3259",
        "ok": "3259",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "102",
        "ok": "102",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "379",
        "ok": "379",
        "ko": "-"
    },
    "percentiles1": {
        "total": "46",
        "ok": "46",
        "ko": "-"
    },
    "percentiles2": {
        "total": "51",
        "ok": "51",
        "ko": "-"
    },
    "percentiles3": {
        "total": "74",
        "ok": "74",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3049",
        "ok": "3049",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 994,
    "percentage": 98
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 16,
    "percentage": 2
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "8.08",
        "ok": "8.08",
        "ko": "-"
    }
}
    },"req_-built-scripts--beed0": {
        type: "REQUEST",
        name: "/built/scripts/zone.min.js",
path: "/built/scripts/zone.min.js",
pathFormatted: "req_-built-scripts--beed0",
stats: {
    "name": "/built/scripts/zone.min.js",
    "numberOfRequests": {
        "total": "1010",
        "ok": "1010",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "679",
        "ok": "679",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "31",
        "ok": "31",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "49",
        "ok": "49",
        "ko": "-"
    },
    "percentiles1": {
        "total": "23",
        "ok": "23",
        "ko": "-"
    },
    "percentiles2": {
        "total": "26",
        "ok": "26",
        "ko": "-"
    },
    "percentiles3": {
        "total": "40",
        "ok": "40",
        "ko": "-"
    },
    "percentiles4": {
        "total": "271",
        "ok": "271",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1010,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "8.08",
        "ok": "8.08",
        "ko": "-"
    }
}
    },"req_-built-scripts--0a490": {
        type: "REQUEST",
        name: "/built/scripts/import-map-overrides.js",
path: "/built/scripts/import-map-overrides.js",
pathFormatted: "req_-built-scripts--0a490",
stats: {
    "name": "/built/scripts/import-map-overrides.js",
    "numberOfRequests": {
        "total": "1010",
        "ok": "1010",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "390",
        "ok": "390",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "29",
        "ok": "29",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "31",
        "ok": "31",
        "ko": "-"
    },
    "percentiles1": {
        "total": "23",
        "ok": "23",
        "ko": "-"
    },
    "percentiles2": {
        "total": "26",
        "ok": "26",
        "ko": "-"
    },
    "percentiles3": {
        "total": "37",
        "ok": "37",
        "ko": "-"
    },
    "percentiles4": {
        "total": "256",
        "ok": "256",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1010,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "8.08",
        "ok": "8.08",
        "ko": "-"
    }
}
    },"req_-built-scripts--da42e": {
        type: "REQUEST",
        name: "/built/scripts/system.min.js",
path: "/built/scripts/system.min.js",
pathFormatted: "req_-built-scripts--da42e",
stats: {
    "name": "/built/scripts/system.min.js",
    "numberOfRequests": {
        "total": "1010",
        "ok": "1010",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "635",
        "ok": "635",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "29",
        "ok": "29",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "39",
        "ok": "39",
        "ko": "-"
    },
    "percentiles1": {
        "total": "23",
        "ok": "23",
        "ko": "-"
    },
    "percentiles2": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "percentiles3": {
        "total": "38",
        "ok": "38",
        "ko": "-"
    },
    "percentiles4": {
        "total": "263",
        "ok": "263",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1010,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "8.08",
        "ok": "8.08",
        "ko": "-"
    }
}
    },"req_-built-scripts--89aea": {
        type: "REQUEST",
        name: "/built/scripts/amd.min.js",
path: "/built/scripts/amd.min.js",
pathFormatted: "req_-built-scripts--89aea",
stats: {
    "name": "/built/scripts/amd.min.js",
    "numberOfRequests": {
        "total": "1010",
        "ok": "1010",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "581",
        "ok": "581",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "29",
        "ok": "29",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "38",
        "ok": "38",
        "ko": "-"
    },
    "percentiles1": {
        "total": "23",
        "ok": "23",
        "ko": "-"
    },
    "percentiles2": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "percentiles3": {
        "total": "38",
        "ok": "38",
        "ko": "-"
    },
    "percentiles4": {
        "total": "260",
        "ok": "260",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1010,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "8.08",
        "ok": "8.08",
        "ko": "-"
    }
}
    },"req_-built-scripts--dbe46": {
        type: "REQUEST",
        name: "/built/scripts/named-exports.js",
path: "/built/scripts/named-exports.js",
pathFormatted: "req_-built-scripts--dbe46",
stats: {
    "name": "/built/scripts/named-exports.js",
    "numberOfRequests": {
        "total": "1010",
        "ok": "1010",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "589",
        "ok": "589",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "29",
        "ok": "29",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "39",
        "ok": "39",
        "ko": "-"
    },
    "percentiles1": {
        "total": "23",
        "ok": "23",
        "ko": "-"
    },
    "percentiles2": {
        "total": "26",
        "ok": "26",
        "ko": "-"
    },
    "percentiles3": {
        "total": "38",
        "ok": "38",
        "ko": "-"
    },
    "percentiles4": {
        "total": "266",
        "ok": "266",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1010,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "8.08",
        "ok": "8.08",
        "ko": "-"
    }
}
    },"req_-built-scripts--e6e7f": {
        type: "REQUEST",
        name: "/built/scripts/named-register.min.js",
path: "/built/scripts/named-register.min.js",
pathFormatted: "req_-built-scripts--e6e7f",
stats: {
    "name": "/built/scripts/named-register.min.js",
    "numberOfRequests": {
        "total": "1010",
        "ok": "1010",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1164",
        "ok": "1164",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "30",
        "ok": "30",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "48",
        "ok": "48",
        "ko": "-"
    },
    "percentiles1": {
        "total": "23",
        "ok": "23",
        "ko": "-"
    },
    "percentiles2": {
        "total": "26",
        "ok": "26",
        "ko": "-"
    },
    "percentiles3": {
        "total": "39",
        "ok": "39",
        "ko": "-"
    },
    "percentiles4": {
        "total": "258",
        "ok": "258",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1009,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "8.08",
        "ok": "8.08",
        "ko": "-"
    }
}
    },"req_-built-vendors--57cfa": {
        type: "REQUEST",
        name: "/built/vendors~main.js",
path: "/built/vendors~main.js",
pathFormatted: "req_-built-vendors--57cfa",
stats: {
    "name": "/built/vendors~main.js",
    "numberOfRequests": {
        "total": "1010",
        "ok": "1010",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1564",
        "ok": "1564",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "40",
        "ok": "40",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "100",
        "ok": "100",
        "ko": "-"
    },
    "percentiles1": {
        "total": "23",
        "ok": "23",
        "ko": "-"
    },
    "percentiles2": {
        "total": "26",
        "ok": "26",
        "ko": "-"
    },
    "percentiles3": {
        "total": "43",
        "ok": "43",
        "ko": "-"
    },
    "percentiles4": {
        "total": "586",
        "ok": "586",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1007,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 2,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "8.08",
        "ok": "8.08",
        "ko": "-"
    }
}
    },"req_-built-main-js-746c8": {
        type: "REQUEST",
        name: "/built/main.js",
path: "/built/main.js",
pathFormatted: "req_-built-main-js-746c8",
stats: {
    "name": "/built/main.js",
    "numberOfRequests": {
        "total": "1010",
        "ok": "1010",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1157",
        "ok": "1157",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "31",
        "ok": "31",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "52",
        "ok": "52",
        "ko": "-"
    },
    "percentiles1": {
        "total": "23",
        "ok": "23",
        "ko": "-"
    },
    "percentiles2": {
        "total": "26",
        "ok": "26",
        "ko": "-"
    },
    "percentiles3": {
        "total": "39",
        "ok": "39",
        "ko": "-"
    },
    "percentiles4": {
        "total": "259",
        "ok": "259",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1009,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "8.08",
        "ok": "8.08",
        "ko": "-"
    }
}
    },"req_-built-assets-s-b3836": {
        type: "REQUEST",
        name: "/built/assets/settings.json",
path: "/built/assets/settings.json",
pathFormatted: "req_-built-assets-s-b3836",
stats: {
    "name": "/built/assets/settings.json",
    "numberOfRequests": {
        "total": "1010",
        "ok": "1010",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "571",
        "ok": "571",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "29",
        "ok": "29",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "39",
        "ok": "39",
        "ko": "-"
    },
    "percentiles1": {
        "total": "23",
        "ok": "23",
        "ko": "-"
    },
    "percentiles2": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "percentiles3": {
        "total": "37",
        "ok": "37",
        "ko": "-"
    },
    "percentiles4": {
        "total": "266",
        "ok": "266",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1010,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "8.08",
        "ok": "8.08",
        "ko": "-"
    }
}
    },"req_-auth-realms-ma-5a21e": {
        type: "REQUEST",
        name: "/auth/realms/master/account",
path: "/auth/realms/master/account",
pathFormatted: "req_-auth-realms-ma-5a21e",
stats: {
    "name": "/auth/realms/master/account",
    "numberOfRequests": {
        "total": "1010",
        "ok": "1010",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "22",
        "ok": "22",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1200",
        "ok": "1200",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "38",
        "ok": "38",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "51",
        "ok": "51",
        "ko": "-"
    },
    "percentiles1": {
        "total": "28",
        "ok": "28",
        "ko": "-"
    },
    "percentiles2": {
        "total": "37",
        "ok": "37",
        "ko": "-"
    },
    "percentiles3": {
        "total": "65",
        "ok": "65",
        "ko": "-"
    },
    "percentiles4": {
        "total": "271",
        "ok": "271",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1009,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "8.08",
        "ok": "8.08",
        "ko": "-"
    }
}
    },"req_-auth-realms-ma-0613f": {
        type: "REQUEST",
        name: "/auth/realms/master/account Redirect 1",
path: "/auth/realms/master/account Redirect 1",
pathFormatted: "req_-auth-realms-ma-0613f",
stats: {
    "name": "/auth/realms/master/account Redirect 1",
    "numberOfRequests": {
        "total": "1010",
        "ok": "0",
        "ko": "1010"
    },
    "minResponseTime": {
        "total": "22",
        "ok": "-",
        "ko": "22"
    },
    "maxResponseTime": {
        "total": "1061",
        "ok": "-",
        "ko": "1061"
    },
    "meanResponseTime": {
        "total": "43",
        "ok": "-",
        "ko": "43"
    },
    "standardDeviation": {
        "total": "72",
        "ok": "-",
        "ko": "72"
    },
    "percentiles1": {
        "total": "29",
        "ok": "-",
        "ko": "29"
    },
    "percentiles2": {
        "total": "36",
        "ok": "-",
        "ko": "36"
    },
    "percentiles3": {
        "total": "69",
        "ok": "-",
        "ko": "69"
    },
    "percentiles4": {
        "total": "394",
        "ok": "-",
        "ko": "394"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 0,
    "percentage": 0
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 1010,
    "percentage": 100
},
    "meanNumberOfRequestsPerSecond": {
        "total": "8.08",
        "ok": "-",
        "ko": "8.08"
    }
}
    },"req_-json-applicati-ca958": {
        type: "REQUEST",
        name: "/json/applicationUnits",
path: "/json/applicationUnits",
pathFormatted: "req_-json-applicati-ca958",
stats: {
    "name": "/json/applicationUnits",
    "numberOfRequests": {
        "total": "2020",
        "ok": "2020",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2393",
        "ok": "2393",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "29",
        "ok": "29",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "61",
        "ok": "61",
        "ko": "-"
    },
    "percentiles1": {
        "total": "23",
        "ok": "23",
        "ko": "-"
    },
    "percentiles2": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "percentiles3": {
        "total": "37",
        "ok": "37",
        "ko": "-"
    },
    "percentiles4": {
        "total": "257",
        "ok": "257",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 2019,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 1,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "16.16",
        "ok": "16.16",
        "ko": "-"
    }
}
    },"req_-built-scripts--a12a0": {
        type: "REQUEST",
        name: "/built/scripts/single-spa.min.js",
path: "/built/scripts/single-spa.min.js",
pathFormatted: "req_-built-scripts--a12a0",
stats: {
    "name": "/built/scripts/single-spa.min.js",
    "numberOfRequests": {
        "total": "1010",
        "ok": "1010",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "670",
        "ok": "670",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "30",
        "ok": "30",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "40",
        "ok": "40",
        "ko": "-"
    },
    "percentiles1": {
        "total": "23",
        "ok": "23",
        "ko": "-"
    },
    "percentiles2": {
        "total": "26",
        "ok": "26",
        "ko": "-"
    },
    "percentiles3": {
        "total": "40",
        "ok": "40",
        "ko": "-"
    },
    "percentiles4": {
        "total": "265",
        "ok": "265",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1010,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "8.08",
        "ok": "8.08",
        "ko": "-"
    }
}
    },"req_-json-usersetti-341fa": {
        type: "REQUEST",
        name: "/json/UserSettings",
path: "/json/UserSettings",
pathFormatted: "req_-json-usersetti-341fa",
stats: {
    "name": "/json/UserSettings",
    "numberOfRequests": {
        "total": "1010",
        "ok": "1010",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "20",
        "ok": "20",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "605",
        "ok": "605",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "29",
        "ok": "29",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "36",
        "ok": "36",
        "ko": "-"
    },
    "percentiles1": {
        "total": "23",
        "ok": "23",
        "ko": "-"
    },
    "percentiles2": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "percentiles3": {
        "total": "37",
        "ok": "37",
        "ko": "-"
    },
    "percentiles4": {
        "total": "257",
        "ok": "257",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1010,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "8.08",
        "ok": "8.08",
        "ko": "-"
    }
}
    },"req_-json-usercommo-43a62": {
        type: "REQUEST",
        name: "/json/UserCommonApplications",
path: "/json/UserCommonApplications",
pathFormatted: "req_-json-usercommo-43a62",
stats: {
    "name": "/json/UserCommonApplications",
    "numberOfRequests": {
        "total": "1010",
        "ok": "1010",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1167",
        "ok": "1167",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "31",
        "ok": "31",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "54",
        "ok": "54",
        "ko": "-"
    },
    "percentiles1": {
        "total": "23",
        "ok": "23",
        "ko": "-"
    },
    "percentiles2": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "percentiles3": {
        "total": "36",
        "ok": "36",
        "ko": "-"
    },
    "percentiles4": {
        "total": "265",
        "ok": "265",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1009,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "8.08",
        "ok": "8.08",
        "ko": "-"
    }
}
    },"req_-hostrootshell--a77cd": {
        type: "REQUEST",
        name: "/hostRootShell/assets/i18n/en_US.json",
path: "/hostRootShell/assets/i18n/en_US.json",
pathFormatted: "req_-hostrootshell--a77cd",
stats: {
    "name": "/hostRootShell/assets/i18n/en_US.json",
    "numberOfRequests": {
        "total": "1010",
        "ok": "1010",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1069",
        "ok": "1069",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "29",
        "ok": "29",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "44",
        "ok": "44",
        "ko": "-"
    },
    "percentiles1": {
        "total": "23",
        "ok": "23",
        "ko": "-"
    },
    "percentiles2": {
        "total": "26",
        "ok": "26",
        "ko": "-"
    },
    "percentiles3": {
        "total": "38",
        "ok": "38",
        "ko": "-"
    },
    "percentiles4": {
        "total": "260",
        "ok": "260",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1009,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "8.08",
        "ok": "8.08",
        "ko": "-"
    }
}
    },"req_-hostmnemo-scri-e7b48": {
        type: "REQUEST",
        name: "/hostMnemo/scripts.js",
path: "/hostMnemo/scripts.js",
pathFormatted: "req_-hostmnemo-scri-e7b48",
stats: {
    "name": "/hostMnemo/scripts.js",
    "numberOfRequests": {
        "total": "1010",
        "ok": "1010",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "7045",
        "ok": "7045",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "99",
        "ok": "99",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "549",
        "ok": "549",
        "ko": "-"
    },
    "percentiles1": {
        "total": "23",
        "ok": "23",
        "ko": "-"
    },
    "percentiles2": {
        "total": "26",
        "ok": "26",
        "ko": "-"
    },
    "percentiles3": {
        "total": "39",
        "ok": "39",
        "ko": "-"
    },
    "percentiles4": {
        "total": "3198",
        "ok": "3198",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 990,
    "percentage": 98
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 20,
    "percentage": 2
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "8.08",
        "ok": "8.08",
        "ko": "-"
    }
}
    },"req_-mnemo-css-comm-d07a9": {
        type: "REQUEST",
        name: "/mnemo/css/common.css",
path: "/mnemo/css/common.css",
pathFormatted: "req_-mnemo-css-comm-d07a9",
stats: {
    "name": "/mnemo/css/common.css",
    "numberOfRequests": {
        "total": "1010",
        "ok": "1010",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "855",
        "ok": "855",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "28",
        "ok": "28",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "37",
        "ok": "37",
        "ko": "-"
    },
    "percentiles1": {
        "total": "23",
        "ok": "23",
        "ko": "-"
    },
    "percentiles2": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "percentiles3": {
        "total": "37",
        "ok": "37",
        "ko": "-"
    },
    "percentiles4": {
        "total": "250",
        "ok": "250",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1009,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "8.08",
        "ok": "8.08",
        "ko": "-"
    }
}
    },"req_-mnemo-resource-1c1a3": {
        type: "REQUEST",
        name: "/mnemo/resources/graph.txt",
path: "/mnemo/resources/graph.txt",
pathFormatted: "req_-mnemo-resource-1c1a3",
stats: {
    "name": "/mnemo/resources/graph.txt",
    "numberOfRequests": {
        "total": "1010",
        "ok": "1010",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "946",
        "ok": "946",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "29",
        "ok": "29",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "44",
        "ok": "44",
        "ko": "-"
    },
    "percentiles1": {
        "total": "23",
        "ok": "23",
        "ko": "-"
    },
    "percentiles2": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "percentiles3": {
        "total": "38",
        "ok": "38",
        "ko": "-"
    },
    "percentiles4": {
        "total": "237",
        "ok": "237",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1009,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "8.08",
        "ok": "8.08",
        "ko": "-"
    }
}
    },"req_-mnemo-resource-4d1f0": {
        type: "REQUEST",
        name: "/mnemo/resources/graph_ru.txt",
path: "/mnemo/resources/graph_ru.txt",
pathFormatted: "req_-mnemo-resource-4d1f0",
stats: {
    "name": "/mnemo/resources/graph_ru.txt",
    "numberOfRequests": {
        "total": "1010",
        "ok": "1010",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2397",
        "ok": "2397",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "32",
        "ok": "32",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "89",
        "ok": "89",
        "ko": "-"
    },
    "percentiles1": {
        "total": "23",
        "ok": "23",
        "ko": "-"
    },
    "percentiles2": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "percentiles3": {
        "total": "38",
        "ok": "38",
        "ko": "-"
    },
    "percentiles4": {
        "total": "262",
        "ok": "262",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1008,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 1,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "8.08",
        "ok": "8.08",
        "ko": "-"
    }
}
    },"req_-mnemo-resource-82cfb": {
        type: "REQUEST",
        name: "/mnemo/resources/editor.txt",
path: "/mnemo/resources/editor.txt",
pathFormatted: "req_-mnemo-resource-82cfb",
stats: {
    "name": "/mnemo/resources/editor.txt",
    "numberOfRequests": {
        "total": "1010",
        "ok": "1010",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1165",
        "ok": "1165",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "29",
        "ok": "29",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "48",
        "ok": "48",
        "ko": "-"
    },
    "percentiles1": {
        "total": "23",
        "ok": "23",
        "ko": "-"
    },
    "percentiles2": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "percentiles3": {
        "total": "37",
        "ok": "37",
        "ko": "-"
    },
    "percentiles4": {
        "total": "261",
        "ok": "261",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1009,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "8.08",
        "ok": "8.08",
        "ko": "-"
    }
}
    },"req_-mnemo-resource-33452": {
        type: "REQUEST",
        name: "/mnemo/resources/editor_ru.txt",
path: "/mnemo/resources/editor_ru.txt",
pathFormatted: "req_-mnemo-resource-33452",
stats: {
    "name": "/mnemo/resources/editor_ru.txt",
    "numberOfRequests": {
        "total": "1010",
        "ok": "1010",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "349",
        "ok": "349",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "28",
        "ok": "28",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "32",
        "ok": "32",
        "ko": "-"
    },
    "percentiles1": {
        "total": "23",
        "ok": "23",
        "ko": "-"
    },
    "percentiles2": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "percentiles3": {
        "total": "36",
        "ok": "36",
        "ko": "-"
    },
    "percentiles4": {
        "total": "261",
        "ok": "261",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1010,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "8.08",
        "ok": "8.08",
        "ko": "-"
    }
}
    },"req_-hostmnemo-main-5bf99": {
        type: "REQUEST",
        name: "/hostMnemo/main.js",
path: "/hostMnemo/main.js",
pathFormatted: "req_-hostmnemo-main-5bf99",
stats: {
    "name": "/hostMnemo/main.js",
    "numberOfRequests": {
        "total": "1010",
        "ok": "1010",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "17336",
        "ok": "17336",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "293",
        "ok": "293",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "1886",
        "ok": "1886",
        "ko": "-"
    },
    "percentiles1": {
        "total": "23",
        "ok": "23",
        "ko": "-"
    },
    "percentiles2": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "percentiles3": {
        "total": "40",
        "ok": "40",
        "ko": "-"
    },
    "percentiles4": {
        "total": "13522",
        "ok": "13522",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 990,
    "percentage": 98
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 20,
    "percentage": 2
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "8.08",
        "ok": "8.08",
        "ko": "-"
    }
}
    },"req_-hostmnemo-asse-3384a": {
        type: "REQUEST",
        name: "/hostMnemo/assets/i18n/en_US.json",
path: "/hostMnemo/assets/i18n/en_US.json",
pathFormatted: "req_-hostmnemo-asse-3384a",
stats: {
    "name": "/hostMnemo/assets/i18n/en_US.json",
    "numberOfRequests": {
        "total": "1010",
        "ok": "1010",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "556",
        "ok": "556",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "27",
        "ok": "27",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "26",
        "ok": "26",
        "ko": "-"
    },
    "percentiles1": {
        "total": "23",
        "ok": "23",
        "ko": "-"
    },
    "percentiles2": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "percentiles3": {
        "total": "37",
        "ok": "37",
        "ko": "-"
    },
    "percentiles4": {
        "total": "70",
        "ok": "70",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1010,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "8.08",
        "ok": "8.08",
        "ko": "-"
    }
}
    },"req_-hostmnemo-asse-1c9a9": {
        type: "REQUEST",
        name: "/hostMnemo/assets/i18n/zyfra-mnemo/zyfraMnemoModule.en_US.json",
path: "/hostMnemo/assets/i18n/zyfra-mnemo/zyfraMnemoModule.en_US.json",
pathFormatted: "req_-hostmnemo-asse-1c9a9",
stats: {
    "name": "/hostMnemo/assets/i18n/zyfra-mnemo/zyfraMnemoModule.en_US.json",
    "numberOfRequests": {
        "total": "1010",
        "ok": "1010",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1019",
        "ok": "1019",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "29",
        "ok": "29",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "45",
        "ok": "45",
        "ko": "-"
    },
    "percentiles1": {
        "total": "23",
        "ok": "23",
        "ko": "-"
    },
    "percentiles2": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "percentiles3": {
        "total": "37",
        "ok": "37",
        "ko": "-"
    },
    "percentiles4": {
        "total": "255",
        "ok": "255",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1009,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "8.08",
        "ok": "8.08",
        "ko": "-"
    }
}
    },"req_-hostmnemo-asse-dc6b4": {
        type: "REQUEST",
        name: "/hostMnemo/assets/settings.json",
path: "/hostMnemo/assets/settings.json",
pathFormatted: "req_-hostmnemo-asse-dc6b4",
stats: {
    "name": "/hostMnemo/assets/settings.json",
    "numberOfRequests": {
        "total": "1010",
        "ok": "1010",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "450",
        "ok": "450",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "28",
        "ok": "28",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "31",
        "ok": "31",
        "ko": "-"
    },
    "percentiles1": {
        "total": "23",
        "ok": "23",
        "ko": "-"
    },
    "percentiles2": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "percentiles3": {
        "total": "38",
        "ok": "38",
        "ko": "-"
    },
    "percentiles4": {
        "total": "262",
        "ok": "262",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1010,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "8.08",
        "ok": "8.08",
        "ko": "-"
    }
}
    },"req_-mnemo-resource-a83c8": {
        type: "REQUEST",
        name: "/mnemo/resources/grapheditor.txt",
path: "/mnemo/resources/grapheditor.txt",
pathFormatted: "req_-mnemo-resource-a83c8",
stats: {
    "name": "/mnemo/resources/grapheditor.txt",
    "numberOfRequests": {
        "total": "1010",
        "ok": "1010",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "661",
        "ok": "661",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "29",
        "ok": "29",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "40",
        "ok": "40",
        "ko": "-"
    },
    "percentiles1": {
        "total": "23",
        "ok": "23",
        "ko": "-"
    },
    "percentiles2": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "percentiles3": {
        "total": "38",
        "ok": "38",
        "ko": "-"
    },
    "percentiles4": {
        "total": "264",
        "ok": "264",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1010,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "8.08",
        "ok": "8.08",
        "ko": "-"
    }
}
    },"req_-mnemo-styles-d-ee36a": {
        type: "REQUEST",
        name: "/mnemo/styles/default.xml",
path: "/mnemo/styles/default.xml",
pathFormatted: "req_-mnemo-styles-d-ee36a",
stats: {
    "name": "/mnemo/styles/default.xml",
    "numberOfRequests": {
        "total": "1010",
        "ok": "1010",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "570",
        "ok": "570",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "29",
        "ok": "29",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "37",
        "ok": "37",
        "ko": "-"
    },
    "percentiles1": {
        "total": "23",
        "ok": "23",
        "ko": "-"
    },
    "percentiles2": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "percentiles3": {
        "total": "38",
        "ok": "38",
        "ko": "-"
    },
    "percentiles4": {
        "total": "262",
        "ok": "262",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1010,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "8.08",
        "ok": "8.08",
        "ko": "-"
    }
}
    },"req_-mnemo-stencils-43d94": {
        type: "REQUEST",
        name: "/mnemo/stencils/mnemo.library.xml",
path: "/mnemo/stencils/mnemo.library.xml",
pathFormatted: "req_-mnemo-stencils-43d94",
stats: {
    "name": "/mnemo/stencils/mnemo.library.xml",
    "numberOfRequests": {
        "total": "1010",
        "ok": "1010",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "1175",
        "ok": "1175",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "35",
        "ok": "35",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "79",
        "ok": "79",
        "ko": "-"
    },
    "percentiles1": {
        "total": "23",
        "ok": "23",
        "ko": "-"
    },
    "percentiles2": {
        "total": "26",
        "ok": "26",
        "ko": "-"
    },
    "percentiles3": {
        "total": "41",
        "ok": "41",
        "ko": "-"
    },
    "percentiles4": {
        "total": "283",
        "ok": "283",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1007,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 3,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "8.08",
        "ok": "8.08",
        "ko": "-"
    }
}
    },"req_post-mnemobase--159fa": {
        type: "REQUEST",
        name: "POST/mnemobase/api/v1/mnemo",
path: "POST/mnemobase/api/v1/mnemo",
pathFormatted: "req_post-mnemobase--159fa",
stats: {
    "name": "POST/mnemobase/api/v1/mnemo",
    "numberOfRequests": {
        "total": "8",
        "ok": "8",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "34",
        "ok": "34",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "69",
        "ok": "69",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "54",
        "ok": "54",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "12",
        "ok": "12",
        "ko": "-"
    },
    "percentiles1": {
        "total": "56",
        "ok": "56",
        "ko": "-"
    },
    "percentiles2": {
        "total": "65",
        "ok": "65",
        "ko": "-"
    },
    "percentiles3": {
        "total": "68",
        "ok": "68",
        "ko": "-"
    },
    "percentiles4": {
        "total": "69",
        "ok": "69",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 8,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.064",
        "ok": "0.064",
        "ko": "-"
    }
}
    },"req_put-mnemobase-a-181b7": {
        type: "REQUEST",
        name: "PUT/mnemobase/api/v1/mnemo",
path: "PUT/mnemobase/api/v1/mnemo",
pathFormatted: "req_put-mnemobase-a-181b7",
stats: {
    "name": "PUT/mnemobase/api/v1/mnemo",
    "numberOfRequests": {
        "total": "260",
        "ok": "260",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "108",
        "ok": "108",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "2721",
        "ok": "2721",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "185",
        "ok": "185",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "178",
        "ok": "178",
        "ko": "-"
    },
    "percentiles1": {
        "total": "155",
        "ok": "155",
        "ko": "-"
    },
    "percentiles2": {
        "total": "196",
        "ok": "196",
        "ko": "-"
    },
    "percentiles3": {
        "total": "285",
        "ok": "285",
        "ko": "-"
    },
    "percentiles4": {
        "total": "421",
        "ok": "421",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 258,
    "percentage": 99
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 1,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.08",
        "ok": "2.08",
        "ko": "-"
    }
}
    },"req_get-mnemobase-a-ff9f5": {
        type: "REQUEST",
        name: "GET/mnemobase/api/v1/mnemo/",
path: "GET/mnemobase/api/v1/mnemo/",
pathFormatted: "req_get-mnemobase-a-ff9f5",
stats: {
    "name": "GET/mnemobase/api/v1/mnemo/",
    "numberOfRequests": {
        "total": "732",
        "ok": "732",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "56",
        "ok": "56",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "4951",
        "ok": "4951",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "252",
        "ok": "252",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "324",
        "ok": "324",
        "ko": "-"
    },
    "percentiles1": {
        "total": "155",
        "ok": "155",
        "ko": "-"
    },
    "percentiles2": {
        "total": "228",
        "ok": "228",
        "ko": "-"
    },
    "percentiles3": {
        "total": "747",
        "ok": "747",
        "ko": "-"
    },
    "percentiles4": {
        "total": "1494",
        "ok": "1494",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 700,
    "percentage": 96
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 18,
    "percentage": 2
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 14,
    "percentage": 2
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "5.856",
        "ok": "5.856",
        "ko": "-"
    }
}
    },"req_get-mnemo-resou-96eb1": {
        type: "REQUEST",
        name: "GET/mnemo/resources/grapheditor_ru.txt",
path: "GET/mnemo/resources/grapheditor_ru.txt",
pathFormatted: "req_get-mnemo-resou-96eb1",
stats: {
    "name": "GET/mnemo/resources/grapheditor_ru.txt",
    "numberOfRequests": {
        "total": "732",
        "ok": "732",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "595",
        "ok": "595",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "29",
        "ok": "29",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "34",
        "ok": "34",
        "ko": "-"
    },
    "percentiles1": {
        "total": "23",
        "ok": "23",
        "ko": "-"
    },
    "percentiles2": {
        "total": "27",
        "ok": "27",
        "ko": "-"
    },
    "percentiles3": {
        "total": "39",
        "ok": "39",
        "ko": "-"
    },
    "percentiles4": {
        "total": "245",
        "ok": "245",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 732,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "5.856",
        "ok": "5.856",
        "ko": "-"
    }
}
    },"req_get-mnemo-style-4f382": {
        type: "REQUEST",
        name: "GET/mnemo/styles/default.xml",
path: "GET/mnemo/styles/default.xml",
pathFormatted: "req_get-mnemo-style-4f382",
stats: {
    "name": "GET/mnemo/styles/default.xml",
    "numberOfRequests": {
        "total": "732",
        "ok": "732",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "284",
        "ok": "284",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "28",
        "ok": "28",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "26",
        "ok": "26",
        "ko": "-"
    },
    "percentiles1": {
        "total": "23",
        "ok": "23",
        "ko": "-"
    },
    "percentiles2": {
        "total": "26",
        "ok": "26",
        "ko": "-"
    },
    "percentiles3": {
        "total": "37",
        "ok": "37",
        "ko": "-"
    },
    "percentiles4": {
        "total": "251",
        "ok": "251",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 732,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "5.856",
        "ok": "5.856",
        "ko": "-"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
