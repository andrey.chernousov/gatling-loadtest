var stats = {
    type: "GROUP",
name: "Global Information",
path: "",
pathFormatted: "group_missing-name-b06d1",
stats: {
    "name": "Global Information",
    "numberOfRequests": {
        "total": "1840",
        "ok": "1840",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "20668",
        "ok": "20668",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "249",
        "ok": "249",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "1615",
        "ok": "1615",
        "ko": "-"
    },
    "percentiles1": {
        "total": "24",
        "ok": "24",
        "ko": "-"
    },
    "percentiles2": {
        "total": "29",
        "ok": "29",
        "ko": "-"
    },
    "percentiles3": {
        "total": "250",
        "ok": "250",
        "ko": "-"
    },
    "percentiles4": {
        "total": "11585",
        "ok": "11585",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 1799,
    "percentage": 98
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 40,
    "percentage": 2
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "59.355",
        "ok": "59.355",
        "ko": "-"
    }
},
contents: {
"req_login-99dea": {
        type: "REQUEST",
        name: "Login",
path: "Login",
pathFormatted: "req_login-99dea",
stats: {
    "name": "Login",
    "numberOfRequests": {
        "total": "60",
        "ok": "60",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "136",
        "ok": "136",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "617",
        "ok": "617",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "174",
        "ok": "174",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "96",
        "ok": "96",
        "ko": "-"
    },
    "percentiles1": {
        "total": "149",
        "ok": "149",
        "ko": "-"
    },
    "percentiles2": {
        "total": "156",
        "ok": "156",
        "ko": "-"
    },
    "percentiles3": {
        "total": "263",
        "ok": "263",
        "ko": "-"
    },
    "percentiles4": {
        "total": "595",
        "ok": "595",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 60,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.935",
        "ok": "1.935",
        "ko": "-"
    }
}
    },"req_-mnemoschemes-a-0c177": {
        type: "REQUEST",
        name: "/mnemoschemes/api/v1/mnemo/info",
path: "/mnemoschemes/api/v1/mnemo/info",
pathFormatted: "req_-mnemoschemes-a-0c177",
stats: {
    "name": "/mnemoschemes/api/v1/mnemo/info",
    "numberOfRequests": {
        "total": "120",
        "ok": "120",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "34",
        "ok": "34",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "334",
        "ok": "334",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "51",
        "ok": "51",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "41",
        "ok": "41",
        "ko": "-"
    },
    "percentiles1": {
        "total": "39",
        "ok": "39",
        "ko": "-"
    },
    "percentiles2": {
        "total": "47",
        "ok": "47",
        "ko": "-"
    },
    "percentiles3": {
        "total": "73",
        "ok": "73",
        "ko": "-"
    },
    "percentiles4": {
        "total": "297",
        "ok": "297",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 120,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.871",
        "ok": "3.871",
        "ko": "-"
    }
}
    },"req_post-mnemoschem-dba6c": {
        type: "REQUEST",
        name: "POST/mnemoschemes/api/v1/mnemo",
path: "POST/mnemoschemes/api/v1/mnemo",
pathFormatted: "req_post-mnemoschem-dba6c",
stats: {
    "name": "POST/mnemoschemes/api/v1/mnemo",
    "numberOfRequests": {
        "total": "8",
        "ok": "8",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "27",
        "ok": "27",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "31",
        "ok": "31",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "29",
        "ok": "29",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "1",
        "ok": "1",
        "ko": "-"
    },
    "percentiles1": {
        "total": "29",
        "ok": "29",
        "ko": "-"
    },
    "percentiles2": {
        "total": "29",
        "ok": "29",
        "ko": "-"
    },
    "percentiles3": {
        "total": "31",
        "ok": "31",
        "ko": "-"
    },
    "percentiles4": {
        "total": "31",
        "ok": "31",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 8,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.258",
        "ok": "0.258",
        "ko": "-"
    }
}
    },"req_get--e0e39": {
        type: "REQUEST",
        name: "GET/",
path: "GET/",
pathFormatted: "req_get--e0e39",
stats: {
    "name": "GET/",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "93",
        "ok": "93",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "24",
        "ok": "24",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "10",
        "ok": "10",
        "ko": "-"
    },
    "percentiles1": {
        "total": "23",
        "ok": "23",
        "ko": "-"
    },
    "percentiles2": {
        "total": "24",
        "ok": "24",
        "ko": "-"
    },
    "percentiles3": {
        "total": "26",
        "ok": "26",
        "ko": "-"
    },
    "percentiles4": {
        "total": "60",
        "ok": "60",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.613",
        "ok": "1.613",
        "ko": "-"
    }
}
    },"req_get-built-scrip-f7a2f": {
        type: "REQUEST",
        name: "GET/built/scripts/zone.min.js",
path: "GET/built/scripts/zone.min.js",
pathFormatted: "req_get-built-scrip-f7a2f",
stats: {
    "name": "GET/built/scripts/zone.min.js",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "446",
        "ok": "446",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "66",
        "ok": "66",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "92",
        "ok": "92",
        "ko": "-"
    },
    "percentiles1": {
        "total": "23",
        "ok": "23",
        "ko": "-"
    },
    "percentiles2": {
        "total": "71",
        "ok": "71",
        "ko": "-"
    },
    "percentiles3": {
        "total": "266",
        "ok": "266",
        "ko": "-"
    },
    "percentiles4": {
        "total": "420",
        "ok": "420",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.613",
        "ok": "1.613",
        "ko": "-"
    }
}
    },"req_get-built-scrip-6c713": {
        type: "REQUEST",
        name: "GET/built/scripts/import-map-overrides.js",
path: "GET/built/scripts/import-map-overrides.js",
pathFormatted: "req_get-built-scrip-6c713",
stats: {
    "name": "GET/built/scripts/import-map-overrides.js",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "705",
        "ok": "705",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "48",
        "ok": "48",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "106",
        "ok": "106",
        "ko": "-"
    },
    "percentiles1": {
        "total": "23",
        "ok": "23",
        "ko": "-"
    },
    "percentiles2": {
        "total": "24",
        "ok": "24",
        "ko": "-"
    },
    "percentiles3": {
        "total": "185",
        "ok": "185",
        "ko": "-"
    },
    "percentiles4": {
        "total": "495",
        "ok": "495",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.613",
        "ok": "1.613",
        "ko": "-"
    }
}
    },"req_get-built-scrip-3e0b8": {
        type: "REQUEST",
        name: "GET/built/scripts/system.min.js",
path: "GET/built/scripts/system.min.js",
pathFormatted: "req_get-built-scrip-3e0b8",
stats: {
    "name": "GET/built/scripts/system.min.js",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "939",
        "ok": "939",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "46",
        "ok": "46",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "131",
        "ok": "131",
        "ko": "-"
    },
    "percentiles1": {
        "total": "23",
        "ok": "23",
        "ko": "-"
    },
    "percentiles2": {
        "total": "24",
        "ok": "24",
        "ko": "-"
    },
    "percentiles3": {
        "total": "29",
        "ok": "29",
        "ko": "-"
    },
    "percentiles4": {
        "total": "602",
        "ok": "602",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 49,
    "percentage": 98
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 1,
    "percentage": 2
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.613",
        "ok": "1.613",
        "ko": "-"
    }
}
    },"req_get-built-scrip-a6908": {
        type: "REQUEST",
        name: "GET/built/scripts/amd.min.js",
path: "GET/built/scripts/amd.min.js",
pathFormatted: "req_get-built-scrip-a6908",
stats: {
    "name": "GET/built/scripts/amd.min.js",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "592",
        "ok": "592",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "39",
        "ok": "39",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "86",
        "ok": "86",
        "ko": "-"
    },
    "percentiles1": {
        "total": "22",
        "ok": "22",
        "ko": "-"
    },
    "percentiles2": {
        "total": "24",
        "ok": "24",
        "ko": "-"
    },
    "percentiles3": {
        "total": "28",
        "ok": "28",
        "ko": "-"
    },
    "percentiles4": {
        "total": "431",
        "ok": "431",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.613",
        "ok": "1.613",
        "ko": "-"
    }
}
    },"req_get-built-scrip-a5f03": {
        type: "REQUEST",
        name: "GET/built/scripts/named-exports.js",
path: "GET/built/scripts/named-exports.js",
pathFormatted: "req_get-built-scrip-a5f03",
stats: {
    "name": "GET/built/scripts/named-exports.js",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "250",
        "ok": "250",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "28",
        "ok": "28",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "32",
        "ok": "32",
        "ko": "-"
    },
    "percentiles1": {
        "total": "23",
        "ok": "23",
        "ko": "-"
    },
    "percentiles2": {
        "total": "24",
        "ok": "24",
        "ko": "-"
    },
    "percentiles3": {
        "total": "26",
        "ok": "26",
        "ko": "-"
    },
    "percentiles4": {
        "total": "141",
        "ok": "141",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.613",
        "ok": "1.613",
        "ko": "-"
    }
}
    },"req_get-built-scrip-96ae1": {
        type: "REQUEST",
        name: "GET/built/scripts/named-register.min.js",
path: "GET/built/scripts/named-register.min.js",
pathFormatted: "req_get-built-scrip-96ae1",
stats: {
    "name": "GET/built/scripts/named-register.min.js",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "253",
        "ok": "253",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "32",
        "ok": "32",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "45",
        "ok": "45",
        "ko": "-"
    },
    "percentiles1": {
        "total": "23",
        "ok": "23",
        "ko": "-"
    },
    "percentiles2": {
        "total": "24",
        "ok": "24",
        "ko": "-"
    },
    "percentiles3": {
        "total": "28",
        "ok": "28",
        "ko": "-"
    },
    "percentiles4": {
        "total": "251",
        "ok": "251",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.613",
        "ok": "1.613",
        "ko": "-"
    }
}
    },"req_get-built-vendo-cdd2c": {
        type: "REQUEST",
        name: "GET/built/vendors~main.js",
path: "GET/built/vendors~main.js",
pathFormatted: "req_get-built-vendo-cdd2c",
stats: {
    "name": "GET/built/vendors~main.js",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "604",
        "ok": "604",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "153",
        "ok": "153",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "165",
        "ok": "165",
        "ko": "-"
    },
    "percentiles1": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "percentiles2": {
        "total": "251",
        "ok": "251",
        "ko": "-"
    },
    "percentiles3": {
        "total": "484",
        "ok": "484",
        "ko": "-"
    },
    "percentiles4": {
        "total": "600",
        "ok": "600",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.613",
        "ok": "1.613",
        "ko": "-"
    }
}
    },"req_get-built-main--37f57": {
        type: "REQUEST",
        name: "GET/built/main.js",
path: "GET/built/main.js",
pathFormatted: "req_get-built-main--37f57",
stats: {
    "name": "GET/built/main.js",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "279",
        "ok": "279",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "29",
        "ok": "29",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "36",
        "ok": "36",
        "ko": "-"
    },
    "percentiles1": {
        "total": "23",
        "ok": "23",
        "ko": "-"
    },
    "percentiles2": {
        "total": "24",
        "ok": "24",
        "ko": "-"
    },
    "percentiles3": {
        "total": "26",
        "ok": "26",
        "ko": "-"
    },
    "percentiles4": {
        "total": "166",
        "ok": "166",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.613",
        "ok": "1.613",
        "ko": "-"
    }
}
    },"req_get-built-asset-4566d": {
        type: "REQUEST",
        name: "GET/built/assets/settings.json",
path: "GET/built/assets/settings.json",
pathFormatted: "req_get-built-asset-4566d",
stats: {
    "name": "GET/built/assets/settings.json",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "588",
        "ok": "588",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "38",
        "ok": "38",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "85",
        "ok": "85",
        "ko": "-"
    },
    "percentiles1": {
        "total": "22",
        "ok": "22",
        "ko": "-"
    },
    "percentiles2": {
        "total": "23",
        "ok": "23",
        "ko": "-"
    },
    "percentiles3": {
        "total": "26",
        "ok": "26",
        "ko": "-"
    },
    "percentiles4": {
        "total": "421",
        "ok": "421",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.613",
        "ok": "1.613",
        "ko": "-"
    }
}
    },"req_get-auth-realms-24c57": {
        type: "REQUEST",
        name: "GET/auth/realms/master/account",
path: "GET/auth/realms/master/account",
pathFormatted: "req_get-auth-realms-24c57",
stats: {
    "name": "GET/auth/realms/master/account",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "22",
        "ok": "22",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "249",
        "ok": "249",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "39",
        "ok": "39",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "53",
        "ok": "53",
        "ko": "-"
    },
    "percentiles1": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "percentiles2": {
        "total": "26",
        "ok": "26",
        "ko": "-"
    },
    "percentiles3": {
        "total": "155",
        "ok": "155",
        "ko": "-"
    },
    "percentiles4": {
        "total": "248",
        "ok": "248",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.613",
        "ok": "1.613",
        "ko": "-"
    }
}
    },"req_get-auth-realms-177d8": {
        type: "REQUEST",
        name: "GET/auth/realms/master/account Redirect 1",
path: "GET/auth/realms/master/account Redirect 1",
pathFormatted: "req_get-auth-realms-177d8",
stats: {
    "name": "GET/auth/realms/master/account Redirect 1",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "28",
        "ok": "28",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "398",
        "ok": "398",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "58",
        "ok": "58",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "88",
        "ok": "88",
        "ko": "-"
    },
    "percentiles1": {
        "total": "31",
        "ok": "31",
        "ko": "-"
    },
    "percentiles2": {
        "total": "35",
        "ok": "35",
        "ko": "-"
    },
    "percentiles3": {
        "total": "322",
        "ok": "322",
        "ko": "-"
    },
    "percentiles4": {
        "total": "387",
        "ok": "387",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.613",
        "ok": "1.613",
        "ko": "-"
    }
}
    },"req_get-json-applic-72f39": {
        type: "REQUEST",
        name: "GET/json/applicationUnits",
path: "GET/json/applicationUnits",
pathFormatted: "req_get-json-applic-72f39",
stats: {
    "name": "GET/json/applicationUnits",
    "numberOfRequests": {
        "total": "100",
        "ok": "100",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "578",
        "ok": "578",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "35",
        "ok": "35",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "66",
        "ok": "66",
        "ko": "-"
    },
    "percentiles1": {
        "total": "23",
        "ok": "23",
        "ko": "-"
    },
    "percentiles2": {
        "total": "24",
        "ok": "24",
        "ko": "-"
    },
    "percentiles3": {
        "total": "26",
        "ok": "26",
        "ko": "-"
    },
    "percentiles4": {
        "total": "250",
        "ok": "250",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 100,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "3.226",
        "ok": "3.226",
        "ko": "-"
    }
}
    },"req_get-built-scrip-2d4a0": {
        type: "REQUEST",
        name: "GET/built/scripts/single-spa.min.js",
path: "GET/built/scripts/single-spa.min.js",
pathFormatted: "req_get-built-scrip-2d4a0",
stats: {
    "name": "GET/built/scripts/single-spa.min.js",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "398",
        "ok": "398",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "50",
        "ok": "50",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "74",
        "ok": "74",
        "ko": "-"
    },
    "percentiles1": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "percentiles2": {
        "total": "42",
        "ok": "42",
        "ko": "-"
    },
    "percentiles3": {
        "total": "146",
        "ok": "146",
        "ko": "-"
    },
    "percentiles4": {
        "total": "389",
        "ok": "389",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.613",
        "ok": "1.613",
        "ko": "-"
    }
}
    },"req_get-json-userse-56296": {
        type: "REQUEST",
        name: "GET/json/UserSettings",
path: "GET/json/UserSettings",
pathFormatted: "req_get-json-userse-56296",
stats: {
    "name": "GET/json/UserSettings",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "27",
        "ok": "27",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "23",
        "ok": "23",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "1",
        "ok": "1",
        "ko": "-"
    },
    "percentiles1": {
        "total": "23",
        "ok": "23",
        "ko": "-"
    },
    "percentiles2": {
        "total": "24",
        "ok": "24",
        "ko": "-"
    },
    "percentiles3": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "percentiles4": {
        "total": "27",
        "ok": "27",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.613",
        "ok": "1.613",
        "ko": "-"
    }
}
    },"req_get-json-userco-f4ad0": {
        type: "REQUEST",
        name: "GET/json/UserCommonApplications",
path: "GET/json/UserCommonApplications",
pathFormatted: "req_get-json-userco-f4ad0",
stats: {
    "name": "GET/json/UserCommonApplications",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "446",
        "ok": "446",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "32",
        "ok": "32",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "59",
        "ok": "59",
        "ko": "-"
    },
    "percentiles1": {
        "total": "23",
        "ok": "23",
        "ko": "-"
    },
    "percentiles2": {
        "total": "24",
        "ok": "24",
        "ko": "-"
    },
    "percentiles3": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "percentiles4": {
        "total": "240",
        "ok": "240",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.613",
        "ok": "1.613",
        "ko": "-"
    }
}
    },"req_get-hostrootshe-d0154": {
        type: "REQUEST",
        name: "GET/hostRootShell/assets/i18n/en_US.json",
path: "GET/hostRootShell/assets/i18n/en_US.json",
pathFormatted: "req_get-hostrootshe-d0154",
stats: {
    "name": "GET/hostRootShell/assets/i18n/en_US.json",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "245",
        "ok": "245",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "28",
        "ok": "28",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "31",
        "ok": "31",
        "ko": "-"
    },
    "percentiles1": {
        "total": "23",
        "ok": "23",
        "ko": "-"
    },
    "percentiles2": {
        "total": "24",
        "ok": "24",
        "ko": "-"
    },
    "percentiles3": {
        "total": "26",
        "ok": "26",
        "ko": "-"
    },
    "percentiles4": {
        "total": "138",
        "ok": "138",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.613",
        "ok": "1.613",
        "ko": "-"
    }
}
    },"req_get-hostmnemo-s-7aeb7": {
        type: "REQUEST",
        name: "GET/hostMnemo/scripts.js",
path: "GET/hostMnemo/scripts.js",
pathFormatted: "req_get-hostmnemo-s-7aeb7",
stats: {
    "name": "GET/hostMnemo/scripts.js",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "6837",
        "ok": "6837",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "1466",
        "ok": "1466",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "2000",
        "ok": "2000",
        "ko": "-"
    },
    "percentiles1": {
        "total": "26",
        "ok": "26",
        "ko": "-"
    },
    "percentiles2": {
        "total": "2669",
        "ok": "2669",
        "ko": "-"
    },
    "percentiles3": {
        "total": "5691",
        "ok": "5691",
        "ko": "-"
    },
    "percentiles4": {
        "total": "6382",
        "ok": "6382",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 30,
    "percentage": 60
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 20,
    "percentage": 40
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.613",
        "ok": "1.613",
        "ko": "-"
    }
}
    },"req_get-mnemo-css-c-7a130": {
        type: "REQUEST",
        name: "GET/mnemo/css/common.css",
path: "GET/mnemo/css/common.css",
pathFormatted: "req_get-mnemo-css-c-7a130",
stats: {
    "name": "GET/mnemo/css/common.css",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "250",
        "ok": "250",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "28",
        "ok": "28",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "32",
        "ok": "32",
        "ko": "-"
    },
    "percentiles1": {
        "total": "24",
        "ok": "24",
        "ko": "-"
    },
    "percentiles2": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "percentiles3": {
        "total": "26",
        "ok": "26",
        "ko": "-"
    },
    "percentiles4": {
        "total": "141",
        "ok": "141",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.613",
        "ok": "1.613",
        "ko": "-"
    }
}
    },"req_get-mnemo-resou-6daa7": {
        type: "REQUEST",
        name: "GET/mnemo/resources/graph.txt",
path: "GET/mnemo/resources/graph.txt",
pathFormatted: "req_get-mnemo-resou-6daa7",
stats: {
    "name": "GET/mnemo/resources/graph.txt",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "249",
        "ok": "249",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "28",
        "ok": "28",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "32",
        "ok": "32",
        "ko": "-"
    },
    "percentiles1": {
        "total": "23",
        "ok": "23",
        "ko": "-"
    },
    "percentiles2": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "percentiles3": {
        "total": "27",
        "ok": "27",
        "ko": "-"
    },
    "percentiles4": {
        "total": "141",
        "ok": "141",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.613",
        "ok": "1.613",
        "ko": "-"
    }
}
    },"req_get-mnemo-resou-95e3c": {
        type: "REQUEST",
        name: "GET/mnemo/resources/graph_ru.txt",
path: "GET/mnemo/resources/graph_ru.txt",
pathFormatted: "req_get-mnemo-resou-95e3c",
stats: {
    "name": "GET/mnemo/resources/graph_ru.txt",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "247",
        "ok": "247",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "32",
        "ok": "32",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "44",
        "ok": "44",
        "ko": "-"
    },
    "percentiles1": {
        "total": "24",
        "ok": "24",
        "ko": "-"
    },
    "percentiles2": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "percentiles3": {
        "total": "26",
        "ok": "26",
        "ko": "-"
    },
    "percentiles4": {
        "total": "246",
        "ok": "246",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.613",
        "ok": "1.613",
        "ko": "-"
    }
}
    },"req_get-mnemo-resou-d3607": {
        type: "REQUEST",
        name: "GET/mnemo/resources/editor.txt",
path: "GET/mnemo/resources/editor.txt",
pathFormatted: "req_get-mnemo-resou-d3607",
stats: {
    "name": "GET/mnemo/resources/editor.txt",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "250",
        "ok": "250",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "37",
        "ok": "37",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "53",
        "ok": "53",
        "ko": "-"
    },
    "percentiles1": {
        "total": "24",
        "ok": "24",
        "ko": "-"
    },
    "percentiles2": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "percentiles3": {
        "total": "147",
        "ok": "147",
        "ko": "-"
    },
    "percentiles4": {
        "total": "250",
        "ok": "250",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.613",
        "ok": "1.613",
        "ko": "-"
    }
}
    },"req_get-mnemo-resou-03f4c": {
        type: "REQUEST",
        name: "GET/mnemo/resources/editor_ru.txt",
path: "GET/mnemo/resources/editor_ru.txt",
pathFormatted: "req_get-mnemo-resou-03f4c",
stats: {
    "name": "GET/mnemo/resources/editor_ru.txt",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "248",
        "ok": "248",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "28",
        "ok": "28",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "31",
        "ok": "31",
        "ko": "-"
    },
    "percentiles1": {
        "total": "23",
        "ok": "23",
        "ko": "-"
    },
    "percentiles2": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "percentiles3": {
        "total": "26",
        "ok": "26",
        "ko": "-"
    },
    "percentiles4": {
        "total": "143",
        "ok": "143",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.613",
        "ok": "1.613",
        "ko": "-"
    }
}
    },"req_get-hostmnemo-m-22113": {
        type: "REQUEST",
        name: "GET/hostMnemo/main.js",
path: "GET/hostMnemo/main.js",
pathFormatted: "req_get-hostmnemo-m-22113",
stats: {
    "name": "GET/hostMnemo/main.js",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "20668",
        "ok": "20668",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "6006",
        "ok": "6006",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "7467",
        "ok": "7467",
        "ko": "-"
    },
    "percentiles1": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "percentiles2": {
        "total": "13715",
        "ok": "13715",
        "ko": "-"
    },
    "percentiles3": {
        "total": "17427",
        "ok": "17427",
        "ko": "-"
    },
    "percentiles4": {
        "total": "19363",
        "ok": "19363",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 30,
    "percentage": 60
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 20,
    "percentage": 40
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.613",
        "ok": "1.613",
        "ko": "-"
    }
}
    },"req_get-hostmnemo-a-913f4": {
        type: "REQUEST",
        name: "GET/hostMnemo/assets/i18n/en_US.json",
path: "GET/hostMnemo/assets/i18n/en_US.json",
pathFormatted: "req_get-hostmnemo-a-913f4",
stats: {
    "name": "GET/hostMnemo/assets/i18n/en_US.json",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "700",
        "ok": "700",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "37",
        "ok": "37",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "95",
        "ok": "95",
        "ko": "-"
    },
    "percentiles1": {
        "total": "23",
        "ok": "23",
        "ko": "-"
    },
    "percentiles2": {
        "total": "24",
        "ok": "24",
        "ko": "-"
    },
    "percentiles3": {
        "total": "27",
        "ok": "27",
        "ko": "-"
    },
    "percentiles4": {
        "total": "378",
        "ok": "378",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.613",
        "ok": "1.613",
        "ko": "-"
    }
}
    },"req_get-hostmnemo-a-19ffa": {
        type: "REQUEST",
        name: "GET/hostMnemo/assets/i18n/zyfra-mnemo/zyfraMnemoModule.en_US.json",
path: "GET/hostMnemo/assets/i18n/zyfra-mnemo/zyfraMnemoModule.en_US.json",
pathFormatted: "req_get-hostmnemo-a-19ffa",
stats: {
    "name": "GET/hostMnemo/assets/i18n/zyfra-mnemo/zyfraMnemoModule.en_US.json",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "139",
        "ok": "139",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "27",
        "ok": "27",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "17",
        "ok": "17",
        "ko": "-"
    },
    "percentiles1": {
        "total": "23",
        "ok": "23",
        "ko": "-"
    },
    "percentiles2": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "percentiles3": {
        "total": "47",
        "ok": "47",
        "ko": "-"
    },
    "percentiles4": {
        "total": "95",
        "ok": "95",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.613",
        "ok": "1.613",
        "ko": "-"
    }
}
    },"req_get-hostmnemo-a-5dc3d": {
        type: "REQUEST",
        name: "GET/hostMnemo/assets/settings.json",
path: "GET/hostMnemo/assets/settings.json",
pathFormatted: "req_get-hostmnemo-a-5dc3d",
stats: {
    "name": "GET/hostMnemo/assets/settings.json",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "46",
        "ok": "46",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "6",
        "ok": "6",
        "ko": "-"
    },
    "percentiles1": {
        "total": "22",
        "ok": "22",
        "ko": "-"
    },
    "percentiles2": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "percentiles3": {
        "total": "44",
        "ok": "44",
        "ko": "-"
    },
    "percentiles4": {
        "total": "45",
        "ok": "45",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.613",
        "ok": "1.613",
        "ko": "-"
    }
}
    },"req_get-mnemo-resou-b8f0f": {
        type: "REQUEST",
        name: "GET/mnemo/resources/grapheditor.txt",
path: "GET/mnemo/resources/grapheditor.txt",
pathFormatted: "req_get-mnemo-resou-b8f0f",
stats: {
    "name": "GET/mnemo/resources/grapheditor.txt",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "251",
        "ok": "251",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "34",
        "ok": "34",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "41",
        "ok": "41",
        "ko": "-"
    },
    "percentiles1": {
        "total": "24",
        "ok": "24",
        "ko": "-"
    },
    "percentiles2": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "percentiles3": {
        "total": "88",
        "ok": "88",
        "ko": "-"
    },
    "percentiles4": {
        "total": "217",
        "ok": "217",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.613",
        "ok": "1.613",
        "ko": "-"
    }
}
    },"req_get-mnemo-style-4f382": {
        type: "REQUEST",
        name: "GET/mnemo/styles/default.xml",
path: "GET/mnemo/styles/default.xml",
pathFormatted: "req_get-mnemo-style-4f382",
stats: {
    "name": "GET/mnemo/styles/default.xml",
    "numberOfRequests": {
        "total": "81",
        "ok": "81",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "469",
        "ok": "469",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "31",
        "ok": "31",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "55",
        "ok": "55",
        "ko": "-"
    },
    "percentiles1": {
        "total": "22",
        "ok": "22",
        "ko": "-"
    },
    "percentiles2": {
        "total": "24",
        "ok": "24",
        "ko": "-"
    },
    "percentiles3": {
        "total": "26",
        "ok": "26",
        "ko": "-"
    },
    "percentiles4": {
        "total": "292",
        "ok": "292",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 81,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "2.613",
        "ok": "2.613",
        "ko": "-"
    }
}
    },"req_get-mnemo-stenc-e38f6": {
        type: "REQUEST",
        name: "GET/mnemo/stencils/mnemo.library.xml",
path: "GET/mnemo/stencils/mnemo.library.xml",
pathFormatted: "req_get-mnemo-stenc-e38f6",
stats: {
    "name": "GET/mnemo/stencils/mnemo.library.xml",
    "numberOfRequests": {
        "total": "50",
        "ok": "50",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "599",
        "ok": "599",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "100",
        "ok": "100",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "117",
        "ok": "117",
        "ko": "-"
    },
    "percentiles1": {
        "total": "24",
        "ok": "24",
        "ko": "-"
    },
    "percentiles2": {
        "total": "166",
        "ok": "166",
        "ko": "-"
    },
    "percentiles3": {
        "total": "275",
        "ok": "275",
        "ko": "-"
    },
    "percentiles4": {
        "total": "496",
        "ok": "496",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 50,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1.613",
        "ok": "1.613",
        "ko": "-"
    }
}
    },"req_get-mnemoscheme-b6811": {
        type: "REQUEST",
        name: "GET/mnemoschemes/api/v1/mnemo/",
path: "GET/mnemoschemes/api/v1/mnemo/",
pathFormatted: "req_get-mnemoscheme-b6811",
stats: {
    "name": "GET/mnemoschemes/api/v1/mnemo/",
    "numberOfRequests": {
        "total": "31",
        "ok": "31",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "35",
        "ok": "35",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "409",
        "ok": "409",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "150",
        "ok": "150",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "111",
        "ok": "111",
        "ko": "-"
    },
    "percentiles1": {
        "total": "150",
        "ok": "150",
        "ko": "-"
    },
    "percentiles2": {
        "total": "201",
        "ok": "201",
        "ko": "-"
    },
    "percentiles3": {
        "total": "359",
        "ok": "359",
        "ko": "-"
    },
    "percentiles4": {
        "total": "394",
        "ok": "394",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 31,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1",
        "ok": "1",
        "ko": "-"
    }
}
    },"req_get-mnemo-resou-96eb1": {
        type: "REQUEST",
        name: "GET/mnemo/resources/grapheditor_ru.txt",
path: "GET/mnemo/resources/grapheditor_ru.txt",
pathFormatted: "req_get-mnemo-resou-96eb1",
stats: {
    "name": "GET/mnemo/resources/grapheditor_ru.txt",
    "numberOfRequests": {
        "total": "31",
        "ok": "31",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "21",
        "ok": "21",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "127",
        "ok": "127",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "34",
        "ok": "34",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "27",
        "ok": "27",
        "ko": "-"
    },
    "percentiles1": {
        "total": "24",
        "ok": "24",
        "ko": "-"
    },
    "percentiles2": {
        "total": "25",
        "ok": "25",
        "ko": "-"
    },
    "percentiles3": {
        "total": "102",
        "ok": "102",
        "ko": "-"
    },
    "percentiles4": {
        "total": "127",
        "ok": "127",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 31,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "1",
        "ok": "1",
        "ko": "-"
    }
}
    },"req_put-mnemoscheme-aabbe": {
        type: "REQUEST",
        name: "PUT/mnemoschemes/api/v1/mnemo",
path: "PUT/mnemoschemes/api/v1/mnemo",
pathFormatted: "req_put-mnemoscheme-aabbe",
stats: {
    "name": "PUT/mnemoschemes/api/v1/mnemo",
    "numberOfRequests": {
        "total": "9",
        "ok": "9",
        "ko": "0"
    },
    "minResponseTime": {
        "total": "73",
        "ok": "73",
        "ko": "-"
    },
    "maxResponseTime": {
        "total": "108",
        "ok": "108",
        "ko": "-"
    },
    "meanResponseTime": {
        "total": "87",
        "ok": "87",
        "ko": "-"
    },
    "standardDeviation": {
        "total": "12",
        "ok": "12",
        "ko": "-"
    },
    "percentiles1": {
        "total": "92",
        "ok": "92",
        "ko": "-"
    },
    "percentiles2": {
        "total": "96",
        "ok": "96",
        "ko": "-"
    },
    "percentiles3": {
        "total": "104",
        "ok": "104",
        "ko": "-"
    },
    "percentiles4": {
        "total": "107",
        "ok": "107",
        "ko": "-"
    },
    "group1": {
    "name": "t < 800 ms",
    "count": 9,
    "percentage": 100
},
    "group2": {
    "name": "800 ms < t < 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group3": {
    "name": "t > 1200 ms",
    "count": 0,
    "percentage": 0
},
    "group4": {
    "name": "failed",
    "count": 0,
    "percentage": 0
},
    "meanNumberOfRequestsPerSecond": {
        "total": "0.29",
        "ok": "0.29",
        "ko": "-"
    }
}
    }
}

}

function fillStats(stat){
    $("#numberOfRequests").append(stat.numberOfRequests.total);
    $("#numberOfRequestsOK").append(stat.numberOfRequests.ok);
    $("#numberOfRequestsKO").append(stat.numberOfRequests.ko);

    $("#minResponseTime").append(stat.minResponseTime.total);
    $("#minResponseTimeOK").append(stat.minResponseTime.ok);
    $("#minResponseTimeKO").append(stat.minResponseTime.ko);

    $("#maxResponseTime").append(stat.maxResponseTime.total);
    $("#maxResponseTimeOK").append(stat.maxResponseTime.ok);
    $("#maxResponseTimeKO").append(stat.maxResponseTime.ko);

    $("#meanResponseTime").append(stat.meanResponseTime.total);
    $("#meanResponseTimeOK").append(stat.meanResponseTime.ok);
    $("#meanResponseTimeKO").append(stat.meanResponseTime.ko);

    $("#standardDeviation").append(stat.standardDeviation.total);
    $("#standardDeviationOK").append(stat.standardDeviation.ok);
    $("#standardDeviationKO").append(stat.standardDeviation.ko);

    $("#percentiles1").append(stat.percentiles1.total);
    $("#percentiles1OK").append(stat.percentiles1.ok);
    $("#percentiles1KO").append(stat.percentiles1.ko);

    $("#percentiles2").append(stat.percentiles2.total);
    $("#percentiles2OK").append(stat.percentiles2.ok);
    $("#percentiles2KO").append(stat.percentiles2.ko);

    $("#percentiles3").append(stat.percentiles3.total);
    $("#percentiles3OK").append(stat.percentiles3.ok);
    $("#percentiles3KO").append(stat.percentiles3.ko);

    $("#percentiles4").append(stat.percentiles4.total);
    $("#percentiles4OK").append(stat.percentiles4.ok);
    $("#percentiles4KO").append(stat.percentiles4.ko);

    $("#meanNumberOfRequestsPerSecond").append(stat.meanNumberOfRequestsPerSecond.total);
    $("#meanNumberOfRequestsPerSecondOK").append(stat.meanNumberOfRequestsPerSecond.ok);
    $("#meanNumberOfRequestsPerSecondKO").append(stat.meanNumberOfRequestsPerSecond.ko);
}
