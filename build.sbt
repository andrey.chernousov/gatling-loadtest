
name := "gatlingtest_3"

version := "0.1"

scalaVersion := "2.12.10"

enablePlugins(GatlingPlugin)

libraryDependencies += "io.gatling.highcharts" % "gatling-charts-highcharts-bundle" % "3.3.0" % "test"

libraryDependencies += "io.gatling" % "gatling-test-framework" % "3.3.1" % "test"

libraryDependencies += "com.typesafe.play" %% "play-json" % "2.7.2"

libraryDependencies += "org.scalaj" % "scalaj-http_2.12" % "2.4.2"

//libraryDependencies += "org.scalatest" % "scalatest_2.12.0-M2" % "2.2.5-M2" % "pom"